$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});



function deleteByKey(ele, key,route_prefix="web") {
    let inputId = $(ele).attr('id');
    swal({
        width: "400px",
        icon: "question",
        title: "Are you sure?",
        showCancelButton: true,
        confirmButtonText: "Delete",
        cancelButtonText: "Cancel",
        confirmButtonColor: "#FF0000",
        html: "<br>Your will not be able to recover this action!",
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: `/${route_prefix}/${key}/${inputId}`,
                method: 'DELETE',
                success: function (response) {
                    if (response.success === 1) {
                        Swal(
                            'DELETED',
                            key.toUpperCase()+' DELETED',
                            'success'
                        )
                        let elmId = $(".table").attr("id");
                        let id = "#"+elmId;
                        $(id).DataTable().ajax.reload();
                    } else if (response.success === 2) {
                        failAlert(key, 'You can\'t delete this data.')
                    } else {
                        failAlert(key);
                    }
                },
                error: function (response) {
                    if (response.status === 300) {
                        failAlert(key, 'You cannot delete this data due to relationship!')
                    }
                }
            })
        }

    })
}



function failAlert(key, custom = null) {
    let message = "Fail to delete";

    if (custom) {
        message = custom
    }

    swal.fire({
        html: message,
        title: "Fail",
        icon: "error",
        showCancelButton: false,
        confirmButtonText: "Close",
    })
}

function customAlertMsg(msg, type = "info") {
    swal.fire({
        icon: type,
        text: msg,
        width: 'auto',
        showCancelButton: false,
        confirmButtonText: "Close",
        title: type.toUpperCase(),
        confirmButtonColor: "#47a1de",
    })
}

function warnEvent(msg) {
    swal.fire({
        icon:'warning',
        text:msg,
        width:'32em',
        showCancelButton:false,
        confirmButtonText: "Close",
        confirmButtonColor: "#47a1de",
        allowOutsideClick:false,
        timerProgressBar:true,
        timer:'10000',
    })
}

function errorEvent(msg) {
    swal.fire({
        icon:'error',
        text:msg,
        width:'32em',
        showCancelButton:false,
        confirmButtonText: "Close",
        confirmButtonColor: "#47a1de",
        allowOutsideClick:false,
        timerProgressBar:true,
        timer:'10000',
    })
}


function successEvent(msg) {
    swal.fire({
        icon:'success',
        text:msg,
        width:'32em',
        showCancelButton:false,
        confirmButtonText: "Close",
        confirmButtonColor: "#47a1de",
        allowOutsideClick:false,
        timerProgressBar:true,
        timer:'10000',
    })
}
