
$(function (edit = 'Edit', del = 'Delete') {
    return $('#mainCategoryDataTable').DataTable({
        bSort: false,
        ordering: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        targets: 'no-sort',
        destroy: true,
        responsive: false,
        ajax: {
            type: 'post',
            url: '/ajax/main_category',
        },
        columns: [
            { data: 'name', name: 'name', searchable: true, render: $.fn.dataTable.render.text() },
            {
                data: 'action',
                name: 'action',
                searchable: true,
                render: function (data, type, row) {
                    let buttons = '';

                    if (data.canEdit === true) {
                        buttons += `<a class="btn btn-warning btn-sm text-white" href="main-category/${row.uuid}/edit"
                            data-bs-toggle="tooltip" data-bs-placement="left" title="${edit}">
                            <i class="fa-regular fa-pen-to-square"></i></a>`;
                    }

                   if (data.canDelete === true) {
                        buttons += `&nbsp;<a class="btn btn-sm btn-danger text-white" href="#" onclick="deleteByKey(this, 'equipment-type')" id="${row.uuid}"
                            data-bs-toggle="tooltip" data-bs-placement="right" title="${del}"><i class="fa-regular fa-trash-can"></i></a>`;
                    }
                    
                    return buttons;
                }
            }
        ],
        "columnDefs": [
            {"searchable": false}
        ],
    });
});
