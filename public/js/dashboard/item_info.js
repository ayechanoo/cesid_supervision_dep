
$(function (edit = 'Edit', del = 'Delete') {
    return $('#itemInfosDataTable').DataTable({
        bSort: false,
        ordering: false,
        processing: true,
        serverSide: true,
        deferRender: true,
        targets: 'no-sort',
        destroy: true,
        responsive: false,
        ajax: {
            type: 'post',
            url: '/ajax/item_infos',
        },
        columns: [
            { data: 'name', name: 'name', searchable: true, render: $.fn.dataTable.render.text() },
            { data: 'sub_category_type', name: 'sub_category_type', searchable: true, render: $.fn.dataTable.render.text() },
            { data: 'category_type', name: 'category_type', searchable: true, render: $.fn.dataTable.render.text() },
            // {
            //     data: 'action',
            //     name: 'action',
            //     searchable: true,
            //     render: function (data, type, row) {
            //         let buttons = '';

            //         if (data.canEdit === true) {
            //             buttons += `<a class="btn btn-warning btn-sm text-white" href="item_info/${row.uuid}/edit"
            //                 data-bs-toggle="tooltip" data-bs-placement="left" title="${edit}">
            //                 <i class="fa-regular fa-pen-to-square"></i></a>`;
            //         }

            //        if (data.canDelete === true) {
            //             buttons += `&nbsp;<a class="btn btn-sm btn-danger text-white" href="#" onclick="deleteByKey(this, 'proposal')" id="${row.uuid}"
            //                 data-bs-toggle="tooltip" data-bs-placement="right" title="${del}"><i class="fa-regular fa-trash-can"></i></a>`;
            //         }
                    
            //         return buttons;
            //     }
            // }
        ],
        "columnDefs": [
            {"searchable": false}
        ],
    });
});
