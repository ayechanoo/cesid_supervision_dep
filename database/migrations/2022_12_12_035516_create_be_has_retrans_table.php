<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('be_has_retrans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->integer('reviewed_by')->default(1);
            $table->unsignedBigInteger('in_transaction_id')->nullable();
            $table->foreign('in_transaction_id')
                ->references('id')
                ->on('in_transactions')
                ->onDelete('cascade');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('be_has_retrans');
    }
};
