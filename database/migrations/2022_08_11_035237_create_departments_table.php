<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('name_eng')->nullable();
            $table->string('name_mm')->nullable();
            $table->unsignedBigInteger('ministry_id')->nullable();
            $table->Integer('parent_id')->nullable();
            $table->Integer('level')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('ministry_id')
                    ->references('id')
                    ->on('ministries')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
};
