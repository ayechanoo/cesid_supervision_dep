<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_sub_categories', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->text('name');
            $table->unsignedBigInteger('item_category_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('item_category_id')
                    ->references('id')
                    ->on('item_categories')
                    ->onDelete('cascade');
        });
    }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_sub_categories');
    }
}
