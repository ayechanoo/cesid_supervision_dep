<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demand_items', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->text('name')->nullable();
            $table->text('specification')->nullable();
            $table->unsignedBigInteger('item_info_id')->nullable();
            $table->unsignedBigInteger('equipment_type_id')->nullable();
            $table->unsignedBigInteger('proposal_id')->nullable();
            $table->string('qty')->nullable();
            $table->string('unit_price')->nullable();
            $table->string('unit')->nullable();
            $table->string('total')->nullable();
            $table->text('justification')->nullable();

            $table->unsignedBigInteger('in_transaction_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('item_info_id')
                    ->references('id')
                    ->on('item_infos')
                    ->onDelete('cascade');
            
            $table->foreign('equipment_type_id')
                    ->references('id')
                    ->on('equipment_types')
                    ->onDelete('cascade');
                    
            $table->foreign('proposal_id')
                ->references('id')
                ->on('proposals')
                ->onDelete('cascade');

            $table->foreign('in_transaction_id')
                ->references('id')
                ->on('in_transactions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demand_items');
    }
}
