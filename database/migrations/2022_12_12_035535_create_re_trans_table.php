<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_trans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('be_has_retrans_id')
                ->references('id')
                ->on('be_has_retrans')
                ->onDelete('cascade');
            $table->unsignedBigInteger('demand_item_id')->nullable();
            $table->foreign('demand_item_id')
                    ->references('id')
                    ->on('demand_items')
                    ->onDelete('cascade');
            $table->string('qty')->nullable();
            $table->string('total')->nullable();
        
            $table->timestamps();
            $table->softDeletes();
        });
    }

    


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_trans');
    }
};
