<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use Carbon\Carbon;

class SupervisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->proposalTypesSeeding();
        $this->subProposalSeeding();
        $this->equipmentTypeSeeding();
        $this->itemCategorySeeding();
        $this->itemSubCategory();
        $this->budgetYearSeeding();
    }

    public function budgetYearSeeding() {
        $data = config('mining.budgetyears');
        $model = new \App\Models\BudgetYear();

        foreach($data as $item) {
        
                $exist = $model::where('from',$item['from'])->where('to',$item['to'])->first();
                if(!$exist) {
                    $model::create([
                        'uuid' =>  Str::uuid()->toString(),
                        'from' => Carbon::parse($item['from']),
                        'to' => Carbon::parse($item['to'])
                    ]);
                }
        
            
        }
        
    }



    public function itemSubCategory() {
        $data = config('mining.item_sub_category');
        $model = new \App\Models\ItemSubCategory;

        foreach($data as $item) {

            
            $item_category = \App\Models\ItemCategory::where('name',strtolower($item['item_category']))->first();

            

            if($item_category) {
                $exist = $model::where('name',strtolower($item['name']))->first();
                if(!$exist) {
                    $model::create([
                        'uuid' =>  Str::uuid()->toString(),
                        'name' => strtolower($item['name']),
                        'item_category_id' => $item_category->id
                    ]);
                }
            }
            
        }

    }

    public function itemCategorySeeding(){
        $data = config('mining.item_category');

        
        foreach($data as $item) {

            $exist = \App\Models\ItemCategory::where('name', strtolower($item))->first();
           

            if(!$exist) {

                \App\Models\ItemCategory::create([
                    'uuid' =>  Str::uuid()->toString(),
                    'name' => strtolower($item)
                ]);

            }

        }
         
    }

    public function equipmentTypeSeeding() {
        $data = config('mining.equipment_type');
        $this->storeName(new \App\Models\EquipmentType, $data);
    }

    public function subProposalSeeding() {
        $data = config('mining.sub_proposal');
        $model = new \App\Models\Proposal();
        foreach($data as $item) {
            $exist = $model::where('name',strtolower($item))->first();
            if(!$exist) {
                $model::create([
                    'uuid' =>  Str::uuid()->toString(),
                    'name' => strtolower($item),
                    'proposal_type_id' => 1
                ]);
            }
            
        }
    }

    public function proposalTypesSeeding() {

        $types = config('mining.main_proposal');
        
        $model = new \App\Models\ProposalType();
        $this->storeName($model, $types);
        
    }

    public function storeName(Model $model, $data) {
        foreach($data as $item) {
            $exist = $model::where('name',strtolower($item))->first();
            if(!$exist) {
                $model::create([
                    'uuid' =>  Str::uuid()->toString(),
                    'name' => strtolower($item)
                ]);
            }
            
        }
    }
    
}
