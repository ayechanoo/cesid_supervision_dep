<?php

namespace Database\Seeders;

use File;
use Illuminate\Database\Seeder;
use App\Models\Ministry;

class MinistrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ministries = $this->import_CSV('Ministry & Orginazation');

        foreach ($ministries as $val) {
            $ministry = Ministry::create([
                'name_eng' => $val[0],
                'name_mm' => $val[1],
                'level' => $val[2]
             ]);
        }
    }

    public function import_CSV($filename, $delimiter = ',')
    {
        $file = File::get("database/data/$filename.csv");
        if (!$file) {
            return false;
        }

        $header = null;
        $result = array();

        if (($handle = fopen("database/data/$filename.csv", 'r'))!== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $result[] = $data;
            }
        }


        return $result;
    }
}
