<?php

namespace Database\Seeders;

use File;
use Illuminate\Database\Seeder;
use Database\Seeders\MinistrySeeder;
use App\Models\Ministry;
use App\Models\Department;

class DepartmentSeeder extends Seeder
{
    public $ministry;
    public function __construct(MinistrySeeder $ministry)
    {
        $this->ministry = $ministry;
    }

    public function run()
    {
        $ministries = $this->import_CSV('Ministry & Orginazation');
        $departments = $this->import_CSV('Department & Job');
        foreach ($departments as $k => $department) {
            if ($k == 0) {
                continue;
            }
            $ministry_name_mm = $department[2];

            $ministryList = array_filter($ministries, function ($item) use ($ministry_name_mm) {
                return $ministry_name_mm == $item[1];
            });
            //dd($ministryList);

            if (count($ministryList) > 0) {
                foreach ($ministryList as $s) {
                    $minisrty_name_mm = $s[1];
                }



                $final_ministry_id = Ministry::where('name_mm',$minisrty_name_mm)->first()->id;
                $department = Department::create(
                    [
                        'name_eng' => $department[0],
                        'name_mm' => $department[1],
                        'ministry_id' => $final_ministry_id,
                        'level' => $department[3]
                    ]
                    );
                    

            }
        }
    }

    public function import_CSV($filename, $delimiter = ',')
    {
        $file = File::get("database/data/$filename.csv");
        if (!$file) {
            return false;
        }

        $header = null;
        $result = array();

        if (($handle = fopen("database/data/$filename.csv", 'r'))!== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $result[] = $data;
            }
        }


        return $result;
    }


}
