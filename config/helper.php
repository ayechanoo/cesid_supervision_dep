<?php

return [
    'units'=> [
        'No' => 1,
        'Nos' => 2,
        'Set' => 3,
        'Sets' => 4,
        'Lot' => 5,
        'Pack' => 6
    ],
    'reviewed_by' => [
        1 => 'TC Dep',
        2 => 'ဒုဝန်ကြီးဌာန',
        3 => 'ဝန်ကြီးဌာန',
    ]
];