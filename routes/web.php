<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\MinistryController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\SuperVisionController;
use App\Http\Controllers\DemandController;
use App\Http\Controllers\SvController;
use App\Http\Controllers\BudgetController;
use App\Http\Controllers\EquipmentTypeController;
use App\Http\Controllers\ProposalTypeController;
use App\Http\Controllers\ProposalController;
use App\Http\Controllers\MainCategoryController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\ItemInfoController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\RecommendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function() {
    return redirect()->route('equipment-type.index');
});

Route::get('/tem',function() {
    return view('layouts.template');
});


Route::post('/ajax-demand-store',[DemandController::class,'store'])->name('ajax-demand-store');


Route::prefix("admin")->group(function () {
    Route::resource('/ministry', MinistryController::class)->except('show');
    Route::resource('/department', DepartmentController::class)->except('show');
});

Route::prefix("supervision")->group(function () {

    Route::get('index',[SuperVisionController::class,'index'])->name('supervision.index');
    Route::get('sa-1001',[SuperVisionController::class,'exportSaone'])->name('supervision.saone');
    Route::post('importexcel', [SuperVisionController::class, 'importExcel'])->name('excel.import');
    Route::post('demandItem-excel-store', [SuperVisionController::class, 'demandStoreWithExcel'])->name('demand-excel.store');
    Route::post('demandItem-excel-export',[SuperVisionController::class,'demandItemExcelExport'])->name('demand-excel.export');
    Route::get('demandItem-excel-download',[SuperVisionController::class,'downloadExcelAllQtyVsDeps'])->name('download.excel');
    Route::get('demandItem-excel-download',[SuperVisionController::class,'downloadExcelAllQtyVsDeps'])->name('download.excel');

    Route::get('demandItem-excel-sa-one-download',[SuperVisionController::class,'downloadExcel'])->name('download.sa-one.excel');

});


Route::prefix("sv")->group(function () {

     Route::get('/',[SuperVisionController::class,'index'])->name('sv.index');
     
     Route::post('/sv-upload',[SuperVisionController::class,'importExcel'])->name('sv.upload');

     Route::post('/store/data',[SvController::class,'storeData'])->name('sv.store');
    // Route::post('importexcel', [SuperVisionController::class, 'importExcel'])->name('excel.import');
    // Route::post('demandItem-excel-store', [SuperVisionController::class, 'demandStoreWithExcel'])->name('demand-excel.store');
    // Route::post('demandItem-excel-export',[SuperVisionController::class,'demandItemExcelExport'])->name('demand-excel.export');
    // Route::get('demandItem-excel-download',[SuperVisionController::class,'downloadExcel'])->name('download.excel');

});

Route::prefix("manual")->group(function () {

    Route::get('/', [DemandController::class,'create'])->name('manual.create');
    Route::get('re/excel-import-form', [RecommendController::class,'excelImportForm'])->name('manual.re.excel-import-form');
    Route::post('re/recommend-import-form',[RecommendController::class,'recommendImport'])->name('manual.re.excel-create-store');

});
Route::resource('budget',BudgetController::class);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix("setup")->group(function () {

Route::resource('equipment-type',EquipmentTypeController::class); 
Route::resource('main-category',MainCategoryController::class); 
Route::resource('sub-category',SubCategoryController::class); 
Route::resource('proposal-type',ProposalTypeController::class);
Route::resource('item_info',ItemInfoController::class);
Route::resource('main-proposal',ProposalController::class,['names' => 'main-proposal'])   ;

});

Route::prefix('/ajax')->group(function () {

    Route::post('/equip-type', [EquipmentTypeController::class,'getEquipmentTypes'])->name('ajax.equip_type');
    Route::post('/main_category', [MainCategoryController::class,'getMainCategories'])->name('ajax.main_category');
    Route::post('/sub_category', [SubCategoryController::class,'getSubCategories'])->name('ajax.sub_category');
    Route::post('/item_infos', [ItemInfoController::class,'getItemInfos'])->name('ajax.item_infos');
    Route::post('/propo-type', [ProposalTypeController::class,'getProposalTypes'])->name('ajax.propo_type');
    Route::post('/propo', [ProposalController::class,'getProposals'])->name('ajax.proposel');
    Route::post('/depsByMinId', [DepartmentController::class,'depsByMinId'])->name('ajax.deps_by_minid');
    Route::post('/inTransByBudgetDep', [DemandController::class,'filterWithBudgetDep'])->name('ajax.intrans_by_budget_dep');

    Route::post('/categoriesByminUUid', [MainCategoryController::class,'categoriesByMinUuid'])->name('ajax.categories_by_min_uuid');
    
});

Route::prefix("report")->group(function () {

    Route::get('/filter/budget-dep',[ReportController::class,'reportsByBYandDep'])->name('report.filter.budget_dep');

    Route::post('/filter/budget-dep/collection',[ReportController::class,'collectionByBYandDep'])->name('report.filter.budget_dep.collection');

    Route::get('/qties_with_all_deps', [SuperVisionController::class,'reportTotalWithQty'])->name('report.qties_with_all_deps');

    Route::get('/report/deps/by_minid/create',[RecommendController::class,'depRecordsByMiniCreate'])->name('report.deps_by_minid.create');

    Route::post('/report/deps/by_minid/preview',[ReportController::class,'depRecordsByMiniPreview'])->name('report.deps_by_minid.preview');

});
