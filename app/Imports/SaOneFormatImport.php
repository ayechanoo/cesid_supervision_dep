<?php

namespace App\Imports;

use App\Models\VehicleIssueInfo;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Models\ProposalType;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Database\Eloquent\Model;


class SaOneFormatImport implements ToCollection, WithHeadingRow,WithCalculatedFormulas
{
    protected $row_count = 7;
    protected $errors = [];
    protected $valid_data = [];
    protected $proposal_disc;
    protected $equipment_dic;
    protected $item_disc;
    protected $final_data = [];


    protected $headingRow = 7;

    public function __construct() {

        
        $this->proposal_dic = $this->preparedProposalandSubDic();

        // dd($this->proposal_dic);
        $this->item_dic = $this->preparedItemDic();
       
        // dd($this->item_dic);
    }
        
    public function collection(Collection $collection)
    {
         //dd($collection);
        $new_colllection = [
            'mainproposal' => [],
            'subproposal' => [],
            'equpiment' => [],
            'item_category' => [],
            'item_sub_category' => [], 
        ];

        foreach($collection->toArray() as $key => $row) {

            
        
            array_push($new_colllection['equpiment'], $row[4]);
        }

       

        $this->equipment_dic = $this->preparedModelDic(new \App\Models\EquipmentType, $new_colllection['equpiment']);

        // dd($this->equpiment_dic);

        $newData = [];
        $this->row_count += 1;
        $last_spec= 0;
        foreach($collection->toArray() as $key => $row) {
            

           // if($this->row_count == 24) {
           //  dd($row);
           // }
        
            
            $error_idx = $this->row_count;
            $office_status = preg_match('/^[a-z A-Z]+$/', $row[3]);
            $equip_status = preg_match('/^[(0-9) a-z A-Z]+$/', $row[4]);
            

            if($office_status && $equip_status) {

                if($this->row_count == 37) {
                   // dd($row);
                }
                
                $newData['proposal_validation'][$this->row_count] = $this->subAndProposalValidation($row, $this->row_count);
                $newData['equipment'][$this->row_count] = $this->equipValidation($row, $this->row_count);
                $newData['item_Validation'][$this->row_count] = $this->itemValidation($row, $this->row_count);
                $newData['demand_item'][$this->row_count] = $this->existDataVal($row);
                
            }else{
                
                end($this->valid_data);

                $key = key($this->valid_data);

                //dd($this->valid_data[$key]);

                if(!isset($this->valid_data[$key][8]['value'])){
                    dd($this->valid_data[$key]);
                }
                
                $this->valid_data[$key][8]['value'] .= $row[8] ?? '';
            }
            

            $this->row_count +=1 ; 
           
        }

        

    }



    public function preparedItemDic() {
        $data = \DB::table('item_infos')
        ->leftJoin('item_sub_categories','item_sub_categories.id','item_infos.item_sub_category_id')
        ->rightJoin('item_categories','item_infos.item_category_id','item_categories.id')
        ->selectRaw('Lower(item_infos.name) as item_name, item_infos.id as item_id, Lower(item_sub_categories.name) as sub_categ_name, item_sub_categories.id as item_sub_category_id, item_categories.id as item_main_category_id, Lower(item_categories.name) as main_categ_name')
                    ->get();
      
         
        // $data = \DB::table('item_categories')
        //         ->leftjoin('item_sub_categories','item_categories.id','item_sub_categories.item_category_id')
        //         ->leftjoin('item_infos','item_infos.item_sub_category_id','item_sub_categories.id')
        //         ->selectRaw('item_infos.name as item_name, item_infos.id as item_id, Lower(item_sub_categories.name) as sub_categ_name, item_sub_categories.id as item_sub_category_id, item_categories.id as item_main_category_id, Lower(item_categories.name) as main_categ_name')
        //         ->get();
        
        // dd($data);
        $collection = [];

        

        foreach($data as $k => $i) {

          if(isset($collection[$i->main_categ_name])) {


            if(!is_null($i->sub_categ_name)) {

                if(isset($collection[$i->main_categ_name][$i->sub_categ_name])) {

                    

                    $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name]['name'] = ucwords($i->item_name);
                    $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name] ['id']= $i->item_id;

                }else{
                    
                    $collection[$i->main_categ_name][$i->sub_categ_name]['name'] = ucwords($i->sub_categ_name);
                    $collection[$i->main_categ_name][$i->sub_categ_name]['id'] = $i->item_sub_category_id;
                    $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name]['name'] = ucwords($i->item_name);
                    $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name] ['id']= $i->item_id;
                }
                
            }else{
                $collection[$i->main_categ_name][$i->item_name]['name'] = ucwords($i->item_name);
                $collection[$i->main_categ_name][$i->item_name]['id'] = $i->item_id;
                
            }

          }else{

            $collection[$i->main_categ_name]['name'] = $i->main_categ_name;
            $collection[$i->main_categ_name]['id'] = $i->item_main_category_id;

            if(!is_null($i->sub_categ_name)) {
                $collection[$i->main_categ_name][$i->sub_categ_name]['name'] = ucwords($i->sub_categ_name);
                $collection[$i->main_categ_name][$i->sub_categ_name]['id'] = $i->item_sub_category_id;
                $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name]['name'] = ucwords($i->item_name);
                $collection[$i->main_categ_name][$i->sub_categ_name][$i->item_name] ['id']= $i->item_id;
            }else{
                $collection[$i->main_categ_name][$i->item_name]['name'] = ucwords($i->item_name);
                $collection[$i->main_categ_name][$i->item_name]['id'] = $i->item_id;
                
            }
            
          }
            
        }

       return $collection;
    }

    public function preparedProposalandSubDic() {
        
        $data = \DB::table('proposals')
                ->join('proposal_types','proposals.proposal_type_id','proposal_types.id')
                ->selectRaw('Lower(proposal_types.name) as parent_name,proposal_types.id as parent_id , proposals.id as child_id, Lower(proposals.name) as child_name')
                ->get();
        
        $collection = [];
        
        foreach($data as $k => $i) {

            if(isset($collection[$i->parent_name])) {

                $collection[$i->parent_name][$i->child_name]['name'] =ucwords($i->child_name);
                $collection[$i->parent_name][$i->child_name]['id'] =$i->child_id;

            }else{
                $collection[$i->parent_name]['name'] = ucwords($i->parent_name);
                $collection[$i->parent_name]['id'] = $i->parent_id;
                $collection[$i->parent_name][$i->child_name]['name'] =ucwords($i->child_name);
                $collection[$i->parent_name][$i->child_name]['id'] =$i->child_id;
                
            }
            
            
        }

        return $collection;
    }

    public function existDataVal($row) {

        
       
        $columns = [8,9,10,11,12,19];

        foreach($columns as $col) {

            $regex = preg_match("/[-]/", $row[$col]);

            
            if(!is_null($row[$col]) || $row[$col] != '') {
                $index = strtolower($row[$col]);

                
                
                $this->valid_data[$this->row_count][$col]['type'] = true;
                $this->valid_data[$this->row_count][$col]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][$col]['col'] = $col;

                if($col == 9 && is_null($row[$col])) {
                     $this->valid_data[$this->row_count][$col]['value']=  1;
                }

                if($col == 11  && is_null($row[$col])) {
                    $this->valid_data[$this->row_count][$col]['value']=  $row[12];
                }

                
                
                if($col == 12 || $col== 8) {
                    
                    $this->valid_data[$this->row_count][$col]['value']=  $row[$col];
                   
                    
                }else{
                    $this->valid_data[$this->row_count][$col]['value']=  $row[$col];
                }

                
    
            }else{

                if($col == 8) {
                   $this->valid_data[$this->row_count][$col]['type'] = true;
                     $this->valid_data[$this->row_count][$col]['value'] = trim($row[8]);
                }else{
                    $this->valid_data[$this->row_count][$col]['type'] = false;
                     $this->valid_data[$this->row_count][$col]['value'] = null;
                    $this->errors[$this->row_count][$col]['status'] = 404; 
                    if($col == 19) {
                        $this->errors[$this->row_count][$col]['message'] = "Please fill information(optional)"; 
                    }else{
                        $this->errors[$this->row_count][$col]['message'] = "Please fill information"; 
                    }
                }
                
               
            }

        }

     //dd($this->valid_data); 
        
        
    }

    public function equipValidation($row,$row_count) {
       //dd($this->equipment_dic);
        if($row[4]) {
            $index = strtolower($row[4]);
            // dd($index);
            if(isset($this->equipment_dic[$index])) {

                $this->valid_data[$this->row_count][4]['type'] = true;
                $this->valid_data[$this->row_count][4]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][4]['col'] = 4;
                $this->valid_data[$this->row_count][4]['name'] = ucwords($this->equipment_dic[$index][0]['name']);
                $this->valid_data[$this->row_count][4]['id'] = $this->equipment_dic[$index][0]['id'];

            }else{

                $this->valid_data[$this->row_count][4]['type'] = false;
                $this->valid_data[$this->row_count][4]['name'] = $index;
                $this->errors[$this->row_count][4]['status'] = 403; 
                $this->errors[$this->row_count][4]['message'] = "{$row[4]} is not found in source! Please Create or recheck again!"; 

            }
            

        }else{
            $this->valid_data[$this->row_count][4]['type'] = false;
            $this->valid_data[$this->row_count][4]['name'] = $row[4];
            $this->errors[$this->row_count][4]['status'] = 404; 
            $this->errors[$this->row_count][4]['message'] = "Please fill information"; 
        }

    }


    public function subAndProposalValidation($row) {

        
        
        $column = [2,3];

        $data = [ 
            'excel_line' => $this->row_count,
            'type' => 'valid',
            'message' => '',
            
        ];

        

        if($row[2]) {
            
            $index = strtolower($row[2]);

            if(count($this->proposal_dic) > 0 && array_key_exists($index, $this->proposal_dic)) {
                // dd($this->proposal_dic[$index]);
                $this->valid_data[$this->row_count][2]['type'] = true;
                $this->valid_data[$this->row_count][2]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][2]['col'] = 2;
                $this->valid_data[$this->row_count][2]['name'] = ucwords($this->proposal_dic[$index]['name']);
                $this->valid_data[$this->row_count][2]['id'] = $this->proposal_dic[$index]['id'];
            }else{
                $this->valid_data[$this->row_count][2]['type'] = false;
                $this->valid_data[$this->row_count][2]['name'] = $index;
                $this->errors[$this->row_count][2]['status'] = 403; 
                $this->errors[$this->row_count][2]['message'] = "{$row[2]} is not found in source! Please Create or recheck again!"; 
            }

        }else{
            $this->valid_data[$this->row_count][2]['type'] = false;
            $this->valid_data[$this->row_count][2]['name'] = $row[2];
            $this->errors[$this->row_count][2]['status'] = 404; 
            $this->errors[$this->row_count][2]['message'] = "Please Fill data"; 
        }

        if($row[3]) {
            $project = strtolower($row[2]);
            $subproject = strtolower($row[3]);
            if(isset($this->proposal_dic[$project][$subproject])) {
                $this->valid_data[$this->row_count][3]['type'] = true;
                $this->valid_data[$this->row_count][3]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][3]['col'] = 3;
                $this->valid_data[$this->row_count][3]['name'] = ucwords($this->proposal_dic[$project][$subproject]['name']);
                $this->valid_data[$this->row_count][3]['id'] = $this->proposal_dic[$project][$subproject]['id'];
            }else{
                $this->valid_data[$this->row_count][3]['type'] = false;
                $this->valid_data[$this->row_count][3]['name'] = $row[3];
                $this->errors[$this->row_count][3]['status'] = 403; 
                $this->errors[$this->row_count][3]['message'] = "{$row[3]} is not found in source! Please Create or recheck again!"; 
            }
        }else{
            $this->valid_data[$this->row_count][3]['type'] = false;
            $this->valid_data[$this->row_count][3]['name'] = $row[3];
            $this->errors[$this->row_count][3]['status'] = 404; 
            $this->errors[$this->row_count][3]['message'] = "Please Fill data"; 
        }

    }

    public function itemValidation($row) {

        
        
        $column = [5,6,7];

        

        if($row[5]) {
            $index = strtolower($row[5]);
                    
            
            if(isset($this->item_dic[$index])) {
                $this->valid_data[$this->row_count][5]['type'] = true;
                $this->valid_data[$this->row_count][5]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][5]['col'] = 5;
                $this->valid_data[$this->row_count][5]['name'] = ucwords($this->item_dic[$index]['name']);
                $this->valid_data[$this->row_count][5]['id'] =$this->item_dic[$index]['id'];
                
            }else{
                $this->valid_data[$this->row_count][5]['type'] = false;
                $this->valid_data[$this->row_count][5]['name'] = $row[5];
                $this->errors[$this->row_count][5]['status'] = 403; 
                $this->errors[$this->row_count][5]['message'] = "{$row[5]} welcomehow is not found in source! Please Create or recheck again!"; 
            }

        }else{
            $this->valid_data[$this->row_count][5]['type'] = false;
            $this->valid_data[$this->row_count][5]['name'] = $row[5];
            $this->errors[$this->row_count][5]['status'] = 404; 
            $this->errors[$this->row_count][5]['message'] ="Please Fill data"; 
        }


        if($row[6]) {
            
            $project = strtolower($row[5]);
            $subproject = strtolower($row[6]);

          

            if(isset($this->item_dic[$project][$subproject])) {
                $this->valid_data[$this->row_count][6]['type'] = true;
                $this->valid_data[$this->row_count][6]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][6]['col'] = 6;
                $this->valid_data[$this->row_count][6]['name'] = ucwords($this->item_dic[$project][$subproject]['name']);
                $this->valid_data[$this->row_count][6]['id'] = $this->item_dic[$project][$subproject]['id'];
            }else{
                $this->valid_data[$this->row_count][6]['type'] = false;
                $this->valid_data[$this->row_count][6]['name'] = $subproject;
                $this->errors[$this->row_count][6]['status'] = 403; 
                $this->errors[$this->row_count][6]['message'] = "{$row[6]} is not found in source! Please Create or recheck again!"; 
            }
        }
        else{
            $this->valid_data[$this->row_count][6]['type'] = false;
            $this->valid_data[$this->row_count][6]['name'] = ($row[6] == '') ? null : strtolower($row[6]);
            $this->errors[$this->row_count][6]['status'] = 500; 
            $this->errors[$this->row_count][6]['message'] = "Please Fill data"; 
        }

        if($row[7]) {

            
            $categ = strtolower($row[5]);
           
            $sub_categ = strtolower($row[6]);
            $item = strtolower($row[7]);

            
            
            if(isset($this->item_dic[$categ][$sub_categ][$item])) {
               
                $this->valid_data[$this->row_count][7]['type'] = true;
                $this->valid_data[$this->row_count][7]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][7]['col'] = 7;
                $this->valid_data[$this->row_count][7]['name'] = ucwords($this->item_dic[$categ][$sub_categ][$item]['name']);
                $this->valid_data[$this->row_count][7]['id'] = $this->item_dic[$categ][$sub_categ][$item]['id'];
            }elseif(isset($this->item_dic[$categ][$item])){
               
                $this->valid_data[$this->row_count][7]['type'] = true;
                $this->valid_data[$this->row_count][7]['row'] = $this->row_count;
                $this->valid_data[$this->row_count][7]['col'] = 7;
                $this->valid_data[$this->row_count][7]['name'] = ucwords($this->item_dic[$categ][$item]['name']);
                $this->valid_data[$this->row_count][7]['id'] = $this->item_dic[$categ][$item]['id'];
            }
            else{
               
               // dd($this->item_dic);
                $this->valid_data[$this->row_count][7]['type'] = false;
                $this->valid_data[$this->row_count][7]['name'] = $item;
                $this->errors[$this->row_count][7]['status'] = 403; 
                $this->errors[$this->row_count][7]['message'] = "{$row[7]} is not found in source! Please Create or recheck again!"; 
            }

            
        }else{
            $this->valid_data[$this->row_count][7]['type'] = false;
            $this->valid_data[$this->row_count][7]['name'] = $row[7];
            $this->errors[$this->row_count][7]['status'] = 500; 
            $this->errors[$this->row_count][7]['message'] = "Please Fill data"; 
        }


        
    }
    
    public function preparedModelDic(Model $model,$list) {
        return $model::selectRaw('lower(name) as name , id')->whereIn('name',$list)->get()->groupBy('name')->toArray();
    }

  


    public function getResults() 
    {
        return [
            'valid_data' => $this->valid_data,
            'errors' => $this->errors
        ] ;
    }


}
