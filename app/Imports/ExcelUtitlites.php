<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExcelUtitlites implements ToCollection,WithEvents,WithHeadingRow
{
    public $sheetNames;
    public $sheetData;
    public $valid_status;
    public $row = 6;
    
    public function __construct(){
        $this->sheetNames = [];
        $this->sheetData = [];
        $this->valid_status = false;
    }


    public function collection(Collection $collection)
    {
        $data = $collection->toArray();
        
        $first_row = array_values($data[0]);
        

        $containsSearch  = !array_diff($first_row, [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]);
       
        $this->valid_status = $containsSearch;
        
        
        
        $this->sheetData[] = $collection;
    }

    public function registerEvents(): array
    {
        return [
            BeforeSheet::class => function(BeforeSheet $event) {
                
                $this->sheetNames[] = $event->getSheet()->getDelegate()->getTitle();

            } 
        ];
    }

    public function getSheets() {
        return $this->sheetData;
    }

    public function getSheetNames() {
        return $this->sheetNames;
    }

    public function getValidStatus() {
        return $this->valid_status;
    }


    public function headingRow(): int
    {
        return 6;
    }


    // public function sheets(): array
    // {
    //     return [
    //        0 => new SaOneFormatImport()
    //     ];
    // }
}
