<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ReExcelImport implements ToCollection
{
    protected $row_count = 7;
    protected $errors = [];
    protected $valid_data = [];
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $this->row_count += 1;
        foreach($collection->toArray() as $key => $row) {
            $error_idx = $this->row_count;
            $office_status = preg_match('/^[a-z A-Z]+$/', $row[3]);
            $equip_status = preg_match('/^[(0-9) a-z A-Z]+$/', $row[4]);

            if($office_status && $equip_status) {
                $this->existDataVal($row, $this->row_count);
            }else{

            }
            $this->row_count +=1 ; 
        }


    }


    public function existDataVal($row) {

        
        $columns = [13,14,15];

        foreach($columns as $col) {

            $regex = preg_match("/[-]/", $row[$col]);

            
            if(!is_null($row[$col]) || $row[$col] != '') {
                $this->valid_data[$this->row_count][$col]['value']=  $row[$col];
                   
    
            }else{
                $this->valid_data[$this->row_count][$col]['type'] = false;
                 $this->valid_data[$this->row_count][4]['value'] = null;
                $this->errors[$this->row_count][$col]['status'] = 404; 
                if($col == 19) {
                    $this->errors[$this->row_count][$col]['message'] = "Please fill information(optional)"; 
                }else{
                    $this->errors[$this->row_count][$col]['message'] = "Please fill information"; 
                }
               
            }

        }

     //dd($this->valid_data); 
        
        
    }

    public function getResults() 
    {
        return [
            'valid_data' => $this->valid_data,
            'errors' => $this->errors
        ] ;
    }
}
