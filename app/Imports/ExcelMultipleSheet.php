<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;
use App\Imports\SaOneFormatImport;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ExcelMultipleSheet implements WithMultipleSheets
{
    
    protected $all = [];
    public function __construct($name) {
        $this->all[$name] = new VTwoSaOneFormat();
        //$this->all[$name] = new SaOneFormatImport();
    }
  
  

    /**
    * @param Collection $collection
    */
    public function sheets(): array
    {
        
      return $this->all;
            
    }




}
