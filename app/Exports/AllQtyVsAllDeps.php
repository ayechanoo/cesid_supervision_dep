<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use App\Repositories\ItemInfo\ItemInfoRepositoryInterface;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use DB;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class AllQtyVsAllDeps extends DefaultValueBinder implements FromView,WithColumnWidths
{
    protected $item_info_repo ;
    public function __construct(ItemInfoRepositoryInterface $item_info_repo) {
        $this->item_info_repo = $item_info_repo;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {

        $sub_categories = $this->item_info_repo->allCategoryList();

        
        $case_query = '';

        foreach($sub_categories as $k ) {
            foreach($k as $v => $sub) {
            $case_query .= ",sum(case when ISC_name = '".$sub->Item_Category."' then total else '-' end) as '".$sub->Item_Category."_qty"."',sum(case when ISC_name = '".$sub->Item_Category."' then total_amt else '-' end) as '".$sub->Item_Category."_amount"."'";
            }
        }

        

        $raw_query = "with base_query as(select sum( case when tbl.qty = '' then 1 else tbl.qty end ) as total, sum(tbl.total) as total_amt, name_eng,name_mm ,ISC.name as ISC_name from (select DI.name as demand_name,DI.in_transaction_id as demand_trans_id, DI.qty,DI.total, item_infos.item_sub_category_id 
        from demand_items as DI 
        join item_infos
        on item_infos.id = DI.item_info_id
        
        ) as tbl join item_sub_categories as ISC on ISC.id = tbl.item_sub_category_id 
        join in_transactions as Ts on Ts.id = tbl.demand_trans_id
        right join departments as Ds on Ds.id = Ts.department_id group By name_eng,name_mm, ISC.name)

        select distinct(name_mm)$case_query from base_query group By name_mm";

        

       $collection = \DB::select($raw_query);

       return view('exports.all_qty_vs_deps',compact('collection','sub_categories'));
        
    }


    
    public function columnFormats(): array
    {
       return [];
    }

    public function columnWidths(): array
    {
        return [
           
            'A' => 45,          
        ];
    }
}
