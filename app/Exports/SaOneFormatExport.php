<?php

namespace App\Exports;
use App\Models\DemandItem;

use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use App\Models\Department;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use App\Models\BudgetYear;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class SaOneFormatExport extends DefaultValueBinder implements WithCustomValueBinder,FromView,WithColumnWidths,WithColumnFormatting
{
    protected $dep;
    protected $year;

    public function __construct($bugetYear, $dep) {
        $this->year = $bugetYear;
        $this->dep = $dep;
    }
    /**
    * @return \Illuminate\Support\Collection
    */

    public function bindValue(Cell $cell, $value) {
        $list = [];
       if($cell->getColumn() == 'D') {
            if(is_numeric($value) && $value >4) {
                
           
            $cell->setValueExplicit((number_format(($value / 1000000),2)), DataType::TYPE_STRING);
            return true;
            }
       }

       return parent::bindValue($cell, $value);
        
    }
    

    public function view(): View
    {
        $budget_year = $this->year;
        $department_id = $this->dep ;


        
        $department = Department::with('ministry')->find($department_id);
       
    
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};
        $ministry_name = $department->ministry->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

        
        $budget = BudgetYear::find($budget_year)->first();

        $budget_from = Carbon::parse($budget->from)->format('F Y');
        
        $budget_to = Carbon::parse($budget->to)->format('F Y');

        $data = DemandItem::with('inTransaction')
                ->whereHas('inTransaction',function($q) use ($budget_year, $department_id) {
                    $q->where('budget_year_id',$budget_year)->where('department_id',$department_id);
                })
                ->join('equipment_types as equp','equp.id','demand_items.equipment_type_id')
                ->join('proposals as pro','pro.id','demand_items.proposal_id')
                ->join('proposal_types as pro_types','pro_types.id','pro.proposal_type_id')
                ->join('item_infos as ifs','ifs.id','demand_items.item_info_id')
                ->leftJoin('item_sub_categories as item_subs','item_subs.id','ifs.item_sub_category_id')
                ->leftJoin('item_categories as item_cats','item_cats.id','ifs.item_category_id')
                ->selectRaw('demand_items.*, pro_types.name as pro_type,pro.name as proposal_name,equp.name as equp_name,  
                ifs.name as item_info_name ,   
                ifs.name as item_info_name , 
                case when isnull(item_subs.name)
        then item_cats.name
        else item_subs.name
        end as item_sub_name')->
                get();
        $collection  = $data->groupBy(['pro_type','proposal_name','equp_name'])->toArray();
        
        return view('exports.saone_fromat_excel',compact('collection'))->with([
            'dep_name' => $dep_name,'ministry_name' => $ministry_name, 'budget_from' => $budget_from,'budget_to' => $budget_to
        ]);
        
    }

    public function columnFormats(): array
        {
           return [];
        }

    public function columnWidths(): array
    {
        return [
            'A' => 6,
            'B' => 45,            
        ];
    }
}
