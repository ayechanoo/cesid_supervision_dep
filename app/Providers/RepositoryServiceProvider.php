<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Ministry\MinistryRepositoryInterface;
use App\Repositories\Ministry\MinistryRepository;

use App\Repositories\Department\DepartmentRepository;
use App\Repositories\Department\DepartmentRepositoryInterface;


use App\Repositories\EquipmentType\EquipmentTypeRepositoryInterface;
use App\Repositories\EquipmentType\EquipmentTypeRepository;

use App\Repositories\ItemInfo\ItemInfoRepositoryInterface;
use App\Repositories\ItemInfo\ItemInfoRepository;

use App\Repositories\MainCategory\ItemCategoryRepositoryInterface;
use App\Repositories\MainCategory\ItemCategoryRepository;

use App\Repositories\ItemSubCategory\ItemSubCategoryRepositoryInterface;
use App\Repositories\ItemSubCategory\ItemSubCategoryRepository;

use App\Repositories\ProposalType\ProposalTypeRepositoryInterface;
use App\Repositories\ProposalType\ProposalTypeRepository;

use App\Repositories\Proposal\ProposalRepositoryInterface;
use App\Repositories\Proposal\ProposalRepository;

use App\Repositories\BeHasReTrans\BeHasReTransRepositoryInterface;
use App\Repositories\BeHasReTrans\BeHasReTransRepository;

use App\Repositories\ReTransaction\ReTransactionRepositoryInterface;
use App\Repositories\ReTransaction\ReTransactionRepository;

use App\Repositories\InTransaction\InTransactionRepositoryInterface;
use App\Repositories\InTransaction\InTransactionRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(MinistryRepositoryInterface::class, MinistryRepository::class);
        $this->app->singleton(DepartmentRepositoryInterface::class, DepartmentRepository::class);
        $this->app->singleton(EquipmentTypeRepositoryInterface::class, EquipmentTypeRepository::class);
        $this->app->singleton(ItemCategoryRepositoryInterface::class, ItemCategoryRepository::class);
        $this->app->singleton(ItemInfoRepositoryInterface::class, ItemInfoRepository::class);
        $this->app->singleton(ItemSubCategoryRepositoryInterface::class, ItemSubCategoryRepository::class);
        $this->app->singleton(ProposalTypeRepositoryInterface::class, ProposalTypeRepository::class);
        $this->app->singleton(ProposalRepositoryInterface::class, ProposalRepository::class);
        $this->app->singleton(InTransactionRepositoryInterface::class, InTransactionRepository::class);
        $this->app->singleton(BeHasReTransRepositoryInterface::class, BeHasReTransRepository::class);
        $this->app->singleton(ReTransactionRepositoryInterface::class, ReTransactionRepository::class);

    }
}
