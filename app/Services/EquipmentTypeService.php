<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Request;
use App\Repositories\EquipmentType\EquipmentTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

class EquipmentTypeService extends CommonService{

    protected $repo;

    public function __construct(
        EquipmentTypeRepositoryInterface $repo,
    ) {
        $this->repo = $repo;
    }

    public function getAllList() {
        return $this->repo->getAll();
    }

    public function createEquipmentType($data)
    {
      $equipmentType = $this->repo->insert($data);

        DB::beginTransaction();
        try {
           //$equipmentType = $this->repo->insert($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function getEquipmentTypeByUuid($id)
    {
        return $this->repo->getDataByUuid($id);
    }

    public function equipmentTypeUpdate($data, $id)
    {
        $equipmentType = $this->repo->update($data, $id);

        DB::beginTransaction();

        try {
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function deleteEquipmentType($uuid)
    { 
        try {
            DB::beginTransaction();

            $equipmentType = $this->getEquipmentTypeByUuid($uuid);

            $this->repo->destroy($equipmentType->id);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            errorLogger($exception->getMessage(), $exception->getFile(), $exception->getLine());
            return 500;
        }
        return 1;
    }

    public function makeDtCollection($request): array
    {

        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->repo->totalCount($params);

        $equipmentTypes = $this->repo->getAll($params);
        
        $data = $equipmentTypes->map(function ($equipmentType) use ($action) {
            return [
                'action' => $action,
                'uuid' => $equipmentType->uuid,
                'name' => $equipmentType->name ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }


}
