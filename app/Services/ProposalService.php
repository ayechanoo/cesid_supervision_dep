<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Request;
use App\Repositories\Proposal\ProposalRepositoryInterface;
use App\Repositories\ProposalType\ProposalTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProposalService extends CommonService{

    protected $repo;
    protected $proposalTypeRepo;
    public function __construct(
        ProposalRepositoryInterface $repo, ProposalTypeRepositoryInterface $proposalTypeRepo,
    ) {
        $this->repo = $repo;
        $this->proposalTypeRepo = $proposalTypeRepo;
    }
    public function getAllProposalTypes()
    {
        return $this->proposalTypeRepo->getAll();
    }

    public function createProposalType($data)
    {
      
        DB::beginTransaction();
        try {
            $proposal = $this->repo->insert($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function getProposalByUuid($id)
    {
        return $this->repo->getDataByUuid($id);
    }

    public function proposalUpdate($data, $id)
    {
        // dd($data);
        $proposal = $this->repo->update($data, $id);

        DB::beginTransaction();

        try {
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function deleteProposal($uuid)
    { 
        try {
            DB::beginTransaction();

            $proposal = $this->getProposalByUuid($uuid);

            $this->repo->destroy($proposal->id);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            errorLogger($exception->getMessage(), $exception->getFile(), $exception->getLine());
            return 500;
        }
        return 1;
    }

    public function makeDtCollection($request): array
    {

        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->repo->totalCount($params);

        $proposals = $this->repo->getAll($params);
        
        $data = $proposals->map(function ($proposal) use ($action) {
            return [
                'action' => $action,
                'uuid' => $proposal->uuid,
                'name' => $proposal->name ?? '---',
                'proposal_type' => $proposal->type->name ?? '---'
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }


}
