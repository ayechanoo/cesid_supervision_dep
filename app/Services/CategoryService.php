<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Request;
use App\Repositories\MainCategory\ItemCategoryRepositoryInterface;
use App\Repositories\ItemSubCategory\ItemSubCategoryRepositoryInterface;
use App\Repositories\EquipmentType\EquipmentTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

class CategoryService extends CommonService{

    protected $item_repo;
    protected $item_sub_repo;
    protected $equip_repo;

    public function __construct(
        ItemCategoryRepositoryInterface $item_repo,ItemSubCategoryRepositoryInterface $item_sub_repo, EquipmentTypeRepositoryInterface $equip_repo
    ) {
        $this->item_repo = $item_repo;
        $this->item_sub_repo = $item_sub_repo;
        $this->equip_repo = $equip_repo;
    }

    public function getAllItemCats() {
        return $this->item_repo->getAll();
    }

    public function makeItemCatDtCollection($request) 
    {
        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->item_repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->item_repo->totalCount($params);

        $itemCategories = $this->item_repo->getAll($params);
        
        $data = $itemCategories->map(function ($itemCat) use ($action) {
            return [
                'action' => $action,
                'uuid' => $itemCat->uuid,
                'name' => $itemCat->name ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

    public function makeItemSubCatDtCollection($request) 
    {
        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->item_sub_repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->item_sub_repo->totalCount($params);

        $itemSubCategories = $this->item_sub_repo->getAll($params);
        
        $data = $itemSubCategories->map(function ($itemsub) use ($action) {
            return [
                'action' => $action,
                'uuid' => $itemsub->uuid,
                'name' => $itemsub->name ?? '---',
                'category_type' => $itemsub->category->name ?? '---'
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

    public function createMainCategory($data)
    {
      
        DB::beginTransaction();
        try {
            $main_category = $this->item_repo->insert($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function createSubCat($data) {
        DB::beginTransaction();
        try {
            $proposal = $this->item_sub_repo->insert($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function getItemCategoryByUuid($id)
    {
        return $this->item_repo->getDataByUuid($id);
    }

    public function getItemCategoryByEquipUuid($equip_uuid)
    {
        return $this->item_repo->getDataByEquipUuid($equip_uuid);
    }

    public function getItemSubCategoryByUuid($id)
    {
        return $this->item_sub_repo->getDataByUuid($id);
    }

    public function mainCategoryUpdate($data, $id)
    {
        $equipmentType = $this->item_repo->update($data, $id);

        DB::beginTransaction();

        try {
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function subCategoryUpdate($data, $id)
    {
        $sub_cat = $this->item_sub_repo->update($data, $id);

        DB::beginTransaction();

        try {
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }
   


}
