<?php

namespace App\Services;

use Exception;
use Illuminate\Http\Request;
use App\Repositories\ProposalType\ProposalTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProposalTypeService extends CommonService{

    protected $repo;

    public function __construct(
        ProposalTypeRepositoryInterface $repo,
    ) {
        $this->repo = $repo;
    }

    public function createProposalType($data)
    {
      
        DB::beginTransaction();
        try {
            $proposalType = $this->repo->insert($data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function getProposalTypeByUuid($id)
    {
        return $this->repo->getDataByUuid($id);
    }

    public function proposalTypeUpdate($data, $id)
    {
        $proposalType = $this->repo->update($data, $id);

        DB::beginTransaction();

        try {
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            errorLogger($e->getMessage(), $e->getFile(), $e->getLine());
            return false;
        }

        return true;
    }

    public function deleteProposalType($uuid)
    { 
        try {
            DB::beginTransaction();

            $proposalType = $this->getProposalTypeByUuid($uuid);

            $this->repo->destroy($proposalType->id);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            errorLogger($exception->getMessage(), $exception->getFile(), $exception->getLine());
            return 500;
        }
        return 1;
    }

    public function makeDtCollection($request): array
    {

        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->repo->totalCount($params);

        $proposalTypes = $this->repo->getAll($params);
        
        $data = $proposalTypes->map(function ($proposalType) use ($action) {
            return [
                'action' => $action,
                'uuid' => $proposalType->uuid,
                'name' => $proposalType->name ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }


}
