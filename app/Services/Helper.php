<?php

namespace App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class Helper {


// if (!function_exists('photoUploadStatus')) {
   
//     function photoUploadStatus()
//     {
//         if (Cache::has('photoUploadStatus')) {
//             $status = Cache::get('photoUploadStatus');
//         } else {
//             $setting = Setting::query()->where('key', config('settings.IMAGE_UPLOAD_STATUS'))->first();

//             $status = $setting->value ?? null;

//             Cache::put('photoUploadStatus', $status);
//         }

//         return $status;
//     }
// }

function errorLogger($exception, $file = null, $line = null)
    {
        $time = Carbon::now('Asia/Yangon')->format('H-i-s');
        $path = 'logs/errors/' . 'error-log-' . date('Y-m-d') . '.log';
        config()->set('logging.channels.errorlog.path', storage_path($path));
        Log::channel('errorlog')->error(json_encode([
            'time' => $time, 'msg' => $exception, 'file' => $file, 'line' => $line
        ]));
    }
}