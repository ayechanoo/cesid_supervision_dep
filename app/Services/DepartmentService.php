<?php

namespace App\Services;

use App\Repositories\IssueDepartment\IssueDepartmentRepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Repositories\Ministry\MinistryRepositoryInterface;
use App\Repositories\Department\DepartmentRepositoryInterface;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Services\Helper;


class DepartmentService extends CommonService
{
    protected $ministryRepo;
    protected $departmentRepo;
   
    protected $helper;

    public function __construct(
        MinistryRepositoryInterface $ministryRepo,
        DepartmentRepositoryInterface $departmentRepo,
        Helper $helper
    ) {
        $this->ministryRepo = $ministryRepo;
        $this->departmentRepo = $departmentRepo;
        
        $this->helper = $helper;
    }

    
    public function ministryCreate($data)
    {
        $ministry = $this->ministryRepo->insert($data);
        if ($ministry) {
            $ministry->issue()->create([
                'issueable_id' => $ministry->id,
                'issueable_type' => get_class($ministry)
            ]);

            $ministry->dedicate()->create([
                'receiveable_id' => $ministry->id,
                'receiveable_type' => get_class($ministry)
            ]);
        }

        return $ministry;
    }

    
    public function ministryUpdate($data, $id)
    {
        return $this->ministryRepo->update($data, $id);
    }

    public function ministryGetDataByUUid($uuid)
    {
        return $this->ministryRepo->getDataByUuid($uuid);
    }

    
    public function getAllMinistries()
    {
        return $this->ministryRepo->getAll();
    }

   
    public function getDepartmentByMinistryId($uuid)
    {

        $ministry = $this->ministryRepo->getDatabyUuid($uuid);

       return $this->departmentRepo->getDepartmentByMiniId($ministry->id);

    }


    
    public function departmentCreate($data)
    {
        $department = $this->departmentRepo->insert($data);
        if ($department) {
            $department->issue()->create([
                'issueable_id' => $department->id,
                'issueable_type' => get_class($department)
            ]);
            $department->dedicate()->create([
                'receiveable_id' => $department->id,
                'receiveable_type' => get_class($department)
            ]);
        }
    }

    public function getAllDepartments()
    {
        return $this->departmentRepo->getAll();
    }

    public function departmentGetDataByUUid($uuid)
    {
        return $this->departmentRepo->getDataByUuid($uuid);
    }

    public function departmentUpdate($data, $id)
    {
        return $this->departmentRepo->update($data, $id);
    }

   
    public function internalDepartmentCreate($data)
    {
        $internalDepartment = $this->internalRepo->insert($data);
        if ($internalDepartment) {
            $internalDepartment->issue()->create([
                'issueable_id' => $internalDepartment->id,
                'issueable_type' => get_class($internalDepartment)
            ]);
            $internalDepartment->dedicate()->create([
                'receiveable_id' => $internalDepartment->id,
                'receiveable_type' => get_class($internalDepartment)
            ]);
        }
        return $internalDepartment;
    }

    
    public function internalDepartmentUpdate($data, $id)
    {
        return $this->internalRepo->update($data, $id);
    }

    
    public function internalDepartmentGetDataByUUid($uuid)
    {
        return $this->internalRepo->getDataByUuid($uuid);
    }

    
    public function getAllinternalDepartment()
    {
        return $this->internalRepo->getAll();
    }

    
    public function makeDeparmentDtCollection($request): array
    {
        $action = [
            'canEdit' => Gate::allows('DepartmentUpdate'),
            'canDelete' => Gate::allows('DepartmentDelete')
        ];
        $count = $this->departmentRepo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->departmentRepo->totalCount($params);
        $params = Arr::add($params, 'order_by', ['level' => 'asc']);
        $departments = $this->departmentRepo->getAll($params);

        $data = $departments->map(function ($department) use ($action) {
            return [
                'action' => $action,
                'uuid' => $department->uuid,
                'name_mm' => $department->name_mm ?? '---',
                'name_eng' => $department->name_eng ?? '---',
                'ministry' => $department->ministry->single_name ?? '---',
                'level' => $department->level ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

    
    public function deleteDepartment($uuid): int
    {
        try {
            DB::beginTransaction();

            $department = $this->departmentGetDataByUUid($uuid);
            $department->issue()->delete();
            $department->dedicate()->delete();
            $this->departmentRepo->destroy($department->id);


            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return 500;
        }

        return 1;
    }

   
    public function makeInternalDepartmentDtCollection($request): array
    {
        $action = [
            'canEdit' => Gate::allows('InternalDepartmentUpdate'),
            'canDelete' => Gate::allows('InternalDepartmentDelete')
        ];
        $count = $this->internalRepo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->internalRepo->totalCount($params);

        $internalDepartments = $this->internalRepo->getAll($params);
        $data = $internalDepartments->map(function ($internalDepartment) use ($action) {
            return [
                'action' => $action,
                'uuid' => $internalDepartment->uuid,
                'name_mm' => $internalDepartment->name_mm ?? '---',
                'name_eng' => $internalDepartment->name_eng ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

    
    public function deleteInternalDepartment($uuid): int
    {
        try {
            DB::beginTransaction();

            $internalDepartment = $this->internalDepartmentGetDataByUUid($uuid);

            $internalDepartment->issue()->delete();
            $internalDepartment->dedicate()->delete();
            $this->internalRepo->destroy($internalDepartment->id);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return 500;
        }

        return 1;
    }

    public function makeMinistryDtCollection($request): array
    {
        $action = [
            'canEdit' => Gate::allows('MinistryUpdate'),
            'canDelete' => Gate::allows('MinistryDelete')
        ];
        $count = $this->ministryRepo->totalCount();

        $params = $this->requestParams(request());

        $params['order_by'] = [
            'level' => 'asc',
        ];

        $filteredCnt = $this->ministryRepo->totalCount($params);
        $params = Arr::add($params, 'order_by', ['level' => 'asc']);
        $ministries = $this->ministryRepo->getAll($params);

        $data = $ministries->map(function ($ministry) use ($action) {
            return [
                'action' => $action,
                'uuid' => $ministry->uuid,
                'name_mm' => $ministry->name_mm ?? '---',
                'name_eng' => $ministry->name_eng ?? '---',
                'level' => $ministry->level ?? '---',
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

    public function deleteMinistry($uuid): int
    {
        try {
            DB::beginTransaction();

            $ministry = $this->ministryGetDataByUUid($uuid);

            $this->ministryRepo->destroy($ministry->id);

            $ministry->issue()->delete();

            $ministry->dedicate()->delete();

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return 500;
        }

        return 1;
    }

    public function getVehicleIssueInfoByMinistryId($uuid)
    {
        $ministry = $this->ministryRepo->getDataByUuid($uuid);

        if ($ministry->vehicle_issues->count() > 0) {
            return $ministry->vehicle_issues;
        } else {
            return null;
        }
    }

    public function getVehicleReceiveInfoByMinistryId($uuid)
    {
        $ministry = $this->ministryRepo->getDataByUuid($uuid);

        if ($ministry->vehicle_receives->count() > 0) {
            return $ministry->vehicle_receives;
        } else {
            return null;
        }
    }

    public function getVehicleIssueInfoCountByInternalDepartment($uuid)
    {
        $internalDepartment = $this->internalDepartmentGetDataByUUid($uuid);

        $response = $this->vehicleIssueInfoRepo->getVehicleIssueInfoCountByInternalDepartment($internalDepartment->issue->id);

        return $response;
    }

    public function getVehicleReceiveInfoCountByInternalDepartment($uuid)
    {
        $internalDepartment = $this->internalDepartmentGetDataByUUid($uuid);
        $response = $this->vehicleReceiveInfoRepo->getVehicleReceiveInfoCountByInternalDepartment($internalDepartment->dedicate->id);

        return $response;
    }

    public function getVehicleIssueInfoCountByDepartment($uuid)
    {
        $department = $this->departmentGetDataByUUid($uuid);
        $response = $this->vehicleIssueInfoRepo->getVehicleIssueInfoCountByDepartment($department->issue->id);

        return $response;
    }

    public function getVehicleReceiveInfoCountByDepartment($uuid)
    {
        $department = $this->departmentGetDataByUUid($uuid);
        $response = $this->vehicleReceiveInfoRepo->getVehicleReceiveInfoCountByDepartment($department->dedicate->id);

        return $response;
    }
}
