<?php

namespace App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Models\ProposalType;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\ItemInfo\ItemInfoRepositoryInterface;

class Transaction extends CommonService
{
    protected $item_info_repo;
    public function __construct(ItemInfoRepositoryInterface $item_info_repo) {
        $this->item_info_repo = $item_info_repo;
    }

    public function getAllItemInfos() {
        return $this->item_info_repo->getAll();
    }

    public function existData($collection,Model $model) {
        return $model::selectRaw('lower(name) as name , id')->whereIn('name',$collection)->get()->groupBy('name')->toArray();
    }

    public function proposalCR($collection) {
        foreach($collection as $d) {
            ProposalType::create([
                'name' => $d
            ]);
        }

        return ProposalType::selectRaw('lower(name) as name , id')->get()->groupBy('name')->toArray();

    }


    public function makeItemInfoDtCollection($request): array
    {

        $action = [

            'canEdit' => true,
            'canDelete' => true
        ];

        $count = $this->item_info_repo->totalCount();

        $params = collect($this->requestParams(request()))->toArray();

        $filteredCnt = $this->item_info_repo->totalCount($params);

        $itemInfos = $this->item_info_repo->getAll($params);
        
        $data = $itemInfos->map(function ($info) use ($action) {
            return [
                'action' => $action,
                'uuid' => $info->uuid,
                'name' => $info->name ?? '---',
                'sub_category_type' => $info->itemSubCategory->name ?? '---',
                'category_type' => $info->itemCategory->name ?? '---'
            ];
        })->toArray();

        return [
            'recordsTotal' => $count,
            'draw' => request('draw'),
            'recordsFiltered' => $filteredCnt,
            'data' => $data,
        ];
    }

}