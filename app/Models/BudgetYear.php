<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetYear extends Model
{
    use HasFactory, HasUUID, SoftDeletes;
    protected $fillable = ['uuid', 'from','to'];
}
