<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasUUID;


class ReTrans extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable = ['uuid', 'be_has_retrans_id', 'demand_item_id', 'qty', 'unit', 'total'];
}
