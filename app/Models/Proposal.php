<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proposal extends Model
{
    use HasFactory,SoftDeletes,HasUUID;

    protected $fillable = ['uuid','name','proposal_type_id'];

    protected $with = ['type'];

    public function type() {
        return $this->belongsTo(ProposalType::class,'proposal_type_id','id');
    }
}
