<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\HasUUID;

class InTransaction extends Model
{
    use HasFactory,HasUUID, SoftDeletes;
    protected $fillable = ['uuid', 'budget_year_id', 'department_id'];

    public function budgetYear() {
        return $this->belongsTo(BudgetYear::class);
    }

    public function department() {
        return $this->belongsTo(Department::class);
    }


    public function demandItems() {
        return $this->hasMany(DemandItem::class);
    }

    public function getTotalDemandsAttribute() {
        return $this->hasMany(DemandItem::class)->count();
    }
}
