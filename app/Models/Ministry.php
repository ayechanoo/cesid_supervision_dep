<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\HasLanguage;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasUUID;
use App\Models\Department;
class Ministry extends Model
{
    use HasFactory, HasUUID, SoftDeletes, HasLanguage;

    protected $fillable = ['uuid', 'name_eng', 'name_mm', 'level', 'user_id'];

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    
}
