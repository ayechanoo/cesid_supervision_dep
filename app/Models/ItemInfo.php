<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemInfo extends Model
{
    use HasFactory, HasUUID, SoftDeletes;
    protected $fillable = [
        'uuid', 'name', 'item_category_id','item_sub_category_id'
    ];

    public function itemCategory() {
        return $this->belongsTo(ItemCategory::class);
    }

    public function itemSubCategory() {
        return $this->belongsTo(ItemSubCategory::class,'item_sub_category_id','id');
    }
}
