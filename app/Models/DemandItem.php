<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class DemandItem extends Model
{
    use HasFactory, HasUUID, SoftDeletes;
    protected $fillable = ['uuid', 'name', 'specification','item_info_id','qty','equipment_type_id','unit_price','total','unit','proposal_id', 'justification','in_transaction_id'];

    public function inTransaction() {
        return $this->belongsTo(InTransaction::class);
    }
}
