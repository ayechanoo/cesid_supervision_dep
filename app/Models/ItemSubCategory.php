<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemSubCategory extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable =['name','uuid','item_category_id'];

    public function category() {
        return $this->belongsTo(ItemCategory::class,'item_category_id','id');
    }
}
