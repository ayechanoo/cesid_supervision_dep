<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCategory extends Model
{
    use HasFactory,SoftDeletes,HasUUID;

    protected $fillable =['uuid', 'name', 'equipment_type_id'];

    public function equipmentType() {
        return $this->belongsTo(EquipmentType::class);
    }

    public function subcategories() {
        return $this->hasMany(ItemSubCategory::class);
    }

    public function getSubCountAttribute() {
        return $this->hasMany(ItemSubCategory::class)->count();
    }

}
