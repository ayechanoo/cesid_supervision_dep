<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasLanguage;
use App\Traits\HasUUID;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Ministry;

class Department extends Model
{
    use HasFactory, HasUUID, SoftDeletes,HasLanguage;

    protected $fillable = ['uuid', 'name_eng', 'name_mm', 'ministry_id', 'parent_id','level','user_id'];

    protected $with =['ministry'];

    public function ministry()
    {
        return $this->belongsTo(Ministry::class);
    }
}
