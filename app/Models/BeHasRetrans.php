<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasUUID;

class BeHasRetrans extends Model
{
    use HasFactory, HasUUID, SoftDeletes;

    protected $fillable = ['reviewed_by', 'in_transaction_id'];
}
