<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait HasLanguage
{

    public function getSingleNameAttribute() {
        $lang =  \App::getLocale();

       if($lang == 'en') {
        $lang = 'eng';
       }

       return $this->{'name_'.$lang};
    }



}
