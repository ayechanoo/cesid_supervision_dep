<?php

namespace App\Repositories\Proposal;

use App\Models\Proposal;
use App\Repositories\BaseRepository;

class ProposalRepository extends BaseRepository implements ProposalRepositoryinterface 
{
    public function __construct(Proposal $model)
    {
        parent::__construct($model);
    }
}