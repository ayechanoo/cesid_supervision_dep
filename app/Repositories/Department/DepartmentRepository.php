<?php

namespace App\Repositories\Department;

use App\Models\Department;
use App\Repositories\BaseRepository;

class DepartmentRepository extends BaseRepository implements DepartmentRepositoryInterface
{
    public function __construct(Department $model)
    {
        parent::__construct($model);
    }

    public function getDepartmentByMiniId($id)
    {
        return $this->connection->where('ministry_id', $id)->get();
    }

    public function getDepartmentByMinistryId($id)
    {
        $ministry_in_department = $this->connection->where('ministry_id', $id)->count();
        if ($ministry_in_department > 0) {
            return true;
        } else {
            return false;
        }
    }

    protected function optionsQuery(array $options)
    {
        $query = parent::optionsQuery($options);

       
        if (isset($options['search'])) {
            $query = $query->where('name_mm', 'like', "%{$options['search']}%")
                           ->orWhere('name_eng', 'like', "%{$options['search']}%")
                                ->orWhereHas('ministry', function ($query) use ($options) {
                                    $query->where('name_mm', 'like', "%{$options['search']}%")
                                        ->orWhere('name_eng', 'like', "%{$options['search']}%");
                                });
        }

        return $query;
    }
}
