<?php

namespace App\Repositories\Department;

interface DepartmentRepositoryInterface
{
    public function getDepartmentByMinistryId($id);
}
