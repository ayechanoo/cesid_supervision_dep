<?php

namespace App\Repositories\InTransaction;

use App\Models\InTransaction;
use App\Repositories\BaseRepository;

class InTransactionRepository extends BaseRepository implements InTransactionRepositoryInterface 
{
    public function __construct(InTransaction $model)
    {
        parent::__construct($model);
    }

    public function recordByfilters($arr)
    {
        $query = $this->connection->query();
        if( array_key_exists('budget_year_uuid', $arr)) {
            $query->whereHas('budgetYear', function($q) use ($arr) {
                $q->where('uuid',$arr['budget_year_uuid']);
            });
        }

        if(array_key_exists('department_uuid',$arr)) {
            $query->whereHas('department', function($q) use ($arr) {
                $q->where('uuid', $arr['department_uuid']);
            });
        }

        return $query->first();
    }

    public function recordsByfilters($arr)
    {
        $query = $this->connection->query();
        if( array_key_exists('budget_year_uuid',$arr)) {
            $query->whereHas('budgetYear', function($q) use ($arr) {
                $q->where('uuid',$arr['budget_year_uuid']);
            });
        }

        if(array_key_exists('department_uuid',$arr)) {
            $query->whereHas('department', function($q) use ($arr) {
                $query->where('uuid', $arr['department_uuid']);
            });
        }

        return $query->paginate();
    }

    
}