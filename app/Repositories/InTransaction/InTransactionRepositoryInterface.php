<?php

namespace App\Repositories\InTransaction;

interface InTransactionRepositoryInterface
{
    public function recordByfilters($arr);
}