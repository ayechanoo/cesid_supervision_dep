<?php

namespace App\Repositories\ItemInfo;

interface ItemInfoRepositoryInterface
{
    public function allCategoryList();

    public function categoryByEquipId($id);
}