<?php

namespace App\Repositories\ItemInfo;

use App\Models\ItemInfo;
use App\Repositories\BaseRepository;

class ItemInfoRepository extends BaseRepository implements ItemInfoRepositoryInterface 
{
    public function __construct(ItemInfo $model)
    {
        parent::__construct($model);
    }


    public function allCategoryList() {
      $query = $this->joinquery();
            
            return $this->connection->leftJoin('item_categories as ic','ic.id','item_infos.item_category_id')
            ->leftJoin('item_sub_categories as isc','isc.id','item_infos.item_sub_category_id')->selectRaw('case when isnull(item_infos.item_sub_category_id)
            then ic.name
            else isc.name
            end as Item_Category,ic.name as ic_name')
            ->get()->groupBy('ic_name');

      

    }

    public function categoryByEquipId($id) {
        $query = $this->joinquery();
        return $query->where('ic.equipment_type_id',$id)->selectRaw('case when isnull(item_infos.item_sub_category_id)
            then ic.name
            else isc.name
            end as Item_Category,ic.name as ic_name')
            ->get()->groupBy('ic_name');
    }

    public function joinquery() {
        return $this->connection->query()->leftJoin('item_categories as ic','ic.id','item_infos.item_category_id')
            ->leftJoin('item_sub_categories as isc','isc.id','item_infos.item_sub_category_id');
    }
}