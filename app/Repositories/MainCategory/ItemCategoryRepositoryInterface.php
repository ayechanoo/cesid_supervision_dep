<?php

namespace App\Repositories\MainCategory;

interface ItemCategoryRepositoryInterface
{
    public function getlist();
    public function getDataByEquipUuid($uuid);
    public function getFilterList($arr);
}