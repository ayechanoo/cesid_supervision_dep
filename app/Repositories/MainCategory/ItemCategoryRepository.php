<?php

namespace App\Repositories\MainCategory;

use App\Models\ItemCategory;
use App\Repositories\BaseRepository;

class ItemCategoryRepository extends BaseRepository implements ItemCategoryRepositoryInterface 
{
    public function __construct(ItemCategory $model)
    {
        parent::__construct($model);
    }

    public function getlist() {
        return $this->connection->with('subcategories')->get();
    }

    public function getDataByEquipUuid($equp_uuid) {
        return $this->connection->whereHas('equipmentType',function($q) use ($equp_uuid) {
            $q->where('uuid',$equp_uuid);
        })->get();
    }

    public function getFilterList($arr) {
        $query = $this->connection->query()->with('subcategories');
        if(array_key_exists('uuids',$arr)) {
            $query->whereIn('uuid',$arr['uuids']);
        }

        return $query->get();
    }
}