<?php

namespace App\Repositories\ProposalType;

use App\Models\ProposalType;
use App\Repositories\BaseRepository;

class ProposalTypeRepository extends BaseRepository implements ProposalTypeRepositoryinterface 
{
    public function __construct(ProposalType $model)
    {
        parent::__construct($model);
    }
}