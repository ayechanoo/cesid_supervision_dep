<?php

namespace App\Repositories\EquipmentType;

use App\Models\EquipmentType;
use App\Repositories\BaseRepository;

class EquipmentTypeRepository extends BaseRepository implements EquipmentTypeRepositoryInterface 
{
    public function __construct(EquipmentType $model)
    {
        parent::__construct($model);
    }
}