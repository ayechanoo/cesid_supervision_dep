<?php

namespace App\Repositories\Ministry;

use App\Models\Ministry;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;

class MinistryRepository extends BaseRepository implements MinistryRepositoryInterface
{
    public function __construct(Ministry $model)
    {
        parent::__construct($model);
    }

    protected function optionsQuery(array $options)
    {
        $query = parent::optionsQuery($options);

        if(isset($options['search'])) {
            $query = $query->where(function ($query) use ($options) {
                $query->orWhere('name_mm', 'like', "%{$options['search']}%")
                    ->orWhere('name_eng', 'like', "%{$options['search']}%");
            });
        }

        return $query;
    }
}
