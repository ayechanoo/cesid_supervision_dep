<?php

namespace App\Repositories\ItemSubCategory;

interface ItemSubCategoryRepositoryInterface
{
    public function combineCats();
}