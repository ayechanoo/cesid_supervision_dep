<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\VTwoSaOneFormat;
use Excel;
use App\Services\Transaction;
use App\Models\Department;
use App\Models\ItemInfo;
use App\Models\DemandItem;
use App\Models\Proposal;
use App\Models\ProposalType;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\EquipmentType;
use App\Models\BudgetYear;
use App\Models\InTransaction;
use App\Imports\ExcelUtitlites;
use App\Imports\ExcelMultipleSheet;
use \stdClass;

use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SvController extends Controller
{
    protected $service;
    protected $dict_prj;
    protected $equip_dict;
    protected $prj_dict;

    public function __construct(Transaction $importService) {
       

        $this->prj_dict = ProposalType::selectRaw('lower(name) as name , id')->get()->groupBy('name')->toArray();
        $this->equip_dict = EquipmentType::selectRaw('lower(name) as name , id')->get()->groupBy('name')->toArray();
    }

    public function index() {
        return view('sv.index');
    }

    public function upload(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'budget_from' => 'required|min:5',
            'budget_to' => 'required',
            'department_id' => 'required',
            'upload_file' => 'required',
        ],[
            'budget_from.required' => 'Budget Start is must.',
            'budget_to.min' => 'Budget End is must.',
            'department_id.required' => 'Please Choose Department',
            'upload_file.required' => 'Please Upload file, it is must in SA-1001',
        ]);

        if($validate->fails()){
        return back()->withErrors($validate->errors())->withInput();
        }


        $budget_from = Carbon::parse($request->budget_from)->format('F Y');
        
        $budget_to = Carbon::parse($request->budget_to)->format('F Y');
        $department_id = $request->department_id;
        $department = Department::with('ministry')->find($department_id);
        
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'manual.create';

        array_push($nav_lists,$link);

        $file = $request->file('upload_file');

        $sheet_names = $this->getSheetNames($file);

        foreach($sheet_names as $name) {
            $collection [] =Excel::toCollection(new ExcelMultipleSheet($name), $file)[$name];

            
        }

        
       
       

        // $all_sheet_collection = [];

        // foreach($collection as $value) {
        //     $saoneimport = new VTwoSaOneFormat();
        //  //    $saoneimport->setHeadingRow(8);
        //     $saoneimport->collection($value);
            
        //  //    dd($saoneimport->getResults());
        //     $all_sheet_collection[] = $saoneimport->getAllData();
            
        //  }

         

      

        return view('sv.table_form_template',compact('dep_name','budget_from','budget_to'))
        ->with([
            'sheets' => $sheet_names,
            'collection' => $collection,
            'nav_lists' => $nav_lists ,
            'budget_year_from' => $request->budget_from,
            'budget_year_to' => $request->budget_to,
            'department' => $department,
        ]);

      //  return view('sv.table_form_template')->with(['collection' => $collection, 'nav_lists' => []]);
    }

    public function storeData(Request $request) {
        
        $input = $request->all();
        if(isset($input['list'])) {
            $lists = json_decode($input['list'],true);
            if(count($lists) == 0) {
                return response()->json([
                    'msg' => 'there is no data to import'
                ]);
            }


            $budget_year = BudgetYear::create([
                'from' => $input['budget_from'],
                'to'  => $input['budget_to']
               ]);
        
               $in_transaction = InTransaction::create([
                'budget_year_id' => $budget_year->id,
                'department_id' => $request->department['id']
               ]);

               
           $status =  $this->createItem($lists ,$in_transaction);
            
           if($status) {
            return response()->json([
                'msg' => 'successfully',
                'status' => true
            ]);
           }
        }
    }

    public function createItem($data, $transaction) {
        $in_transaction = 1;
        foreach($data as $d) {
            
            $sub_key = $this->trimAndlower($d['prj_sub_type']);
            $prj_key = $this->trimAndlower($d['prj_type']);

           

            $proposal = $this->checkorNewProposal($prj_key, $sub_key);

            
            foreach($d['equipment_list'] as $q => $equip) {

               

                $equip_key = $this->trimAndlower($equip['name']);

                
            
               $db_equip =  $this->checkorNewEquipId($equip_key);
                
              

               foreach($equip['items'] as $k => $v) {
                
                   
              
            

                    $item_name = $this->trimAndlower($v['name']);

                    
                    $item_cate = $this->trimAndlower($v['item_category']);
                    $item_sub_cate = $this->trimAndlower($v['item_subcategory']);

                   
                   
                    $item_info = $this->checkItemInfoId($item_name, $item_cate, $item_sub_cate);
                    $demand_item = DemandItem::create([
                        'name' => $item_info->name,
                        'specification' => $v['specification'] ?? null,
                        'justification' => $v['justification'] ?? null,
                        'item_info_id' => $item_info->id,
                        'qty' => $v['qty'] ?? 1 ,
                        'equipment_type_id' => $db_equip->id,
                        'unit_price' => $v['unit_price'] ,
                        'unit' => $v['unit'],
                        'proposal_id' => $proposal->id,
                        'total' => $v['total_price'] ?? 0,
                        'in_transaction_id' => $transaction->id
                       ]);
               }
            }

           
            
        }

        return true;
    }
    
    public function checkItemInfoId($item, $cate, $subcate) {
        $row = new \stdClass();
        $row->item = $item ?? null;
        $row->cate = $cate ?? null;
        $row->subcate = $subcate ?? null;

      
        if(!is_null($row->subcate)) {
        $item_info_query = ItemInfo::query()->where('name', $row->item)
                ->whereHas('itemSubCategory', function($q) use ($row) {
                    $q->where('name', $row->subcate);
                })->WhereHas('itemCategory', function($q) use ($row) {
                    $q->where('name', $row->cate);
                })
                ->first();
        }else{
            $item_info_query = ItemInfo::query()->where('name', $row->item)
                ->whereHas('itemSubCategory', function($q) use ($row) {
                    $q->where('name', $row->subcate);
                })->orWhereHas('itemCategory', function($q) use ($row) {
                    $q->where('name', $row->cate);
                })
                ->first();

        }
       if(!$item_info_query) {

            if(!is_null($row->subcate)) {

                $item_sub_category = ItemSubCategory::where('name',$row->subcate)->whereHas('category', function($q) use ($row) {
                    $q->where('name',$row->cate);
                })->first();
                
                if(!$item_sub_category) {

                    $item_categ = ItemCategory::where('name', $row->cate)->first();
                    if(!$item_categ) {
                        $item_categ = ItemCategory::create(['name' => strtolower($row->cate)]);
                    }

                    $item_sub_category = ItemSubCategory::create([
                        'name'  => $row->subcate,
                        'item_category_id' => $item_categ->id
                    ]);

                }

                $itemInfo = ItemInfo::create([
                    'name' => $row->item,
                    'item_sub_category_id' => $item_sub_category->id,
                    'item_category_id' => $item_sub_category->item_category_id

                ]);

                return $itemInfo;
                // $item_subcategory = ItemSubCategory::where('name', $row[6]['name'])->first();
                // if(!$item_subcategory) {
                //     $item_subcategory
                // }

            }else{

                $item_categ = ItemCategory::where('name', $row->cate)->first();
                    if(!$item_categ) {
                        $item_categ = ItemCategory::create(['name' => $row->cate]);
                    }

                    $itemInfo = ItemInfo::create([
                        'name' => $row->item,
                        'item_sub_category_id' => null,
                        'item_category_id' => $item_sub_category->item_category_id
    
                    ]);
    
                    return $itemInfo;

            }

       }

       return $item_info_query;

    }

    public function checkorNewEquipId($item) {

       

            $data = EquipmentType::where('name',strtolower($item))->first();

            if(!$data) {
                $data = EquipmentType::create([
                    'name' => strtolower($item),
                ]);
            }
            
        
        return $data;
            
            
           
    }

    public function checkorNewProposal($prj_key, $sub_prj_key) {
        
        $proposal = Proposal::where('name',$sub_prj_key)->whereHas('type',function($q) use ($prj_key) {
            $q->where('name',$prj_key);
        })->first();

        if(!$proposal) {    
            $type = ProposalType::where('name',$prj_key)->first();

            if(!$type) {
                $type = ProposalType::create([
                    'name' => $prj_key
                ]);
            }
            
            $proposal = Proposal::create([
                'name' => $sub_prj_key,
                'proposal_type_id' => $type->id
            ]);
        }

        return $proposal;
    }


    public function dataAnalysis($data) {
        $new_colleciton = [
            'PRJ_TYPE' => [],
            'SUB_PRJ_TYPE' => [],
            'EQUIP' => [],
            'CATE' => [],
            'SUB_CATE' => [],
        ];
        

            $prj_list = $this->makeuniqueList($data,'prj_type');
            $sub_prj_list = $this->makeuniqueList($data,'prj_sub_type');
            $equip_list = [];
            $item_cate_list = [];
            $item_subcate_list = [];
            $item_name_list = [];

            foreach($data as $d) {
                $equip = $this->makeuniqueList($d['equipment_list'],'name');
                $equip_list = [...$equip];

                foreach($d['equipment_list'] as $equip) {
                    $cate = $this->makeuniqueList($equip['items'],'item_category');
                     $item_cate_list = [...$cate];

                     $sub_cate = $this->makeuniqueList($equip['items'],'item_subcategory');
                     $item_subcate_list = [...$sub_cate];

                     $item_name = $this->makeuniqueList($equip['items'],'name');
                     $item_name_list = [...$item_name];

                     
                }
            }

          $prepare_prj = $this->service->existData($prj_list, new \App\Models\ProposalType);
          

          if(count($prepare_prj) == 0) {
            $this->dict_prj = $this->service->proposalCR($prj_list);
          }



          $prepare_prj = $this->service->existData($prj_list);
          

          if(count($prepare_prj) == 0) {
            $this->dict_prj = $this->service->proposalCR($prj_list);
          }

         

         

        
        
    }


    
        

    function makeuniqueList($arr, $findKey) {
        $collection = [];
        foreach($arr as $item) {
            $key = $this->trimAndlower($item[$findKey]);

            if(!in_array($key,$collection)) {
                array_push($collection,$key);
            }
        }

        return $collection;
    }

    function trimAndlower($word) {
        return strtolower(trim($word));
    }

    public function getSheetNames($file) {

        $sheetObj = new ExcelUtitlites();

        Excel::import($sheetObj, $file);

        $sheet_names = $sheetObj->getSheetNames();
        return $sheet_names;
    }

        
    




    
}
