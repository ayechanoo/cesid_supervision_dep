<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\DepartmentService;
use App\Services\Helper;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class MinistryController extends Controller
{
    protected $service;
    protected $helper;

    public function __construct(DepartmentService $service ,Helper $helper)
    {
        $this->service = $service;
        $this->helper = $helper;
    }

    
    public function index(Request $request)
    {
        return view('Ministry.index');
    }

    
    public function create()
    {
        return view('Ministry.create');
    }

    
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name_eng' => 'required',
            'name_mm' => 'required',
            'level' => 'nullable|numeric',
        ]);
        $inputs = $request->all();

        DB::beginTransaction();
        try {
        $this->service->ministryCreate($inputs);

            $success = true;
            if ($success) {
                DB::commit();
            }
           
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine()); 
            return back()->with('error_msg', 'something went wrong!');
        }
        return redirect()->route('ministry.index')->with('message', 'Successfully Saved!');
    }

   
    public function edit($id)
    {
        $row = $this->service->ministryGetDataByUUid($id);

        return view('Ministry.edit')->with(['row' => $row]);
    }

    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name_eng' => 'required',
            'name_mm' => 'required',
            'level' => 'nullable|numeric',
        ]);
        $inputs = $request->only('name_mm', 'name_eng', 'level');
        $row = $this->service->ministryGetDataByUUid($uuid);

        try {
            $data = $this->service->ministryUpdate($inputs, $row->id);

            return redirect()->route('ministry.index')->with('message', 'Successfully Saved!');
        } catch (\Exception $e) {
            DB::rollback();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return redirect()->route('ministry.index')->with('error_msg', 'Action Failed!');
        }
    }

    
    public function destroy($uuid)
    {
        $relationToReveiveInfo = $this->service->getVehicleReceiveInfoByMinistryId($uuid);
        $relationToIssueInfo = $this->service->getVehicleIssueInfoByMinistryId($uuid);
        if ($relationToReveiveInfo || $relationToIssueInfo) {
            return response()->json(['status' => false , 'message' => 'you cannot delete due to relationship'], 300);
        }

//
        $response = $this->service->deleteMinistry($uuid);
//
        return response()->json(['success' => $response]);
    }

    public function getMinistries(Request $request): JsonResponse
    {
        return response()->json($this->service->makeMinistryDtCollection($request));
    }
}
