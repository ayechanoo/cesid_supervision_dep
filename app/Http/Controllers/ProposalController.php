<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProposalService;

class ProposalController extends Controller
{
    protected $service;
    public function __construct(ProposalService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        return view('proposal.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proposalTypes = $this->service->getAllProposalTypes();
        return view('proposal.create',['proposalTypes' => $proposalTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'proposal_type_id' => 'required',
        ]);

        $inputs = $request->all();
        $response = $this->service->createProposalType($inputs);

        if ($response) {
            return redirect()->route('main-proposal.index')
                ->with('message', 'Successfully Added!');
        } else {
            return redirect()->back()->withInput()
        ->with('error_msg', 'something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proposalTypes = $this->service->getAllProposalTypes();
        $row = $this->service->getProposalByUuid($id);
        return view('proposal.edit')->with(['row' => $row,'proposalTypes' => $proposalTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name' => 'required',
            'proposal_type_id' => 'required',
        ]);

        $inputs = $request->only('name','proposal_type_id');

        $row = $this->service->getProposalByUuid($uuid);
        $response =  $this->service->proposalUpdate($inputs, $row->id);

        if ($response) {
            return redirect()->route('main-proposal.index')
                ->with('message', 'Successfully Updated!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
    public function getProposals(Request $request)
    {
        return response()->json($this->service->makeDtCollection($request));
    }
}
