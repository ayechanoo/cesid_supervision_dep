<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Ministry;
use \App\Models\Department;
use \App\Models\BudgetYear;
use \App\Models\InTransaction;
use App\Repositories\InTransaction\InTransactionRepositoryInterface as InTransRepo;
use App\Repositories\BeHasReTrans\BeHasReTransRepositoryInterface as BeHasReTransRepo;
use App\Repositories\ReTransaction\ReTransactionRepositoryInterface as ReTransRepo;

use App\Imports\ExcelMultipleSheet;
use App\Imports\ExcelUtitlites;
use App\Imports\ReExcelImport;
use Excel;

class RecommendController extends Controller
{
    protected $in_trans_repo;
    protected $be_has_re_repo;
    protected $re_trans_repo;

    public function __construct(InTransRepo $in_trans_repo, BeHasReTransRepo $be_has_re_repo, ReTransRepo $re_trans_repo)
    {
        $this->re_trans_repo = $re_trans_repo;
        $this->be_has_re_repo = $be_has_re_repo;
        $this->in_trans_repo = $in_trans_repo;
    }

    public function depRecordsByMiniCreate() {
        return view('industrial_supervision.deps_vs_rebe_list_form');
    }
    
    public function excelImportForm() {
        $ministries = Ministry::all();
        $budget_years = BudgetYear::all();
        $in_transactions = InTransaction::all();
        
        return view('industrial_supervision.re_excel_import_form')->with(['ministries' => $ministries,
                    'budget_years' => $budget_years,
                    'in_transactions' => $in_transactions
                ]);
    }

    public function recommendImport(Request $request) 
    {
       // dd($request);
      $budget_years_uuid = $request->budget_year;
      $department_uuid = $request->department_id;
      $reviewed_by = $request->reviewed_by;

      $be_record = $this->in_trans_repo->recordByfilters([
        'budget_year_uuid' => $budget_years_uuid,
        'department_uuid' => $department_uuid
      ]);

      $total_demands = $be_record->total_demands;

      $be_items = $be_record->demandItems;


     

      $the_file = $request->file('excel_file');
      $sheet_names = $this->getSheetNames($the_file);

      $read_count = 0;

      $all_sheet_collection = [];

      foreach($sheet_names as $name) {

            $read_data =Excel::toCollection(new ExcelMultipleSheet($name), $the_file)[$name];

            $saoneimport = new ReExcelImport();
            $saoneimport->collection($read_data);
            $collection = $saoneimport->getResults();
           
            $all_sheet_collection = [...$collection['valid_data']];
            $errors[] = $collection['errors'];
            
        }

       $read_count = count($all_sheet_collection);

       if($read_count == $total_demands) {
        $be_has_re = $this->be_has_re_repo->insert([
            'reviewed_by' => $reviewed_by,
            'in_transaction_id' => $be_record->id
        ]);
        foreach($be_items as $key => $item) {
            $re_row = $all_sheet_collection[$key];
            // dd($re_row);
            $this->re_trans_repo->insert([
                'be_has_retrans_id' => $be_has_re->id,
                'demand_item_id' => $item->id,
                'qty' => $re_row[13]['value'],
                'unit' => $re_row[14]['value'],
                'total' => ((int)$re_row[15]['value']) * 1000,
            ]);
        }

        return redirect()->route('manual.re.excel-import-form')->with(['message' => 'successfully add']);
        
       }else{
        return redirect()->route('manual.re.excel-import-form')->with(['error_msg' => 'recommend item are not compatible with demands imported! Please check again with Demand List!']);
        
       }



    }

    public function getSheetNames($file) {

        $sheetObj = new ExcelUtitlites();

        Excel::import($sheetObj, $file);

        $sheet_names = $sheetObj->getSheetNames();
        return $sheet_names;
    }
}
