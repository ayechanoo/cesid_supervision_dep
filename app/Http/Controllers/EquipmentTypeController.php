<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\EquipmentTypeService;

class EquipmentTypeController extends Controller
{
    protected $service;
    public function __construct(EquipmentTypeService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('equipment_type.index');
    }

    public function create()
    {
        return view('equipment_type.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);
        $inputs = $request->all();
        
        $response = $this->service->createEquipmentType($inputs);

        if ($response) {
            return redirect()->route('equipment-type.index')
                ->with('message', 'Successfully Added!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $row = $this->service->getEquipmentTypeByUuid($id);
        return view('equipment_type.edit')->with(['row' => $row]);
    }

    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        $inputs = $request->only('name');

        $row = $this->service->getEquipmentTypeByUuid($uuid);
        $response =  $this->service->equipmentTypeUpdate($inputs, $row->id);

        if ($response) {
            return redirect()->route('equipment-type.index')
                ->with('message', 'Successfully Updated!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $response = $this->service->deleteEquipmentType($uuid);

        return response()->json(['success' => $response]);
    }

    public function getEquipmentTypes(Request $request)
    {
        return response()->json($this->service->makeDtCollection($request));
    }
}
