<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CategoryService;

class SubCategoryController extends Controller
{
    protected $service;
    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('sub_category.index');
    }

    public function getSubCategories(Request $request)
    {
        return response()->json($this->service->makeItemSubCatDtCollection($request));
    }

    public function create()
    {
        $categories = $this->service->getAllItemCats();
        
        return view('sub_category.create')->with(['categories' => $categories]);
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'item_category_id' => 'required',
        ]);

        $inputs = $request->all();
        $response = $this->service->createSubCat($inputs);

        if ($response) {
            return redirect()->route('sub-category.index')
                ->with('message', 'Successfully Added!');
        } else {
            return redirect()->back()->withInput()
        ->with('error_msg', 'something went wrong!');
        }
    }


    public function edit($id)
    {
        $categories = $this->service->getAllItemCats();
        $row = $this->service->getItemSubCategoryByUuid($id);
        return view('sub_category.edit')->with(['row' => $row,'categories' => $categories]);
    }


    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name' => 'required',
            'item_category_id' => 'required',
        ]);

        $inputs = $request->only('name','item_category_id');

        $row = $this->service->getItemSubCategoryByUuid($uuid);
        $response =  $this->service->subCategoryUpdate($inputs, $row->id);

        if ($response) {
            return redirect()->route('sub-category.index')
                ->with('message', 'Successfully Updated!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }




}
