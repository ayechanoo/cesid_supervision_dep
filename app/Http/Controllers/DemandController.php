<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BudgetYear;
use App\Models\DemandItem;
use App\Models\Department;
use App\Models\ProposalType;
use App\Models\Proposal;
use App\Models\EquipmentType;
use App\Models\InTransaction;
use App\Models\ItemCategory;
use App\Models\ItemInfo;
use App\Models\ItemSubCategory;

class DemandController extends Controller
{
    public function create() {

        $budget_years = BudgetYear::all();
      
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        // if(\App::getLocale() == 'en') {
        //     $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        // }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        //}

        $departments = $query->get()->groupBy('minister_name');

       
        $nav_lists = [];
        $link = new \stdClass();
        $link->name = 'Home';
        $link->route = 'sv.index';

        array_push($nav_lists,$link);


        $projects = ProposalType::all();
        
        $sub_projects = Proposal::all();

        $equipments = EquipmentType::all();
        $item_categories = ItemCategory::all();
        $item_sub_categories = ItemSubCategory::all();
        $units = config('helper.units');
       
    

       return view('template.index',compact('departments'))->with(['nav_lists' => $nav_lists, 
            'budget_years' => $budget_years,
            'projects' => $projects,
            'sub_projects' => $sub_projects,
            'equipments' => $equipments,
            'item_categories' => $item_categories,
            'item_sub_categories' => $item_sub_categories,
            'units' => $units
        ]);
    }

    public function store(Request $request) {

        $request->validate([
            'budget' => 'required|array',
            'budget.to' => ['required_with:budget'],
            'budget.from' => ['required_with:budget'],
            'department_id' => ['required','exists:departments,id'],
            'prj' => 'required|array',
            'sub_prj' => 'required|array',
            
            'equip' => 'required|array',
            'cate' => 'required|array',
            'sub_cate' => 'required|array',
            'item.name' => ['required'],
            'item.qty' => ['required'],
            
            'item.unit_string' => ['required','string'],
            'item.total' => ['required'],
            'item.unit_price' => ['required']
            
        ],[
            'budget.required' =>[ 'budget_div' => 'please fill budget start and end'],
            'budget.to.required_with' =>[ 'budget_from_div' => 'please fill budget start and end'],
            'budget.to.required_with' =>[ 'budget_to_div' => 'please fill budget start and end'],
            'prj.required' => ['prj_div' => 'Required! Please choose or create new! '],
            'sub_prj.required' => ['sub_prj_div' => 'Required! Please choose or create new! '],
            'equip.required' =>['equip_div' => 'Required! Please choose or create new! '],
            'cate.required' => ['cate_div' =>'Required! Please choose or create new! '],
            'sub_cate.required' =>['sub_cat_div'=> 'Required! Please choose or create new! '],
            'item.name.required' => ['item_name_div'=>'Required!'],
            'item.qty.required' => ['item_qty_div'=>'Required!'],
           
            'item.unit_string.required' => ['item_unit_string_div'=>'Required!'],
            'item.unit_price.required' => ['item_unit_price_div'=>'Required!'],
            'item.total.required' => ['item_total_div'=>'Required!'],
            
        ]);
        
        $inputs = $request->all();

        $prj = $inputs['prj'];
        $main_prj = null;
        if(isset($prj['new'])) {
            $exit = ProposalType::where('name',$prj['new'])->first();
            if(!$exit) {
                $main_prj = ProposalType::create([
                    'name' => $prj['new'] ?? null
                ]);
            }else{
                $main_prj = $exit;
            }
            
        }else{
            $main_prj = ProposalType::find($prj['old']);
        }

        $sub_proposal = $this->checkSubPrj($main_prj, $inputs['sub_prj']);

        $equipment = $this->checkEquipment($inputs['equip']);
        
        $category = $this->checkCategory($inputs['cate']);
        
        $subcategory = $this->checkSubCategory($category, $inputs['sub_cate']);

        $transaction = $this->createTransaction($inputs['budget'],$inputs['department_id']);

        $item_info = $this->createIteminfo($category,$subcategory, $inputs['item']['name']);

        $item = $this->createDemand($inputs['item'], $item_info, $transaction, $sub_proposal, $equipment);

        if($item){
            return response()->json([
                'status' => true,
                'msg' => 'successfully added'
            ]);
        }
        
        //$this->createDemand($inputs['item'],$transaction);
       
    }

    public function createDemand($item, $item_info, $transaction,$proposal,$db_equip) {
        $unit = 
        $demand =  DemandItem::create([
            'name' => $item_info->name,
            'specification' => $item['specification'] ?? null,
            'justification' => $item['justification'] ?? null,
            'item_info_id' => $item_info->id,
            'qty' => $item['qty'] ?? 1 ,
            'equipment_type_id' => $db_equip->id,
            'unit_price' => $item['unit_price'] ,
            'unit' => $item['unit_string'],
            'proposal_id' => $proposal->id,
            'total' => $item['total'] ?? 0,
            'in_transaction_id' => $transaction->id
            ]);
        return $demand;
    }

    

    public function createIteminfo($main_cat, $sub_cat, $name) {

        if(!is_null($sub_cat)) {
            $item_info = ItemInfo::where('name',$name)->where('item_sub_category_id',$sub_cat->id)->where('item_category_id',$main_cat->id)->first();
        }else{
            $item_info = ItemInfo::where('name',$name)->where('item_category_id',$main_cat->id)->first();
        }

        if(!$item_info) {

            $item_info = ItemInfo::create([
                'name' => $name,
                'item_category_id' => $main_cat->id,
                'item_sub_category_id' => $sub_cat->id ?? null
            ]);

        }

        return $item_info;
        
    }

    public function createTransaction($budget, $dep_id) {
        $budget_year = BudgetYear::where('from',$budget['from'])->where('to',$budget['to'])->first();
        if(!$budget_year) {
             $budget_year = BudgetYear::create([
                'from' => $budget['from'],
                'to'  => $budget['to']
               ]);
        }
        
        $in_transaction = InTransaction::where('budget_year_id',$budget_year->id)
                        ->where('department_id',$dep_id)->first();
            if(!$in_transaction) {
                $in_transaction = InTransaction::create([
                                    'budget_year_id' => $budget_year->id,
                                    'department_id' => $dep_id
                                ]);
            }
        
        return $in_transaction;

    }

    public function checkSubCategory($main_cat, $sub_cat) {
        $sub_category = null;

        if(isset($sub_cat['new'])) {
             $exit = ItemSubCategory::where('name',$sub_cat['new'])->where('item_category_id',$main_cat->id)->first();
             if(!$exit) {
                $sub_category = ItemSubCategory::create([
                    'name' => $sub_cat['new'],
                    'item_category_id' => $main_cat->id
                ]);
             }else{
                 $sub_category = $exit;
             }
        }else{
            $sub_category = ItemSubCategory::find($sub_cat['old']);
        }

        return $sub_category;
    }

    public function checkCategory($cate) {
        $category = null;
        if(isset($cate['new'])) {
             $exit = ItemCategory::where('name',$cate['new'])->first();
             if(!$exit) {
                $category = ItemCategory::create([
                    'name' => $cate['new']
                ]);
             }else{
                 $category = $exit;
             }
        }else{
            $category = ItemCategory::find($cate['old']);
        }

        return $category;
    }

    public function checkEquipment($equip) {
        $equipment_type = null;
        if(isset($equip['new'])) {
             $exit = EquipmentType::where('name',$equip['new'])->first();
             if(!$exit) {
                $equipment_type = EquipmentType::create([
                    'name' => $equip['new']
                ]);
             }else{
                 $equipment_type = $exit;
             }
        }else{
            $equipment_type = EquipmentType::find($equip['old']);
        }

        return $equipment_type;
    }

    public function checkSubPrj($main_prj, $prj) {
        $proposal = null;
            if(isset($prj['new'])) {
             $exit = Proposal::where('name',$prj['new'])->where('proposal_type_id',     $main_prj->id)->first();

                if(!$exit) {
                    $proposal = Proposal::create([
                        'name' => $prj['new'] ?? null,
                        'proposal_type_id' => $main_prj->id
                    ]);
                }else{
                    $proposal = $exit;
                }
                
            }else{
                $proposal = Proposal::find($prj['old']);
            }

            return $proposal;
    }

    public function filterWithBudgetDep(Request $request) 
    {
       $budget_uuid = $request->budget_id;
       $dep_uuid = $request->dep_id;
       $in_transaction = InTransaction::whereHas('budgetYear', function($q) use ($budget_uuid){
        $q->where('uuid',$budget_uuid);
       })->WhereHas('department',function($q) use ($dep_uuid) {
        $q->where('uuid',$dep_uuid);
       })->first();
       
       if($in_transaction) {
            return response()->json(['msg' => 'success','data' => $in_transaction]);
       }else{
        return response()->json(['error_msg' => 'not found'],404);
       }
    }
}
