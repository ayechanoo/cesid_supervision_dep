<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Services\EquipmentTypeService;


class MainCategoryController extends Controller
{
    protected $service;
    protected $equip_service;
    public function __construct(CategoryService $service, EquipmentTypeService $equip_service)
    {
        $this->service = $service;
        $this->equip_service = $equip_service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        return view('main_category.index');
    }

    public function categoriesByMinUuid(Request $request) {
        $equp_uuid = $request->equip_uuid;
       $item_categories = $this->service->getItemCategoryByEquipUuid($equp_uuid);  
       return response()->json($item_categories);
    }

    public function getMainCategories(Request $request)
    {
        return response()->json($this->service->makeItemCatDtCollection($request));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equips = $this->equip_service->getAllList();
        
        return view('main_category.create')->with(['equipments' => $equips]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'equipment_type_id' => 'required|exists:equipment_types,id'
        ]);
        $inputs = $request->all();

        
        $response = $this->service->createMainCategory($inputs);

        if ($response) {
            return redirect()->route('main-category.index')
                ->with(['message'=>'Successfully Added!','type' => 'success']);
        } else {
            return back()->with(['message'=>'Something went wrong','type' => 'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equips = $this->equip_service->getAllList();
        $row = $this->service->getItemCategoryByUuid($id);
        return view('main_category.edit')->with(['row' => $row, 'equipments' => $equips]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'equipment_type_id' => 'required|exists:equipment_types,id'
        ]);

        $inputs = $request->only('name','equipment_type_id');
        // dd($inputs);

        $row = $this->service->getItemCategoryByUuid($id);
        $response =  $this->service->mainCategoryUpdate($inputs, $row->id);

        if ($response) {
            return redirect()->route('main-category.index')
                ->with('message', 'Successfully Updated!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
