<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Transaction;

class ItemInfoController extends Controller
{

    protected $service;
    public function __construct(Transaction $service)
    {
        $this->service = $service;
    }
    public function index()
    {
       
        return view('item_info.index');
    }

    public function getItemInfos(Request $request)
    {
        return response()->json($this->service->makeItemInfoDtCollection($request));
    }
    
}
