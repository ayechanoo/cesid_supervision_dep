<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use Maatwebsite\Excel\Sheet;
use App\Imports\SaOneFormatImport;
use App\Exports\SaOneFormatExport;
use App\Exports\AllQtyVsAllDeps;
use App\Imports\ExcelUtitlites;
use App\Imports\ExcelMultipleSheet;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Department;
use App\Models\ItemInfo;
use App\Models\DemandItem;
use App\Models\Proposal;
use App\Models\ProposalType;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\EquipmentType;
use App\Models\BudgetYear;
use App\Models\InTransaction;
use App\Repositories\MainCategory\ItemCategoryRepositoryInterface;
use App\Repositories\ItemSubCategory\ItemSubCategoryRepositoryInterface;
use App\Repositories\ItemInfo\ItemInfoRepositoryInterface;

use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SuperVisionController extends Controller
{
    protected $item_cat_repo ;
    protected $item_sub_cat_repo ;
    protected $item_info_repo ;
    protected $all_qty_vs_deps;
   

    public function __construct(ItemCategoryRepositoryInterface $item_cat_repo, ItemSubCategoryRepositoryInterface $item_sub_cat_repo,ItemInfoRepositoryInterface $item_info_repo, AllQtyVsAllDeps $all_qty_vs_deps) {
        $this->item_cat_repo = $item_cat_repo;
        $this->item_sub_cat_repo = $item_sub_cat_repo;
        $this->item_info_repo = $item_info_repo;
        $this->all_qty_vs_deps = $all_qty_vs_deps;
    }

    public function index() {

        $budget_years = BudgetYear::all();
      
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        if(\App::getLocale() == 'en') {
            $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        }

        $departments = $query->get()->groupBy('minister_name');

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'budget.create';

        array_push($nav_lists,$link);

        return view('industrial_supervision.index',compact('departments'))->with(['nav_lists' => $nav_lists, 'budget_years' => $budget_years]);
    }

    public function reportTotalWithQty() {
    

        $nav_lists = [];
        $link = new \stdClass();
        $link->name = 'Home';
        $link->route = 'sv.index';

        array_push($nav_lists,$link);

        $cates = $this->item_cat_repo->getlist();


        $sub_categories = $this->item_info_repo->allCategoryList();
        

        
        $case_query = '';

        foreach($sub_categories as $k ) {
            foreach($k as $v => $sub) {
            $case_query .= ",sum(case when ISC_name = '".$sub->Item_Category."' then total_qty else '-' end) as '".$sub->Item_Category."_qty"."',sum(case when ISC_name = '".$sub->Item_Category."' then total_amt else '-' end) as '".$sub->Item_Category."_amount"."'";
            }
        }

        

        $raw_query = "with base_query as (select sum(tbl2.qty) as total_qty,sum(tbl2.total) as total_amt, Item_Category as ISC_name,deps.name_mm as dep_name from (select case when isnull(item_sub_category_id)
        then IC.name
        else ISC.name
        end as Item_Category,tbl.qty as qty, tbl.total, demand_trans_id as transaction_id from
        (select DI.name as demand_name,DI.in_transaction_id as demand_trans_id, DI.qty,DI.total, item_infos.item_sub_category_id,item_infos.item_category_id
        from demand_items as DI
        join item_infos
        on item_infos.id = DI.item_info_id
        ) as tbl right join item_sub_categories as ISC on ISC.id = tbl.item_sub_category_id
        right join item_categories as IC on IC.id = tbl.item_category_id)tbl2 
        join in_transactions on in_transactions.id = tbl2.transaction_id
        join departments as deps on deps.id = in_transactions.department_id
        group by deps.name_mm ,tbl2.Item_Category) 
        
        select dep_name as name_mm$case_query from base_query group By dep_name";

        

       $collection = \DB::select($raw_query);

    //    dd($collection);
        

    

        return view('industrial_supervision.item_all',compact('collection','sub_categories','cates'))->with(['nav_lists' => $nav_lists]);
       
        
        
        

    }

    public function importExcel(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'budget_from' => 'required|min:5',
            'budget_to' => 'required',
            'department_id' => 'required',
            'excel_file' => 'required',
        ],[
            'budget_from.required' => 'Budget Start is must.',
            'budget_to.min' => 'Budget End is must.',
            'department_id.required' => 'Please Choose Department',
            'excel_file.required' => 'Please Upload file, it is must in SA-1001',
        ]);
        if($validate->fails()){
        return back()->withErrors($validate->errors())->withInput();
        }

        $budget_from = Carbon::parse($request->budget_from)->format('F Y');
        
        $budget_to = Carbon::parse($request->budget_to)->format('F Y');
        $department_id = $request->department_id;
        $department = Department::with('ministry')->find($department_id);
        
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

      
       
         $the_file = $request->file('excel_file');
        //  dd($the_file);
         $valid_format = $this->checkCorrectformat($the_file);


         if($valid_format == false) {
            return redirect()->route('supervision.index')->with([
                'error_msg'=> 'Incorrected Excel is used! Please check again!']);
         }
       
        $sheet_names = $this->getSheetNames($the_file);

        foreach($sheet_names as $name) {

            $read_data =Excel::toCollection(new ExcelMultipleSheet($name), $the_file, \Maatwebsite\Excel\Excel::ODS)[$name];
            

             $saoneimport = new SaOneFormatImport();
                $saoneimport->collection($read_data);
        $all_sheet_collection[] = $saoneimport->getResults();

        }

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'budget.create';

        array_push($nav_lists,$link);



        return view('industrial_supervision.preview_result',compact('all_sheet_collection','dep_name','budget_from','budget_to'))
        ->with([
            'nav_lists' => $nav_lists ,
            'budget_year_from' => $request->budget_from,
            'budget_year_to' => $request->budget_to,
            'department' => $department,
        ]);

        // $spreadsheet = IOFactory::load($the_file->getRealPath());
        //    $sheet        = $spreadsheet->getActiveSheet();
        //    $row_limit    = $sheet->getHighestDataRow();
        //    $column_limit = $sheet->getHighestDataColumn();
        //    $row_range    = range( 7, $row_limit );
        //    $column_range = range( 'K', $column_limit );
        //    $startcount = 2;

        //    foreach ( $row_range as $row ) {
        //     $data[] = [
        //         'Serial_No' =>$sheet->getCell( 'A' . $row )->getValue(),
        //         'ProjectPropsal' => $sheet->getCell( 'B' . $row )->getValue(),
        //         'equipment_type' => $sheet->getCell( 'C' . $row )->getValue(),
        //         'Item_category' => $sheet->getCell( 'D' . $row )->getValue(),
        //         'Item_subCategory' => $sheet->getCell( 'E' . $row )->getFormattedValue(),
        //         'Item_name' => $sheet->getCell( 'F' . $row )->getValue(),
        //         'Item_specific' =>$sheet->getCell( 'G' . $row )->getValue(),
        //         'Item_qty' =>$sheet->getCell( 'H' . $row )->getValue(),
        //         'Item_unit' =>$sheet->getCell( 'I' . $row )->getValue(),
        //         'Item_unit_price' =>$sheet->getCell( 'J' . $row )->getValue(),
        //         'Item_total' =>$sheet->getCell( 'K' . $row )->getValue(),
        //     ];
        //     $startcount++;
        // }

        // dd($data);
    }

    public function checkCorrectformat($file) {

        $sheetObj = new ExcelUtitlites();

        Excel::import($sheetObj, $file);

       return $sheetObj->getValidStatus();
        
    }

     public function getSheetNames($file) {

        $sheetObj = new ExcelUtitlites();

        Excel::import($sheetObj, $file);

        $sheet_names = $sheetObj->getSheetNames();
        return $sheet_names;
    }

    public function demandStoreWithExcel(Request $request) {
        
       // dd($request);
       $budget_year = BudgetYear::create([
        'from' => $request->budget_from,
        'to'  => $request->budget_to
       ]);

       $in_transaction = InTransaction::create([
        'budget_year_id' => $budget_year->id,
        'department_id' => $request->department_id
       ]);
       
        if(!$request->data) {
            return back()->with(['error' => 'invalid information']);
        }

        $data = json_decode($request->data,true);

        //dd($data);
        // $error_results = $data['errors'];
        
        // $results = $data['valid_data'];

        foreach($data as $k => $v) {

            // $error_results = $data['errors'];
        
        $result = $v['valid_data'];
            
            $this->creatingNewData($result,$in_transaction);
        }

        return redirect()->route('supervision.index');

    
    }

    public function creatingNewData($results,$in_transaction) {
        //dd($results);

         
        foreach($results as $key => $row) {
            // dd($row);
            $exists = $this->checkAllExists($row);
            // dd($exists);
            if($exists == true) {
               
             
             if($row[7]['type'] == false)  {
                 $item_category_id = $row[5]['id'];
                 $item_sub_category_id = $row[6]['id']; 
                 $item_info_name = $row[7]['name'];
 
                 $item_info = ItemInfo::where('name',$row[7]['name'])->first();
 
                 if(!$item_info) {
                     $item_info = ItemInfo::create([
                         'name' => $item_info_name,
                         'item_category_id' => $item_category_id,
                         'item_sub_category_id' => $item_sub_category_id
                     ]);
                 }
 
                 
 
                 
             }else{
                 $qty = $row[9]['value'];
                 $unit_price = (int)$row[11]['value'];
                 $total = $qty*((int)$unit_price);
                 
                // 8 to 12  is for data qty
                $item_info = ItemInfo::find($row[7]['id']);
             
             }
 
             // dd($item_info);

            // dd($row);
 
             $demand_item = DemandItem::create([
                 'name' => $item_info->name,
                 'specification' => trim($row[8]['value']),
                 'item_info_id' => $item_info->id,
                 'qty' => $qty,
                 'equipment_type_id' => $row[4]['id'],
                 'unit_price' => $unit_price ,
                 'unit' => $row[10]['value'],
                 'proposal_id' => $row[3]['id'],
                 'total' => $total ?? 0,
                 'in_transaction_id' => $in_transaction->id,
                  'justification' => isset($row[18]['value']) ? trim($row[18]['value'])  : null
                ]);
             
            }else{

              
              
               
                 $qty = (isset($row[9]['value']) && !is_null($row[9]['value']))  ? $row[9]['value'] : 1 ;
                 $unit_price = $row[11]['value'] ?? $row[12]['value'];
                
               
                 
                 $unit_price =  ((int)$unit_price) * 1000;

                
                    $total = ((int)$qty) * ($unit_price);

                    
                    
                   
                 $proposal = $this->checkOrNewProposal($row);  

                //  dd($proposal);
                 
                 $equipment_id = $this->checkorNewEquipId($row[4]); 
 
                 $item_info = $this->checkItemInfoId($key,$row);
                 
                 $demand_item = DemandItem::create([
                     'name' => $item_info->name,
                     'specification' => $row[8]['value'] ?? null,
                     'item_info_id' => $item_info->id,
                     'qty' => $qty,
                     'equipment_type_id' => $equipment_id,
                     'unit_price' => $unit_price ,
                     'unit' => $row[10]['value'] ?? '',
                     'proposal_id' => $proposal->id,
                     'total' => $total ?? 0,
                     'in_transaction_id' => $in_transaction->id,
                     'justification' => isset($row[18]['value']) ? trim($row[18]['value'])  : null
                    ]);
                    
            }
         }
 
         \Log::info('successfully added!');
    }

    public function checkItemInfoId($key = null, $row) {

         $equipment_name = $row[4]['name'];

        // if($row[7]['name'] == 'aws'|| $row[7]['name'] =='AWS') {
        //     dd($row);
        // }
        
        $item_info_query = ItemInfo::query()->where('name', $row[7]['name'])
                ->whereHas('itemSubCategory', function($q) use ($row) {
                    $q->where('name', trim($row[6]['name']));
                })->orWhereHas('itemCategory', function($q) use ($row) {
                    $q->where('name', trim($row[5]['name']));
                })
                ->first();

                if(!is_null($row[6]['name'])) {
                $item_info_query = ItemInfo::query()->where('name', trim($row[7]['name']))
                        ->whereHas('itemSubCategory', function($q) use ($row) {
                            $q->where('name', trim($row[6]['name']));
                        })->WhereHas('itemCategory', function($q) use ($row) {
                            $q->where('name', trim($row[5]['name']));
                        })
                        ->first();
                }else{
                    $item_info_query = ItemInfo::query()->where('name', $row[7]['name'])
                        ->WhereHas('itemCategory', function($q) use ($row) {
                            $q->where('name', trim($row[5]['name']));
                        })
                        ->first();
        
                }

        // if($row[7]['name'] == 'aws'|| $row[7]['name'] =='AWS') {
        //     dd($item_info_query);
        // }
        
       if(!$item_info_query) {
        

            if(!is_null($row[6]['name'])) {
                

                $item_sub_category = ItemSubCategory::where('name',$row[6]['name'])->whereHas('category', function($q) use ($row) {
                    $q->where('name',trim($row[5]['name']));
                })->first();

                
                
                if(!$item_sub_category) {

                   

                    $item_categ = ItemCategory::where('name', $row[5]['name'])->whereHas('equipmentType',function($q) use ($equipment_name) {
                        $q->where('name',$equipment_name);
                    })->first();
                    if(!$item_categ) {
                        $equip = EquipmentType::where('name',$equipment_name)->first();
                        $item_categ = ItemCategory::create([
                                        'name' => strtolower(trim($row[5]['name'])),
                                        'equipment_type_id' => $equip->id,
                                        ]);
                    }

                    $item_sub_category = ItemSubCategory::create([
                        'name'  => strtolower(trim($row['6']['name'])),
                        'item_category_id' => $item_categ->id
                    ]);

                }

                $itemInfo = ItemInfo::create([
                    'name' => trim($row[7]['name']),
                    'item_sub_category_id' => $item_sub_category->id,
                    'item_category_id' => $item_sub_category->item_category_id

                ]);

                return $itemInfo;



                // $item_subcategory = ItemSubCategory::where('name', $row[6]['name'])->first();
                // if(!$item_subcategory) {
                //     $item_subcategory
                // }

            }else{


                //dd('hmm there is name');

                $item_categ = ItemCategory::where('name', $row[5]['name'])->whereHas('equipmentType',function($q) use ($equipment_name) {
                        $q->where('name',$equipment_name);
                    })->first();


                
                    if(!$item_categ) {
                        $equip = EquipmentType::where('name',$equipment_name)->first();
                        if($equip) {

                        }else{
                            dd($key);
                            dd($equipment_name);
                        }
                        $item_categ = ItemCategory::create([
                                        'name' => strtolower(trim($row[5]['name'])),
                                        'equipment_type_id' => $equip->id,
                                        ]);
                    }


                    $itemInfo = ItemInfo::create([
                        'name' => trim($row[7]['name']) ?? null,
                        'item_sub_category_id' => null,
                        'item_category_id' => $item_categ->id
    
                    ]);
    
                    return $itemInfo;

            }

       }

       return $item_info_query;

    }

    public function checkorNewEquipId($item) {
        if($item['type'] == true) {
            return $item['id'];
        }else{
            $data = EquipmentType::where('name',strtolower($item['name']))->first();
            if(!$data) {
                $data = EquipmentType::create([
                    'name' => strtolower($item['name']),
                ]);
            }
            
            return $data->id;
        }
    }

    public function checkorNewProposal($row) {
       
        $proposal = Proposal::where('name',$row[3]['name'])->whereHas('type',function($q) use ($row) {
            $q->where('name',$row[2]['name']);
        })->first();

        

        if(!$proposal) { 
            
            $type = ProposalType::where('name',$row[2]['name'])->first();

            if(!$type) {
                $type = ProposalType::create([
                    'name' => $row['2']['name']
                ]);
            }

        
            $proposal = Proposal::create([
                'name' => strtolower($row[3]['name']),
                'proposal_type_id' => $type->id
            ]);
        }
       // dd($proposal);

        return $proposal;
    }

    public function exportSaone() {
        $budget_years = BudgetYear::all();
      
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        if(\App::getLocale() == 'en') {
            $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        }

        $departments = $query->get()->groupBy('minister_name');

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'budget.create';

        array_push($nav_lists,$link);

        return view('industrial_supervision.saone_export_form',compact('departments'))->with(['nav_lists' => $nav_lists, 'budget_years' => $budget_years]);
    }

    public function  demandItemExcelExport(Request $request) {

        $budget_year = $request->budget_year;
        $department_id = $request->department_id;

        
        $department = Department::with('ministry')->find($department_id);
       
    
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};
        $ministry_name = $department->ministry->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

        
        $budget = BudgetYear::find($budget_year)->first();
        
        $nav_lists = [];
            $data = DemandItem::with('inTransaction')
                    ->whereHas('inTransaction',function($q) use ($budget_year, $department_id) {
                        $q->where('budget_year_id',$budget_year)->where('department_id',$department_id);
                    })
               ->join('equipment_types as equp','equp.id','demand_items.equipment_type_id')
                ->join('proposals as pro','pro.id','demand_items.proposal_id')
                ->join('proposal_types as pro_types','pro_types.id','pro.proposal_type_id')
                ->join('item_infos as ifs','ifs.id','demand_items.item_info_id')
                ->leftJoin('item_sub_categories as item_subs','item_subs.id','ifs.item_sub_category_id')
                ->leftJoin('item_categories as item_cats','item_cats.id','ifs.item_category_id')
                ->selectRaw('demand_items.*, pro_types.name as pro_type,pro.name as proposal_name,equp.name as equp_name,  
                ifs.name as item_info_name , 
                case when isnull(item_subs.name)
        then item_cats.name
        else item_subs.name
        end as item_sub_name')->
                get();
        $collection  = $data->groupBy(['pro_type','proposal_name','equp_name'])->toArray();

        //dd($collection);
        
        return view('industrial_supervision.export_data',compact('collection','department','budget','dep_name','ministry_name'))->with(['nav_lists' => $nav_lists, 'budget_year_id' => $budget_year, 'department_id' => $department_id]);
        
    }

    public function downloadExcelAllQtyVsDeps(Request $request) {
       $export = $this->all_qty_vs_deps;
        
        return Excel::download($export, 'mn.xlsx');
    }

    public function downloadExcel(Request $request) {
       
        
        $export = new SaOneFormatExport($request->budgetYear_id, $request->department_id);
        return Excel::download($export, 'mn.xlsx');
    }

    public function checkAllExists($row) {
        // dd($row);
        $status = true;
        for($x = 2; $x <= 6 ; $x++) {
            if($row[$x]['type'] == false) {
                
                return false;
            }
            $status = $row[$x]['type'];
        }
        
        if($status == true) {
            return true;
        }
        return false;
    }

    

}

