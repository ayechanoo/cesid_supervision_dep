<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProposalTypeService;

class ProposalTypeController extends Controller
{
    protected $service;
    public function __construct(ProposalTypeService $service)
    {
        $this->service = $service;
    }
    
    public function index()
    {
        return view('proposal_type.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proposal_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);
        $inputs = $request->all();

        $response = $this->service->createProposalType($inputs);

        if($response){
            return redirect()->route('proposal-type.index')
            ->with('message', 'Successfully Added!');
        }
        else{
            return back()->with('error message' , 'Something went wrong!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = $this->service->getProposalTypeByUuid($id);
        return view('proposal_type.edit')->with(['row' => $row]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        $inputs = $request->only('name');

        $row = $this->service->getProposalTypeByUuid($uuid);
        $response =  $this->service->proposalTypeUpdate($inputs, $row->id);

        if ($response) {
            return redirect()->route('proposal-type.index')
                ->with('message', 'Successfully Updated!');
        } else {
            return back()->with('error_msg', 'something went wrong!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProposalTypes(Request $request)
    {
        return response()->json($this->service->makeDtCollection($request));
    }
}
