<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proposal;
use App\Models\ProposalType;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\ItemInfo;
use App\Models\Department;
use App\Models\DemandItem;
use App\Models\BudgetYear;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proposal_types = ProposalType::select('name')->get();
        $propsals = Proposal::with('type')->get();
        $item_categs = ItemCategory::all();
        $item_sub_categs = ItemSubCategory::with('category')->get();
        $budget_years = BudgetYear::all();
        $item_infos = ItemInfo::all();
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        if(\App::getLocale() == 'en') {
            $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        }

        $departments = $query->get()->groupBy('minister_name');

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'Import Excel';
        $link->route = 'supervision.index';

        array_push($nav_lists,$link);


        return view('industrial_supervision.budget.create')->with([
            'proposal_types' => $proposal_types,
            'proposals' => $propsals,
            'item_categs' => $item_categs,
            'item_sub_categs' => $item_sub_categs,
            'item_infos' => $item_infos,
            'budget_years' => $budget_years,
            'departments' => $departments
        ])->with(['nav_lists' => $nav_lists]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
