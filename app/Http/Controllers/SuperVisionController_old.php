<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use Maatwebsite\Excel\Sheet;
use App\Imports\SaOneFormatImport;
use App\Exports\SaOneFormatExport;
use App\Imports\ExcelUtitlites;

use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Department;
use App\Models\ItemInfo;
use App\Models\DemandItem;
use App\Models\Proposal;
use App\Models\ProposalType;
use App\Models\ItemCategory;
use App\Models\ItemSubCategory;
use App\Models\EquipmentType;
use App\Models\BudgetYear;
use App\Models\InTransaction;

use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SuperVisionController extends Controller
{
    public function index() {

        $budget_years = BudgetYear::all();
      
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        if(\App::getLocale() == 'en') {
            $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        }

        $departments = $query->get()->groupBy('minister_name');

        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'budget.create';

        array_push($nav_lists,$link);

        return view('industrial_supervision.index',compact('departments'))->with(['nav_lists' => $nav_lists, 'budget_years' => $budget_years]);
    }

    public function importExcel(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'budget_from' => 'required|min:5',
            'budget_to' => 'required',
            'department_id' => 'required',
            'excel_file' => 'required',
        ],[
            'budget_from.required' => 'Budget Start is must.',
            'budget_to.min' => 'Budget End is must.',
            'department_id.required' => 'Please Choose Department',
            'excel_file.required' => 'Please Upload file, it is must in SA-1001',
        ]);
        if($validate->fails()){
        return back()->withErrors($validate->errors())->withInput();
        }

        $budget_from = Carbon::parse($request->budget_from)->format('F Y');
        
        $budget_to = Carbon::parse($request->budget_to)->format('F Y');
        $department_id = $request->department_id;
        $department = Department::with('ministry')->find($department_id);
        
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

      
       
         $the_file = $request->file('excel_file');
        //  dd($the_file);
       
        $sheet = new ExcelUtitlites();
        

        // $sheetNames = Excel::load($the_file)->getSheetNames();

       
        // $sheetClass->import($sheet,$this_file);
        Excel::import($sheet, $the_file);

        
        $all_data = $sheet->getSheets();

        $all_sheet_collection = [];
        // $saoneimport = new SaOneFormatImport();
        // $saoneimport->collection($all_data[2]);

        // dd($saoneimport->getResults());
        
        // dd($all_data);
        foreach($all_data as $value) {
           $saoneimport = new SaOneFormatImport();
        //    $saoneimport->setHeadingRow(8);
           $saoneimport->collection($value);
           
        //    dd($saoneimport->getResults());
           $all_sheet_collection[] = $saoneimport->getResults();
           
        }

        

        // foreach($all_data as $data) {
        //     $list = new SaOneFormatImport();
        //     Excel::import($list,$data);
        //     dd($list->getResults());
        // }
       
        // $result =  $sheet->getResults();

        // dd($result);


        $nav_lists = [];

        $link = new \stdClass();
        $link->name = 'New Budget';
        $link->route = 'budget.create';

        array_push($nav_lists,$link);



        return view('industrial_supervision.preview_result',compact('all_sheet_collection','dep_name','budget_from','budget_to'))
        ->with([
            'nav_lists' => $nav_lists ,
            'budget_year_from' => $request->budget_from,
            'budget_year_to' => $request->budget_to,
            'department' => $department,
        ]);

        // $spreadsheet = IOFactory::load($the_file->getRealPath());
        //    $sheet        = $spreadsheet->getActiveSheet();
        //    $row_limit    = $sheet->getHighestDataRow();
        //    $column_limit = $sheet->getHighestDataColumn();
        //    $row_range    = range( 7, $row_limit );
        //    $column_range = range( 'K', $column_limit );
        //    $startcount = 2;

        //    foreach ( $row_range as $row ) {
        //     $data[] = [
        //         'Serial_No' =>$sheet->getCell( 'A' . $row )->getValue(),
        //         'ProjectPropsal' => $sheet->getCell( 'B' . $row )->getValue(),
        //         'equipment_type' => $sheet->getCell( 'C' . $row )->getValue(),
        //         'Item_category' => $sheet->getCell( 'D' . $row )->getValue(),
        //         'Item_subCategory' => $sheet->getCell( 'E' . $row )->getFormattedValue(),
        //         'Item_name' => $sheet->getCell( 'F' . $row )->getValue(),
        //         'Item_specific' =>$sheet->getCell( 'G' . $row )->getValue(),
        //         'Item_qty' =>$sheet->getCell( 'H' . $row )->getValue(),
        //         'Item_unit' =>$sheet->getCell( 'I' . $row )->getValue(),
        //         'Item_unit_price' =>$sheet->getCell( 'J' . $row )->getValue(),
        //         'Item_total' =>$sheet->getCell( 'K' . $row )->getValue(),
        //     ];
        //     $startcount++;
        // }

        // dd($data);
    }

    public function demandStoreWithExcel(Request $request) {
        
       // dd($request);
       $budget_year = BudgetYear::create([
        'from' => $request->budget_from,
        'to'  => $request->budget_to
       ]);

       $in_transaction = InTransaction::create([
        'budget_year_id' => $budget_year->id,
        'department_id' => $request->department_id
       ]);
       
        if(!$request->data) {
            return back()->with(['error' => 'invalid information']);
        }

        $data = json_decode($request->data,true);

        //dd($data);
        // $error_results = $data['errors'];
        
        // $results = $data['valid_data'];

        foreach($data as $k => $v) {

            // $error_results = $data['errors'];
        
        $result = $v['valid_data'];
            
            $this->creatingNewData($result,$in_transaction);
        }

        return redirect()->route('supervision.index');

    
    }

    public function creatingNewData($results,$in_transaction) {
        //dd($results);

         
        foreach($results as $row) {
            
            $exists = $this->checkAllExists($row);
            if($exists == true) {
             
             if($row[7]['type'] == false)  {
                 $item_category_id = $row[5]['id'];
                 $item_sub_category_id = $row[6]['id']; 
                 $item_info_name = $row[7]['name'];
 
                 $item_info = ItemInfo::where('name',$row[7]['name'])->first();
 
                 if(!$item_info) {
                     $item_info = ItemInfo::create([
                         'name' => $item_info_name,
                         'item_category_id' => $item_category_id,
                         'item_sub_category_id' => $item_sub_category_id
                     ]);
                 }
 
                 
 
                 
             }else{
                 $qty = $row[9]['value'];
                 $unit_price = $row[11]['value'];
                 $total = $qty*$unit_price;
                 
                // 8 to 12  is for data qty
                $item_info = ItemInfo::find($row[7]['id']);
             
             }
 
             // dd($item_info);
 
             $demand_item = DemandItem::create([
                 'name' => $item_info->name,
                 'specification' => $row[8]['value'],
                 'item_info_id' => $item_info->id,
                 'qty' => $qty,
                 'equipment_type_id' => $row[4]['id'],
                 'unit_price' => $unit_price ,
                 'unit' => $row[10]['value'],
                 'proposal_id' => $row[3]['id'],
                 'total' => $total ?? 0,
                 'in_transaction_id' => $in_transaction->id
                ]);
             
            }else{
 
                 $qty = $row[9]['value'];
                 $unit_price = $row[11]['value'];
                 
                 $unit_price =  filter_var($unit_price, FILTER_SANITIZE_NUMBER_INT) * 10;
                    $total = $qty*($unit_price);
                    
                   
                 $proposal = $this->checkOrNewProposal($row);  
                 $equipment_id = $this->checkorNewEquipId($row[4]); 
 
                 $item_info = $this->checkItemInfoId($row);
                 $demand_item = DemandItem::create([
                     'name' => $item_info->name,
                     'specification' => $row[8]['value'],
                     'item_info_id' => $item_info->id,
                     'qty' => $qty,
                     'equipment_type_id' => $equipment_id,
                     'unit_price' => $unit_price ,
                     'unit' => $row[10]['value'],
                     'proposal_id' => $proposal->id,
                     'total' => $total ?? 0,
                     'in_transaction_id' => $in_transaction->id
                    ]);
            }
         }
 
         \Log::info('successfully added!');
    }

    public function checkItemInfoId($row) {
        $item_info_query = ItemInfo::query()->where('name', $row[7]['name'])
                ->whereHas('itemSubCategory', function($q) use ($row) {
                    $q->where('name', $row[6]['name']);
                })->orWhereHas('itemCategory', function($q) use ($row) {
                    $q->where('name', $row[5]['name']);
                })
                ->first();
       if(!$item_info_query) {

            if(!is_null($row[6]['name'])) {

                $item_sub_category = ItemSubCategory::where('name',$row[6]['name'])->whereHas('category', function($q) use ($row) {
                    $q->where('name',$row[5]['name']);
                })->first();
                
                if(!$item_sub_category) {

                    $item_categ = ItemCategory::where('name', $row[5]['name'])->first();
                    if(!$item_categ) {
                        $item_categ = ItemCategory::create(['name' => strtolower($row[5]['name'])]);
                    }

                    $item_sub_category = ItemSubCategory::create([
                        'name'  => strtolower($row['6']['name']),
                        'item_category_id' => $item_categ->id
                    ]);

                }

                $itemInfo = ItemInfo::create([
                    'name' => $row[7]['name'],
                    'item_sub_category_id' => $item_sub_category->id,
                    'item_category_id' => $item_sub_category->item_category_id

                ]);

                return $itemInfo;



                // $item_subcategory = ItemSubCategory::where('name', $row[6]['name'])->first();
                // if(!$item_subcategory) {
                //     $item_subcategory
                // }

            }else{

                $item_categ = ItemCategory::where('name', $row[5]['name'])->first();
                    if(!$item_categ) {
                        $item_categ = ItemCategory::create(['name' => strtolower($row[5]['name'])]);
                    }

                    $itemInfo = ItemInfo::create([
                        'name' => $row[7]['name'],
                        'item_sub_category_id' => null,
                        'item_category_id' => $item_sub_category->item_category_id
    
                    ]);
    
                    return $itemInfo;

            }

       }

       return $item_info_query;

    }

    public function checkorNewEquipId($item) {
        if($item['type'] == true) {
            return $item['id'];
        }else{
            $data = EquipmentType::where('name',strtolower($item['name']))->first();
            if(!$data) {
                $data = EquipmentType::create([
                    'name' => strtolower($item['name']),
                ]);
            }
            
            return $data->id;
        }
    }

    public function checkorNewProposal($row) {
        $proposal = Proposal::where('name',$row[3]['name'])->whereHas('type',function($q) use ($row) {
            $q->where('name',$row[2]['name']);
        })->first();

        if(!$proposal) {    
            $type = ProposalType::where('name',$row[2])->first();

            if(!$type) {
                $type = ProposalType::create([
                    'name' => $row['2']['name']
                ]);
            }
            
            $proposal = Proposal::create([
                'name' => strtolower($row[3]['name']),
                'proposal_type_id' => $type->id
            ]);
        }

        return $proposal;
    }

    public function  demandItemExcelExport(Request $request) {

        $budget_year = $request->budget_year;
        $department_id = $request->department_id;

        
        $department = Department::with('ministry')->find($department_id);
       
    
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};
        $ministry_name = $department->ministry->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

        
        $budget = BudgetYear::find($budget_year)->first();
        
        $nav_lists = [];
            $data = DemandItem::with('inTransaction')
                    ->whereHas('inTransaction',function($q) use ($budget_year, $department_id) {
                        $q->where('budget_year_id',$budget_year)->where('department_id',$department_id);
                    })
                ->join('equipment_types as equp','equp.id','demand_items.equipment_type_id')
                ->join('proposals as pro','pro.id','demand_items.proposal_id')
                ->join('proposal_types as pro_types','pro_types.id','pro.proposal_type_id')
                ->join('item_infos as ifs','ifs.id','demand_items.item_info_id')
                ->join('item_sub_categories as item_subs','item_subs.id','ifs.item_sub_category_id')
                ->selectRaw('demand_items.*, pro_types.name as pro_type,pro.name as proposal_name,equp.name as equp_name,  
                ifs.name as item_info_name , item_subs.name as item_sub_name')->
                get();
        $collection  = $data->groupBy(['pro_type','proposal_name','equp_name'])->toArray();
        
        return view('industrial_supervision.export_data',compact('collection','department','budget','dep_name','ministry_name'))->with(['nav_lists' => $nav_lists, 'budget_year_id' => $budget_year, 'department_id' => $department_id]);
        
    }

    public function downloadExcel(Request $request) {
        
        
        $export = new SaOneFormatExport($request->budgetYear_id, $request->department_id);
        return Excel::download($export, 'mn.xlsx');
    }

    public function checkAllExists($row) {
        // dd($row);
        $status = true;
        for($x = 2; $x <=6 ; $x++) {
            if($status == false) {
                break;
                return $row[$x];
            }
            $status = $row[$x]['type'];
        }
        if($status == true) {
            return true;
        }
    }

    

}
