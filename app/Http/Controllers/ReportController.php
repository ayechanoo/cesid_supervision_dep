<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\MainCategory\ItemCategoryRepositoryInterface;
use App\Repositories\ItemSubCategory\ItemSubCategoryRepositoryInterface;
use App\Repositories\Ministry\MinistryRepositoryInterface as MiniInterface;
use App\Repositories\ItemInfo\ItemInfoRepositoryInterface;
use App\Repositories\EquipmentType\EquipmentTypeRepositoryInterface as EquipInterface;
use App\Exports\AllQtyVsAllDeps;

use App\Models\BudgetYear;
use App\Models\InTransaction;
use App\Models\ItemCategory;

use App\Models\Department;

class ReportController extends Controller
{

    protected $item_cat_repo ;
    protected $item_sub_cat_repo ;
    protected $item_info_repo ;
    protected $all_qty_vs_deps;
   
    protected $min_repo;
    protected $equip_repo;
   

    public function __construct(ItemCategoryRepositoryInterface $item_cat_repo, ItemSubCategoryRepositoryInterface $item_sub_cat_repo,ItemInfoRepositoryInterface $item_info_repo, AllQtyVsAllDeps $all_qty_vs_deps,MiniInterface $min_repo, EquipInterface $equip_repo ) {
        $this->item_cat_repo = $item_cat_repo;
        $this->item_sub_cat_repo = $item_sub_cat_repo;
        $this->item_info_repo = $item_info_repo;
        $this->all_qty_vs_deps = $all_qty_vs_deps;
        $this->min_repo = $min_repo;
        $this->equip_repo = $equip_repo;
    }

    public function depRecordsByMiniPreview(Request $request)
    {
        $budget_year_id = $request->budget_year;
        $ministry_uuid = $request->minstry_id;
        //dd($ministry_uuid);
        $equipment_type_id = $request->equipment_type_id;
        $cat_uuid_list = $request->categories;
        $cat_list =  $this->item_cat_repo->getFilterList(['uuids'=> $cat_uuid_list]);
        $case_query = '';
        foreach($cat_list as $item_cat)
        {
            if(count($item_cat->subcategories) > 0) {
                foreach($item_cat->subcategories as $k => $v)
                {
                    $case_query .= ",sum(case when category_id = {$v->id} then re_qty else 0 end) as 're_isc_{$v->id}_qty', sum(case when category_id = {$v->id} then re_total else 0 end) as 're_isc_{$v->id}_total',sum(case when category_id = {$v->id} then be_qty else 0 end) as 'be_isc_{$v->id}_qty',sum(case when category_id = {$v->id} then be_total else 0 end) as 'be_isc_{$v->id}_total'";
                 }
                
            }else{
                $case_query .= ",sum(case when category_id = {$item_cat->id} then re_qty else 0 end) as 're_ic_{$item_cat->id}_qty', sum(case when category_id = {$item_cat->id} then re_total else 0 end) as 're_ic_{$item_cat->id}_total',sum(case when category_id = {$item_cat->id} then be_qty else 0 end) as 'be_ic_{$item_cat->id}_qty',sum(case when category_id = {$item_cat->id} then be_total else 0 end) as 'be_ic_{$item_cat->id}_total'";
            }
            // $case_query .= ",sum(case when ISC_name = '".$sub->Item_Category."' then total_qty else '-' end) as '".$sub->Item_Category."_qty"."',sum(case when ISC_name = '".$sub->Item_Category."' then total_amt else '-' end) as '".$sub->Item_Category."_amount"."'";
            
        }

        $raw_query = 
                        "with base_query as (select be_trans.department_id as dep_id , case when isnull(item_infos.item_sub_category_id)
                            then ic.id
                            else isc.id
                            end as category_id, 
                             sum(re_items.qty) as re_qty, sum(re_items.total) as re_total,
                             sum(be_items.qty) as be_qty, sum(be_items.total) as be_total 
                             from be_has_retrans as final_trans 
                            join re_trans as re_items on re_items.be_has_retrans_id = final_trans.id
                            join demand_items as be_items on be_items.id = re_items.demand_item_id
                            join item_infos as item_infos on item_infos.id = be_items.item_info_id
                            left join item_categories as ic on ic.id = item_infos.item_category_id
                            left join item_sub_categories as isc on isc.id = item_infos.item_sub_category_id
                            join in_transactions as be_trans on be_trans.id = final_trans.in_transaction_id
                            where reviewed_by = 3   group by dep_id,category_id)
                            select deps.name_mm as department_name$case_query
                            from base_query right join departments as deps on deps.id = base_query.dep_id 
                            left join ministries as minis on minis.id = deps.ministry_id 
                            where minis.uuid='{$ministry_uuid}'
                            group By department_name;";

                $collection = \DB::select($raw_query);
                
                        


        return view('industrial_supervision.dep_specific_cat_by_mini')->with([
            'cat_list' => $cat_list,
            'collection' => $collection
        ]);

    }




    

    public function reportsByBYandDep() {

        $ministires = $this->min_repo->getAll();
        $equipments = $this->equip_repo->getAll();





        $sub_categories = $this->item_info_repo->categoryByEquipId(1);


       // dd($sub_categories);


        // $sub_categories = $this->item_info_repo->allCategoryList();
        $budget_years = BudgetYear::all();
      
        $query = Department::leftJoin('ministries','departments.ministry_id','ministries.id');
        if(\App::getLocale() == 'en') {
            $query->selectRaw('ministries.name_eng as minister_name, departments.name_eng as department_name,departments.id as department_id');
        }else{
            $query->selectRaw('ministries.name_mm as minister_name, departments.name_mm as department_name, departments.id as department_id');
        }

        $departments = $query->get()->groupBy('minister_name');

        $nav_lists = [];

        return view('reports.qtis_with_each_dep_form')->with(['nav_lists' => $nav_lists, 'budget_years' => $budget_years, 'departments' => $departments,
            'sub_categories'=> $sub_categories,
            'ministries'=> $ministires,
            'equipments'=> $equipments

            ]);
    }

    public function collectionByBYandDep(Request $request) {

        $nav_lists = [];

        $budget_year = $request->budget_year;
        $department_id = $request->department_id;

        
        $department = Department::with('ministry')->find($department_id);
       
    
        $dep_name = $department->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};
        $ministry_name = $department->ministry->{'name_'.((\App::getLocale()=='en' ? 'eng' : \App::getLocale()))};

        
        $budget = BudgetYear::find($budget_year)->first();

        $transaction = InTransaction::where('budget_year_id',$budget->id)->where('department_id',$department->id)->first();
        
        if(!$transaction) {
            return back()->with(['error_msg'=> 'Please check budget year!']);
        }

        // dd($request);
        $cates = $this->item_cat_repo->getlist();

        $sub_categories = $this->item_info_repo->allCategoryList();

        
        $case_query = '';

        foreach($sub_categories as $k ) {
            foreach($k as $v => $sub) {
            $case_query .= ",sum(case when ISC_name = '".$sub->Item_Category."' then total_qty else '-' end) as '".$sub->Item_Category."_qty"."',sum(case when ISC_name = '".$sub->Item_Category."' then total_amt else '-' end) as '".$sub->Item_Category."_amount"."'";
            }
        }

        $raw_query = "with base_query as (select sum(tbl2.qty) as total_qty,sum(tbl2.total) as total_amt, Item_Category as ISC_name,deps.name_mm as dep_name from (select case when isnull(item_sub_category_id)
        then IC.name
        else ISC.name
        end as Item_Category,case when isnull(item_sub_category_id)
        then tbl.qty
        else tbl.qty
        end as qty, case when isnull(item_sub_category_id)
        then tbl.total
        else tbl.total
        end as total, demand_trans_id as transaction_id from
        (select DI.name as demand_name,DI.in_transaction_id as demand_trans_id, DI.qty,DI.total, item_infos.item_sub_category_id,item_infos.item_category_id
        from demand_items as DI
        join item_infos
        on item_infos.id = DI.item_info_id
        ) as tbl left join item_sub_categories as ISC on ISC.id = tbl.item_sub_category_id
        left join item_categories as IC on IC.id = tbl.item_category_id)tbl2 
        join in_transactions on in_transactions.id = tbl2.transaction_id
        join departments as deps on deps.id = in_transactions.department_id
        where  transaction_id = {$transaction->id}
        group by deps.name_mm ,tbl2.Item_Category) 
        
        select dep_name as name_mm$case_query from base_query group By dep_name";

        

        

       $collection = \DB::select($raw_query);


       return view('industrial_supervision.item_all',compact('collection','sub_categories'))->with(['nav_lists' => $nav_lists]);
       
    }


}
