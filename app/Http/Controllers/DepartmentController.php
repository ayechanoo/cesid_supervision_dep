<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Services\DepartmentService;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\View\Factory;
use Illuminate\View\View;
use App\Services\Helper;

class DepartmentController extends Controller
{
    protected $service;
    protected $helper;
    public function __construct(DepartmentService $service,Helper $helper)
    {
        $this->service = $service;
        $this->helper = $helper;
    }

    public function index(Request $request)
    {
        return view('Department.index');
    }

    public function create()
    {
        $ministries = $this->service->getAllMinistries();

        return view('Department.create', ['ministries' => $ministries]);
    }

   
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name_eng' => 'required',
            'name_mm' => 'required',
            'ministry_id' => 'required',
            'level' => 'nullable|numeric',

        ]);
        $inputs=$request->all();

        try {
            $this->service->departmentCreate($inputs);
            return redirect()->route('department.index')->with('message', 'Successfully Saved!');
        } catch (\Exception $e) {
            DB::rollback();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return redirect()->route('department.index')->with('error_msg', 'Action Failed!');
        }
    }

   
    public function edit($uuid)
    {
        $row = $this->service->departmentGetDataByUUid($uuid);
        $ministries = $this->service->getAllMinistries();
        return view('Department.edit')->with(['row' => $row,'ministries' => $ministries]);
    }


    public function update(Request $request, $uuid)
    {
        $validated = $request->validate([
            'name_eng' => 'required',
            'name_mm' => 'required',
            'minisrty_id' => 'required',
            'level' => 'nullable|numeric',
        ]);
        $success = false;
        $validated = $request->validate([
            'name_eng' => 'required',
            'name_mm' => 'required',
            'ministry_id' => 'required',
            'level' => 'required',
        ]);
        $inputs = $request->only('name_mm', 'name_eng', 'ministry_id', 'level');

        $row = $this->service->departmentGetDataByUUid($uuid);

        try {
            $data = $this->service->departmentUpdate($inputs, $row->id);
            return redirect()->route('department.index')->with('message', 'Successfully Saved!');
        } catch (\Exception $e) {
            DB::rollback();
            $this->helper->errorLogger($e->getMessage(),$e->getFile(),$e->getLine());
            return redirect()->route('department.index')->with('error_msg', 'Action Failed!');
        }
    }

    public function destroy($uuid)
    {
        $relationToVehicleIssueInfo = $this->service->getVehicleIssueInfoCountByDepartment($uuid);
        $relationToVehicleReceiveInfo = $this->service->getVehicleReceiveInfoCountByDepartment($uuid);


        if ($relationToVehicleIssueInfo || $relationToVehicleReceiveInfo) {
            return response()->json(['status' => false , 'message' => 'you cannot delete due to relationship'], 300);
        }
        $response = $this->service->deleteDepartment($uuid);

        return response()->json(['success' => $response]);
    }

    public function getDepartments(Request $request)
    {
        return response()->json($this->service->makeDeparmentDtCollection($request));
    }

    public function depsByMinId(Request $request) {
        $min_uuid = $request->mini_id;
        return $this->service->getDepartmentByMinistryId($min_uuid);
    } 
}
