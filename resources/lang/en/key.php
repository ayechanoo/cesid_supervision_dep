<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'save' => 'Save',
    'cancel' => 'Cancel',
    'addNew' => 'Add New',
    'name' => 'Name',
    'action' => 'Action',
    'proposalList' => 'Proposal List',
    'proposalType' => 'Proposal Type',
    'createProposal' => 'Proposal Create',
    'newProposal' => 'New Proposal',
    'editProposal' => 'Proposal Edit',
    'updateProposal' => 'Update Proposal',

    'proposalTypeList' => 'Proposal Type List',
    'createProposalType' => 'Proposal Type Create',
    'newProposalType' => 'New Proposal Type',
    'editProposalType' => 'Proposal Type Edit',
    'updateProposalType' => 'Update Proposal Type',

    

    'equipmentTypeList' => 'Equipment Type List',
    'createEquipmentType' => 'Equipment Type Create',
    'newEquipmentType' => 'New Equipment Type',
    'equipmentType' => 'Equipment Type',


    'subcategoryList' => 'Sub Category List',
    'categoryType' => 'Main Category ',

    'createSubCategory' => 'SubCategory Create',
    'newSubCategory' => 'New SubCategory',
    'editSubCategory' => 'SubCategory Edit',
    'updateSubCategory' => 'Update SubCategory',
    'maincategory' => 'Main Cateogry', 
    'subCategoryType' => 'Sub Cateogry', 
    'itemList' => 'Item List', 
    'editEquipmentType' => 'Updating Equipment Type',
    'updateEquipmentType' => 'Equipment Type',
    'main_category' => 'Main Category',
    'createMainCategory' => 'New Main Category'
    
];
