@extends('layouts.frontEnd')
@section('content')
    <!-- END SIDEBAR-->


    <div class="row">
        <div class="col-lg-12">
            <div class="card rounded border-0 my-5">
                <div class="card-body">
                    <div class="row dt-table-header">
                        <div class="px-4 d-flex align-items-center">
                            <h4 class="">{{ __('key.proposalList') }}</h4>
                        </div>
                        <div class="col-12 d-flex justify-content-end">
                            <a href="{{ route('main-proposal.create') }}"
                               class="btn btn-sm btn-primary mr-3 rounded ">
                                <i class="bi bi-plus-square"></i>&nbsp;<span>{{__('key.addNew')}}</span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        
                        <table
                            class="table table-bordered table table-head-primary mb-4"
                            id="proposalDataTable" aria-describedby="heading">
                            <thead>

                            <tr>
                                <th>{{ __('key.name') }}</th>
                                <th>{{ __('key.proposalType') }}</th>
                                <th>{{ __('key.action') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- END QUICK SIDEBAR-->
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/dashboard/proposal.js') }}">
    </script>
@endsection
