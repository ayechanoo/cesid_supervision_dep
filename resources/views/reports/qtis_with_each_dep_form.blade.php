@extends('layouts.frontEnd')
@section('content')
<div class="container-fluid mt-5">
    @include('layouts.msg_layouts')
    
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <span>Report for departments</span>
                </div>
                <div class="card-body">
                    <!-- filter start  -->
                    <div class="">

                    <form action="{{route('report.filter.budget_dep.collection')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="mb-3 row">
                                <label for="department_id" class="col-sm-4 col-form-label">Budget Year</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="department_id">
                                
                                        @foreach($budget_years as $k => $year)
                                        <option value="{{$year->id}}">{{$year->from}} - {{$year->to}}</option>
                                        @endforeach
                                    

                                </select>
                                </div>
                            </div>
                            
                            <div class="mb-3 row">
                                <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                                @foreach($departments as $key => $mini)
                                    <optgroup label="{{$key}}">
                                        @foreach($mini as $k => $min)
                                        <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach

                                </select>
                                </div>
                            </div>

                            <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                            <input type="submit" value="Export" class="btn btn-dark-blue"  />

                        </form>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <span>Report for departments with Ministry</span>
                </div>
                <div class="card-body">
                    <!-- filter start  -->
                    <div class="">

                    <form action="{{route('report.deps_by_minid.preview')}}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="mb-3 row">
                                <label for="budget_year_id" class="col-sm-4 col-form-label">Budget Year</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="budget_year_id">
                                
                                        @foreach($budget_years as $k => $year)
                                        <option value="{{$year->id}}">{{$year->from}} - {{$year->to}}</option>
                                        @endforeach
                                    

                                </select>
                                </div>
                            </div>
                            
                            <div class="mb-3 row">
                                <label for="minstry_id" class="col-sm-4 col-form-label">Ministry</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue" name="minstry_id" aria-label="Default select example" id="minstry_id">
                                @foreach($ministries as $key => $min)
                                    
                                        <option value="{{$min->uuid}}">{{$min->name_mm}}</option>
                                        
                                @endforeach

                                </select>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="equipment_type_id" class="col-sm-4 col-form-label">Equipment Type</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue" name="equipment_type_id" aria-label="Default select example" id="equipment_type_id">
                                @foreach($equipments as $key => $equip)
                                    
                                        <option value="{{$equip->uuid}}">{{$equip->name}}</option>
                                        
                                @endforeach

                                </select>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="categories" class="col-sm-4 col-form-label">Category Type</label>
                                <div class="col-sm-8">
                                <select class="form-select add-border-blue js-example-basic-single"  name="categories[]" multiple="multiple" aria-label="Default select example" id="categories">
                                    <option disabled selected>Please choose one</option>

                                </select>
                                </div>
                            </div>

                            <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                            <input type="submit" value="Export" class="btn btn-dark-blue"  />

                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   
</div>
@endsection

@section('script')
<script type="text/javascript">

     $('.js-example-basic-single').select2();
    $('select[name="equipment_type_id"]').change(function() {
       let equip_uuid = $(this).val();
       let formData = {
                 equip_uuid:equip_uuid,
                _token:"<?php echo csrf_token() ?>"
       }

       $('select[id="categories"]').empty();

       $.ajax({
           type:'POST',
           url:"{{route('ajax.categories_by_min_uuid')}}",
           data:formData,
           success:function(data) {
              if(data) {
                    $.each(data, function(i,v) {
                        $('select[id="categories"]').append(`<option value="${v.uuid}">
                                       ${v.name}
                                  </option>`);
                    })
                  }
           },
           error:function(xhr,status,errors) {
            
                let err = JSON.parse(xhr.responseText);
                console.log(typeof err);
                
           }
        });
    })
</script>
@endsection