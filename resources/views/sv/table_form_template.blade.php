@extends('layouts.frontEnd')
@section('content')

        <!-- Navigation-->
        <!-- Masthead-->
            
        
            <div  style="margin-top:6vh" >

                <div class="container-fluid d-flex" style="height:25%;overflow:hidden;">

                    
                    <div style="width:60%; margin:auto 0px;" class="p-3 row">

                        <div class="col-lg-2">
                            <p class="fw-bold">Project</p>
                            <button class="btn btn-outline-dark-blue rounded-0 btn-lg selected-project">Project Type</button>
                            
                        </div>

                        <div class="col-lg-3">
                            <p class="fw-bold">Office</p>
                            <button class="btn btn-outline-dark-blue rounded-0 btn-lg selected-sub-project">Sub-Project Type</button>
                            
                        </div>


                        <div class="col-lg-3">
                            <p class="fw-bold">Non Controllable Device</p>
                            <button class="btn btn-outline-dark-blue rounded-0 btn-lg selected-equipment">Equipment Type</button>
                            <div  class="pt-2" >
                                <button class=" btn btn-outline-dark-blue rounded-0 btn-md equip-no" >1</button>
                                
                                <button class=" btn btn-outline-danger rounded-0  btn-md btn-newEquipment d-none">+</button>
                                <button class=" btn btn-outline-danger rounded-0  btn-md btn-equipment-done">Done</button>
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <p class="fw-bold">NO.</p>
                            <button class="btn btn-outline-dark-blue rounded-0 btn-lg selected-item">Item Creating </button>
                            <div  class="pt-2" >
                                <button class=" btn btn-outline-dark-blue rounded-0 btn-md item-no" >1</button>
                                
                                <button class=" btn btn-outline-danger rounded-0  btn-md btn-newitem d-none">+</button>
                                <button class=" btn btn-outline-danger rounded-0  btn-md btn-item-done">Done</button>
                            </div>
                        </div>

                        


                        



                        
                    
                        <!-- <button class="btn btn-dark-blue btn-newEquipment">New Equipment</button>
                        <button class="btn btn-danger btn-equipment-done">Adding Equipments Done</button>
                        <button class="btn btn-danger btn-item-done d-none">Adding Items Done</button>
                        
                        <button class="btn btn-primary btn-newitem d-none">New Item</button> -->

                    </div>
                    <div class="border-start flex-fill" style="background-color:#1035f721;">
                        <div id="step-item-1" class="d-none">
                            <div class=" p-2 ">
                            Step 1<span class="text-dark fw-bold">:Please Select Word</span>
                                <div class="mt-2" id="">
                                    <table class="border w-100">
                                        <tr>
                                            <th>Mandatory </th>
                                            <th>Value</th>
                                            <th>Status</th>
                                            
                                            
                                        </tr>

                                        <tr>
                                            <th>Category</th>
                                            <th class="chosen-item_category"></th>
                                            <th></th>
                                        
                                            
                                        </tr>
                                        <tr>
                                            <th>SubCategory</th>
                                            <th class="chosen-item_subcategory"></th>
                                            <th></th>
                                        
                                            
                                        </tr>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div id="step-item-2" class="d-none">
                            <div class="d-flex align-items-centern p-2">
                            Step 2:<span class="text-dark fw-bold">Please Check choosing below!</span>
                                    
                            </div>

                            <div class="ps-2">
                                <p class="mb-0">For:</p>
                                <p class=" mb-0">Category : <span class="fw-bold chosen-item_category"> Air con</span></p>
                                <p class=" mb-0">SubCategory : <span class="fw-bold chosen-item_subcategory"> 1.5 HP</span></p>
                            </div>

                            <div>
                                <div class="d-flex p-2 gap-2 align-items-center justify-content-between ">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="item" value="item" id="item" checked data-color="#0A1195">
                                        <label class="form-check-label fw-bold" for="item">
                                            Item 
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input name="item" class="form-check-input" type="radio" value="specification" data-color="#FF6633" id="specification" >
                                        <label class="form-check-label fw-bold" for="specification">
                                            Specification
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input name="item" class="form-check-input" type="radio" value="justification" data-color="#9D1027" id="justification" >
                                        <label class="form-check-label fw-bold" for="justification">
                                            Justification
                                        </label>
                                    </div>

                                    
                                    <div class="form-froup">
                                        <button class="btn btn-danger btn-item-create">Create Item</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        
                        
                                
                       
                    </div>
                </div>

                <div class="col-md-12">
                    
                    <div class="px-4 table-responsive mb-3">
                       

                    </div>

                        <div class="d-none" id="item-division">

                            
                        </div>
                        <div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                @foreach($sheets as $k => $sheet)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link {{$k == 0 ? 'active' : ''}}" id="sheet-{{$k}}" data-bs-toggle="tab" data-bs-target="#sheet-{{$k}}-pane" type="button" role="tab" aria-controls="sheet-{{$k}}-pane" aria-selected="true">{{$sheet}}</button>
                                </li>
                                @endforeach
                               
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                @foreach($sheets as $k => $sheet)
                                    <div class="tab-pane fade show {{$k == 0 ? 'active':''}}" id="sheet-{{$k}}-pane" role="tabpanel" aria-labelledby="sheet-{{$k}}" tabindex="0">

                                        <!-- table start here -->
                                        <div style="overflow-y: auto; height:900px; "> 
                      
                                        <table  >
                                            <tr>
                                                @for ($j = 1; $j <= 15; $j++)
                                                    <td> {{ $j }}</td>
                                                @endfor
                                            </tr>

                                            
                                            
                                            @foreach($collection[$k] as $key => $arr)
                                            
                                            <tr>
                                                @php $last_index = count($arr)-1; @endphp

                                                @for ($i = 0; $i <= 15; $i++)
                                                    @if($i < $last_index)
                                                    <td data-row="{{$key}}" data-col="{{$i}}"> 
                                                        @if(isset($arr[$i]) && !is_null($arr[$i]))
                                                         {{ $arr[$i]}} 
                                                        <input type="checkbox" id="r{{$key}}-c{{$i}}"/> 
                                                        @else
                                                        
                                                        @endif
                                                    
                                                    </td>
                                                    @endif
                                                @endfor
                                            </tr>
                                            @endforeach
                                        </table>
                    
                                        </div>

                                        <!-- table end here -->
                                    </div>
                                @endforeach
                                
                                
                            </div>
                        </div>
                       
                        
                    </div>
                    
                
                </div>
                
                
            </div>


            <!-- modal start here -->

            <!-- Full screen modal -->
            <div class="modal fade" id="preview_modal" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                <div class="modal-dialog modal-fullscreen modal-fullscreen-sm-down">
                    <div class="modal-content">
                    <div class="modal-header">
                        <div class="mx-auto text-center">
                            <p class="fw-bold mb-0" id="exampleModalToggleLabel">Preview Preview Proposed machinerues abd eqyuonebt for estimate in fiscal year({{$budget_year_from}} - {{$budget_year_to}})</p><br/>
                            <p class="fw-bold mb-0" id="exampleModalToggleLabel">Department:{{$department->name_mm}}</p>
                        </div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Main Project  </th>
                                <th scope="col">Sub Project </th>
                                <th scope="col">Equipment Type</th>
                                <th scope="col">Item Category</th>
                                <th scope="col">Item Sub Category</th>
                                <th scope="col">Name</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Unit Type</th>
                                <th scope="col">Price per Unit</th>
                                <th scope="col">Total </th>
                                <th scope="col">Specification </th>
                                <th scope="col">Justification </th>
                                <th scope="col">Action </th>
                            </tr>
                        </thead>
                        <tbody class="data-preview">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                            
                        </tbody>
                    </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-bs-target="#" data-bs-toggle="modal">close</button>
                        <button class="btn btn-primary btn-submit" >Submit and Report</button>
                    </div>
                    </div>
                </div>
            </div>


        
        
 @endsection

 @section('script')

 <script>

   
        $(document).ready(function() {

            var budget_from = @json($budget_year_from);
            var budget_to = @json($budget_year_to);
            var department = @json($department);

        // $('#preview_modal').modal('show');

        $('.btn-equipment-done').addClass('d-none');
        $('.btn-item-done').addClass('d-none');





        var current_action = 0;
        var current_index = 0;
        var current_equip= 0;
        var current_item = 0;
        
        let itemObj = {
            id: 1,
            name : '',
            item_category: '',
            item_subcategory: '',
            qty:'',
            unit:'',
            unit_price:'',
            total_price: ''
        }
        let formal_item  = {
            id:'',
            prj_type : '',
            prj_sub_type: '',
            equipment_list: []
            
        }
        let arr = [];
        let main_prj = {
            current_prj_index:1,
            done:false,
            list:{
                prj_type : {
                    'message':'Please choose Project',
                    'done' : false,
                    'click_count' : 1
                },
                prj_sub : {
                    'message':'Please choose SubProject ',
                    'done' : false,
                    'click_count' : 1
                },
            }   
        }

        let equip_item = {
            id:'',
            name:'',
            items:[]
                
        }

        var mouse_up_ecount = 1;
        var current_mouse_category_index = 1;
        var current_mouse_sub_category_index = 2;



        //setup for radio box color

        let item_color = {
            item : '#0A1195',
            specification:'#FF6633',
            justification : '#9D1027',
        }

        checkStatus();

        $('.btn-submit').click(function() {
           let data = localStorage.getItem('my_list');
           let formData = {
            'list' : data,
            "_token": "{{ csrf_token() }}",
            'budget_from' : budget_from,
            'budget_to' : budget_to,
            'department' : department,
           }
           $.ajax({
                type: "POST",
                url: "{{route('sv.store')}}",
                data: formData,
                dataType: 'json',
                success: function (res) {
                    if(res) {
                        swal('Successfully created')
                        .then((value) => {
                            
                               localStorage.removeItem('my_list');
                               window.location.href ="{{route('supervision.index')}}";
                            
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        })
        

        $('td').mouseup(function() {

            console.log('current action is '+current_action);
            console.log('mouse_up_ecount'+mouse_up_ecount);

            var selected_data = window.getSelection();
            let selected_text = selected_data.toString();
            console.log(selected_text);

            checkStatus();

            if(current_action == 3 && selected_text.length > 0) {
                
                if(mouse_up_ecount == current_mouse_category_index) {

                    
                    if(confirm('Are you sure for item Category?')) {
                        addData('item_category', selected_text);
                        mouse_up_ecount += 1;
                        
                    }

                }else if(mouse_up_ecount == current_mouse_sub_category_index) {

                    
                    if(confirm('Are you sure for Item Sub Category?')) {
                       addData('item_subcategory', selected_text);
                       
                       mouse_up_ecount += 1;
                    }


                }else{

                }
                
                if(mouse_up_ecount == 3){
                    $('input[type="checkbox"]').removeClass('d-none');
                    $('#step-item-1').addClass('d-none')
                    $('#step-item-2').removeClass('d-none')
                }
            }
            
        })


        $('.btn-equipment-done').click(function() {
            current_index += 1;
            current_equip = 0;

            current_action = 0;

            mouse_up_ecount = 1;

            current_item = 0;


            toggleDisable('.btn-newEquipment',1);
            toggleDisable('.btn-equipment-done',1);
            toggleDisable('.btn-newitem',1);
            toggleDisable('.btn-item-done',1);

            toggleShow('.btn-newEquipment',1);
            toggleShow('.btn-equipment-done',1);
            toggleShow('.btn-newitem',1);
            toggleShow('.btn-item-done',1);


            toggleDisable('.btn-operation',0);

            resetforEquipment();
            
        })

        $('.btn-item-done').click(function() {
            current_equip += 1;

            toggleDisable('.btn-newEquipment',0);
            toggleShow('.btn-newEquipment',0);
            toggleDisable('.btn-newitem',1);
            toggleDisable('.btn-item-done',1);
            toggleShow(`.btn-equipment-done`,0);
            toggleDisable(`.btn-equipment-done`,0);
            
            resetforEquipment();

            
        })

        $('.btn-item-create').click(function() {
            callforItemAtOnce();

            toggleDisable('.btn-newitem',0);
            
            
            
        })


        $('.data-preview').on('click','.btn-delete', function() {
            let prj_index = $(this).data('prj');
            let equip_index = $(this).data('equip');
            let item_index = $(this).data('item');

            let obj = localStorage.getItem('my_list');
            let obj_arr = JSON.parse(obj);

            if(obj_arr[prj_index].equipment_list[equip_index].items[item_index]) {
                obj_arr[prj_index].equipment_list[equip_index].items.splice(item_index,1);

                localStorage.setItem('my_list', JSON.stringify(obj_arr));
            }
            showListdata();
        })


        function resetforEquipment() {

           

           
            resetStep();
        }

        

        function addData(key,text) {
            checkStatus();
            let list = localStorage.getItem('my_list');
            let list_obj_arr = [];
            if(!list) {
                alert('please add Main Project');
                
            }else{
                list_obj_arr = JSON.parse(list);
                // console.log('length is '+list_obj_arr.checkCheckListLength)
                if(list_obj_arr.length == 0) {
                        alert('please choose Project Type first');
                }else{
                    
                    let equip = list_obj_arr[current_index].equipment_list[current_equip];

                    if(!equip) {
                        return false;
                    }
                    
                   
                    let item_list = list_obj_arr[current_index].equipment_list[current_equip].items;

                    console.log(item_list);
                    
                    if(item_list.length > 0) {
                        

                        list_obj_arr[current_index].equipment_list[current_equip].items[current_item][key] = text;
                        
                    }else{
                            item = {
                                        id: 1,
                                        name : '',
                                        item_category: '',
                                        item_subcategory: '',
                                        qty:'',
                                        unit:'',
                                        unit_price:'',
                                        total_price: ''
                                    }

                            item[key] = text;

                            item_list.push(item);
                    }

                    
                    

                    localStorage.setItem('my_list',JSON.stringify(list_obj_arr));

                    $(`.chosen-${key}`).text(text);
                    $(`.chosen-${key}`).next('th').html(`<i class="fa-solid fa-check icon"></i>`)
                }

                // showList();
            }
        }


        $('.btn-clear').click(function() {
            swal({
             title: "Are you sure?",
             text: "Once deleted, you will not be able to recover this!",
             icon: "warning",
             buttons: true,
             dangerMode: true,
           })
          .then((willDelete) => {
                if (willDelete) {
                       localStorage.removeItem('my_list');
                       
                       swal('Clear!')
                } else {
                        swal("Your imaginary file is safe!");
            }
            });

        })
        

        

         
        

        $('.btn-operation').click(function() {
            if(current_action == 0) {
                alert('Plese Select Main Project Type by checking')
            }
           
            
           checkCurrentState();

            // toggleDisable('.btn-newEquipment',1);
            // toggleDisable('.btn-equipment-done',1);
            // toggleDisable('.btn-newitem',1);
            // toggleDisable('.btn-item-done',1);

            // toggleshow('.btn-newEquipment',1);
            // toggleshow('.btn-equipment-done',1);
            // toggleshow('.btn-newitem',1);
            // toggleshow('.btn-item-done',1);
        })

        $('.btn-preview').click(function() {
            showListdata();
            $('#preview_modal').modal('show');
        })

        $('.btn-newitem').click(function() {
            
            console.log(current_equip);
            let my_list = localStorage.getItem('my_list'); 
            let my_list_arr = JSON.parse(my_list);
            
            let items = my_list_arr[current_index].equipment_list[current_equip].items;

            if(items.length == 0) {

                item = {
                            id: 1 ,
                            name : '',
                            item_category: '',
                            item_subcategory: '',
                            qty:'',
                            unit:'',
                            unit_price:'',
                            total_price: ''
                        }
                        my_list_arr[current_index].equipment_list[current_equip].items.push(item);
                    
                

            }else{
                let last_index_equip = items.length - 1;
                           
                my_list_arr[current_index].equipment_list[current_equip].items[current_item] =  {
                            id: items[last_index_equip]['id'] + 1 ,
                            name : '',
                            item_category: '',
                            item_subcategory: '',
                            qty:'',
                            unit:'',
                            unit_price:'',
                            total_price: ''
                        }
            }

           

           localStorage.setItem('my_list', JSON.stringify(my_list_arr));

            
           $('input[type="checkbox"]').addClass('d-none');

           
           $('#step-item-1').removeClass('d-none');
           toggleDisable(`.btn-newitem`,1);
           toggleDisable(`.btn-equipment-done`,1);
           

        }) 


        $('.btn-newEquipment').click(function() {
            let new_equip_index = current_equip + 1;
            

            current_action = 2;
            current_item = 0;
            checkCurrentState();

           
           toggleDisable('.btn-newEquipment',1);
           toggleShow('.btn-item-done',0);
           toggleDisable('.btn-item-done',1);
           toggleShow('.btn-newitem',0);
           toggleDisable('.btn-newitem',0);
           toggleDisable('.btn-equipment-done',0);
         

        })

        

        $('input[type="checkbox"]').change(function() {

            
            // checkThereischeck();

            if(current_action == 3) {
                
                if(this.checked) {

                    let color = $('input[type="radio"]:checked').data('color');
                
                   $(this).parent('td').css({'background-color':color,'color':'#fff'})
                   $(this).attr('data-color', color);
                }
                

            //alert('now is is current action');
            }

            
            const myTimeout = setTimeout(startTrigger, 1000);



        })

        function callforItemAtOnce() {

            let specification = ''; let justification = '';
            

            let item_list = $('input[type="checkbox"][data-color="#0A1195"]:checked').map(function (i,e) {
                
                        return $(e).parent('td').text();
                    }).get();

            console.log(item_list);

            let item_specification_arr = $('input[type="checkbox"][data-color="#FF6633"]:checked').map(function (i,e) {
                console.log(e);
                        return $(e).parent('td').text();
                    }).get();
                
                    
                

            let item_justification_arr = $('input[type="checkbox"][data-color="#9D1027"]:checked').map(function (i,e) {
                console.log(e);
                        return $(e).parent('td').text();
                    }).get();

            
                    specification = stringTransform(item_specification_arr);
                    justification = stringTransform(item_justification_arr);

                    console.log(specification,justification);


                    let create_status = creatNewItem(item_list,specification,justification);
                    
                    if(create_status == true) {
                        current_item += 1 ;
                        mouse_up_ecount = 1;
                        $('input[type="checkbox"]:checked').parent('td').css({'background-color':'#ff0000','color':'#fff'});
                        $('input[type="checkbox"]:checked').removeAttr('data-color');
                        $('input[type="checkbox"]:checked').remove();
                        $('input[type="checkbox"]:checked').prop('checked', false);

                        resetStep();
                    }
                    
            
        }

        function resetStep() {
            cate_key = 'item_category'; sub_cate_key = 'item_subcategory';
                    
                
            $(`.chosen-${cate_key}`).text('');
            $(`.chosen-${cate_key}`).next('th').html('')


            $(`.chosen-${sub_cate_key}`).text('');
            $(`.chosen-${sub_cate_key}`).next('th').html('')

            $('#step-item-1').addClass('d-none');
            $('#step-item-2').addClass('d-none');
        }

        function stringTransform(arr) {
            if(arr.length != 0) {
                return arr.toString();
            }
            return '';
        }


        function creatNewItem(item_list,specification,justification) {
            let obj = localStorage.getItem('my_list');
            if(!obj) {
                return false;
            }
            let origin_list = JSON.parse(obj);

            if(item_list.length >= 5) {
               let item_obj = itemByIndex(origin_list, current_index, current_equip, current_item) ;
               console.log(item_obj);
              
              if(item_obj != null) {
                item_obj.name = item_list[0].trim();
                item_obj.qty = item_list[1].trim();
                item_obj.unit = item_list[2].trim();
                item_obj.unit_price = item_list[3].trim();
                item_obj.total_price = item_list[4].trim();
                item_obj.specification = specification;
                item_obj.justification = justification;

                localStorage.setItem('my_list', JSON.stringify(origin_list));
              }
            }


            if(item_list.length == 4) {

                qty_string = item_list[1].trim();   

               let regx = /\d/;
               if(regx.test(qty_string) == true) {
                     
                var unit_string = qty_string.replace(/^[0-9]/g, "");
                unit_string = unit_string.trim();
                console.log('unit_stirng is'+ unit_string);
                var qty_unit = qty_string.replace(/[^0-9.]/g, "");
                qty_unit = parseInt(qty_unit);
                unit_price = item_list[2].trim();
                total_price = item_list[3].trim();
               }else{
                qty_unit = item_list[1].trim();
                unit_string = item_list[2].trim();
                unit_price = item_list[3].trim();
                total_price = item_list[3].trim();
               }
                   


               let item_obj = itemByIndex(origin_list, current_index, current_equip, current_item) ;
               
              if(item_obj != null) {
                item_obj.name = item_list[0].trim();
                item_obj.qty = qty_unit;
                item_obj.unit = unit_string;
                item_obj.unit_price = unit_price;
                item_obj.total_price = total_price;
                item_obj.specification = specification;
                item_obj.justification = justification;


                localStorage.setItem('my_list', JSON.stringify(origin_list));
              }
            }

            

            if(item_list.length == 2) {
               let item_obj = itemByIndex(origin_list, current_index, current_equip, current_item) ;
               console.log('wel'+item_obj);
              if(item_obj != null) {
                item_obj.name = item_list[0].trim();
                item_obj.qty = '';
                item_obj.unit = '';
                item_obj.unit_price = '';
                item_obj.total_price = item_list[1].trim();
                item_obj.specification = specification;
                item_obj.justification = justification;

                localStorage.setItem('my_list', JSON.stringify(origin_list));
              }
            }

                return true;
        }

        function itemByIndex(my_list_arr, prj,equip, item) {
            // let my_list_obj = localStorage.getItem('my_list');

            if(my_list_arr.length == 0) {
                return null;
            }

            if(!my_list_arr[prj]) {
                return null;
            }

            if(my_list_arr[prj].equipment_list.length == 0) {
                return null;
            }

            if(!my_list_arr[prj].equipment_list[equip]) {
                return null;
            }

            if(my_list_arr[prj].equipment_list[equip].items.length == 0) {
                return null;
            }

            if(!my_list_arr[prj].equipment_list[equip].items[item]) {
                return null;
            }

            if(!my_list_arr[prj].equipment_list[equip].items[item]) {
                return null;
            }

            return my_list_arr[prj].equipment_list[equip].items[item];

        }   

        

        function startTrigger() {
            if(current_action == 0) {
                
                stepOne();
            }

            if(current_action == 1) {
                stepTwo();
            }

            if(current_action == 2) {
                stepThree();
            }


            if(current_action == 3) {

                // let value = $('input[type="radio"]:checked').val();
                // console.log(value);

               //alert('now is is current action');
            }
        }

    
        // $('input[type="checkbox"]').change(function(){
            
        //     if(confirm('are you sure?')) {
        //         current_action +=1; 
        //         //checkCurrentState();

        //     }else{
        //         $('input[type="checkbox"]').prop('checked', false); 
        //     }
        // })

        
        

        

        function checkCurrentState() {
            console.log('now action is'+current_action);
           

            if(current_action == 1) {

                let ans = confirm('Please select Sub project type By Checking');
                if(ans) {

                }else{
                    console.log('current_action is '+ 1);
                    current_action +=1;
                    const myTimeout = setTimeout(startTrigger, 1000);
                }
            }

            if(current_action == 2) {
                alert('Please select Equipment type By Checking ');
                $('input[type="checkbox"]').removeClass('d-none');
            }


            if(current_action == 3) {
                alert('Now You can start add a item or more than one! if you are done, then you must set Done!')
            }

            toggleColor();
        }


        function checkMouse_up_ecount() {
            console.log('now select current is '.mouse_up_ecount);

            if(current_action == 1) {
                alert('please select for Item Main Category')
            }

            if(current_action == 2) {
                alert('please select for Item sub Category')
            }

            if(current_action == 3) {
                alert('please select for Item Name Category')
            }


            
        }

        showListdata();

        

        // showList();

        function stepOne() {
            toggleDisable('.btn-operation',1);
            let checked_len = checkCheckListLength();
            
            if(checked_len == main_prj.list.prj_type.click_count) {
                if(confirm('are you sure?')) {
                    
                    current_action +=1; 
                    

                    let val = $('input[type="checkbox"]:checked').parent('td').text();
                    
                    let list = localStorage.getItem('my_list');
                    let list_obj_arr = [];
                    if(!list) {
                        console.log('no list');
                        formal_item.id = current_index;
                        formal_item.prj_type = val;
                        list_obj_arr.push(formal_item);
                    }else{
                        
                        list_obj_arr = JSON.parse(list);
                        // console.log('length is '+list_obj_arr.checkCheckListLength)
                        if(list_obj_arr.length <= 0) {
                            formal_item.id = current_index;
                            formal_item.prj_type = val;
                            list_obj_arr.push(formal_item);
                        }else{
                            let last_index = list_obj_arr.length - 1;

                            let exist_prj = list_obj_arr.filter(item => item.prj_type == val);
                            
                            console.log(exist_prj.length);
                            if(exist_prj.length == 0) {
                                formal_item.id = current_index + 1;
                                formal_item.prj_type = val;
                                list_obj_arr.push(formal_item);
                            }
                            
                        }
                    }

                   

                    localStorage.setItem('my_list',JSON.stringify(list_obj_arr));
                    
                    checkCurrentState();
                    toggleColor();
                    
                }
                $('input[type="checkbox"]:checked').parent('td').css({'background-color':'#333','color':'#fff'});
                // $('input[type="checkbox"]:checked').remove();
                $('input[type="checkbox"]:checked').prop('checked', false);

                
            } 

             
        }



        function stepTwo() {
           
            let checked_len = checkCheckListLength();
            let val = $('input[type="checkbox"]:checked').parent('td').text();
            if(checked_len == main_prj.list.prj_sub.click_count) {
                if(confirm('Are you sure?')) {
                    current_action +=1; 
                    let list = localStorage.getItem('my_list');
                    let list_obj_arr = {};
                    if(!list) {
                        alert('please choose Project Type first');
                    }else{
                        list_obj_arr = JSON.parse(list);
                        let list_item = list_obj_arr[current_index];
                        list_item.prj_sub_type = val;
                    }

                    localStorage.setItem('my_list',JSON.stringify(list_obj_arr));
                    checkCurrentState();

                    $('input[type="checkbox"]:checked').parent('td').css({'background-color':'#3498db','color':'#fff'});
                    $('input[type="checkbox"]:checked').remove();
                    $('input[type="checkbox"]:checked').prop('checked', false);
                    alert('please click + for new Equipment');

                    toggleDisable('.btn-newEquipment',0);
                    

                    toggleShow('.btn-newEquipment',0);
                    toggleShow('.btn-equipment-done',0);
                    toggleDisable('.btn-equipment-done',0);


                    toggleShow('.btn-newitem',1);
                   
                    
                }
               
                   
              
            } 

            
            
        }

        function stepThree() {
            let equip_item = {};
            let val = $('input[type="checkbox"]:checked').parent('td').text();
            let checked_len = checkCheckListLength();
            if(checked_len == 1) {
                if(confirm('are you sure?')) {
                    
                    let list = localStorage.getItem('my_list');
                    let list_obj_arr = {};
                    if(!list) {
                        alert('please choose Project Type first');
                    }else{
                        list_obj_arr = JSON.parse(list);
                        let list_item = list_obj_arr[current_index];
                        
                        if(list_item.equipment_list.length > 0) {
                           
                            let last_index_equip = list_item.equipment_list.length - 1;

                            let exist_equip = list_item.equipment_list.filter(item => item.name == val);

                            
                            if(exist_equip.length == 0) {
                                equip_item = {
                                                    id:list_item.equipment_list[last_index_equip].id,
                                                    name:val,
                                                    done_status:false,
                                                    items:[]
                                                        
                                                }
                            
                            }
                        }else{
                                equip_item = {
                                                    id:1,
                                                    name:val,
                                                    done_status:false,
                                                    items:[]
                                                        
                                                }
                        }

                        

                        list_item.equipment_list.push(equip_item);

                       localStorage.setItem('my_list',JSON.stringify(list_obj_arr));

                       $('input[type="checkbox"]:checked').parent('td').css({'background-color':'#3498db','color':'#fff'});
                        $('input[type="checkbox"]:checked').remove();
                        $('input[type="checkbox"]:checked').prop('checked', false);

                        current_action +=1;

                        

                        toggleDisable('.btn-newEquipment',1);
                        toggleShow('.btn-item-done',0);
                        toggleDisable('.btn-item-done',0);
                        toggleShow('.btn-newitem',0);
                        toggleDisable('.btn-newitem',0);
                        toggleDisable('.btn-equipment-done',1);
         

                    }

                    showList();
                   // checkCurrentState();
                   
                }
               
                
                
            } 

            
            
        }

        

        function stepFour() {
            
            let checked_len = checkCheckListLength();
            if(checked_len == main_prj.list.prj_type.click_count) {
                if(confirm('are you sure?')) {
                    current_action +=1; 
                    checkCurrentState();
                   
                }
                $('input[type="checkbox"]:checked').parent('td').css({'background-color':'#34889e','color':'#fff'});
                $('input[type="checkbox"]:checked').remove();
                $('input[type="checkbox"]:checked').prop('checked', false);
                
                
            } 

           
            
        }

        function checkCheckListLength() {
           let data_len =  $('input[type="checkbox"]:checked').length;
            console.log('now check:input length is '+ data_len);
            return data_len;
        }


        function showListdata() {
            let list = localStorage.getItem('my_list');
            if(list) {
                let obj_arr = JSON.parse(list);
                let html = '';
                $.each(obj_arr, function(i,v) {
                    $.each(v.equipment_list, function(j,k) {
                        $.each(k.items, function(l,h) {
                            html+='<tr>'
                            html+=`<td> ${l + 1}</td>`
                            html+=`<td> ${v.prj_type}</td>`
                            html+=`<td> ${v.prj_sub_type}</td>`
                            html+=`<td> ${k.name}</td>`
                            html+=`<td> ${h.item_category}</td>`
                            html+=`<td> ${h.item_subcategory}</td>`
                            html+=`<td> ${h.name}</td>`
                            html+=`<td> ${h.qty}</td>`
                            html+=`<td> ${h.unit}</td>`
                            html+=`<td> ${h.unit_price}</td>`  
                            html+=`<td> ${h.total_price}</td>`  
                            html+=`<td> ${h.specification}</td>`  
                            html+=`<td> ${h.justification}</td>`  
                            html+=`<td> <button class="btn btn-danger btn-delete" data-prj ="${i}" data-equip="${j}" data-item="${l}">Remove</button></td>`  
                            html+='</tr>'
                            
                        })
                    })
                })

                $('.data-preview').html(html);
            }else{
                $('.data-preview').html('');
            }
        }

            
        
            $('input[name="item"]').change(function(e) {
                    
                checkRadioStyle();
               
            });
            checkRadioStyle();

            function checkRadioStyle() {

                $('input[name="item"]').each(function(e,v){
                    let {value, name} =v;
                    if ($(`input[value="${value}"]`).is(':checked')) { 
                        $(`input[value="${value}"]`).next('label').css('color', item_color[value])
                    }else{
                        $(`input[value="${value}"]`).next('label').css('color', '#000')
                    }
                })

            }

            function changeCheckboxColor() {

            }
        

        function checkStatus() {
            console.log('now current_action is  => '+current_action);
                    console.log('now current_index is  => '+current_index);
                    console.log('now current_equip is  => '+current_equip);
                    console.log('now current_item is  => '+current_item);


                    console.log('now mouse up event count is  => '+mouse_up_ecount);
        }

        showCurrentInformation();

        function toggleColor() {
            if(current_action == 0) {
                $('.selected-project').addClass('bg-bold-blue');
            }else{
                $('.selected-project').removeClass('bg-bold-blue')
            }

            if(current_action == 1) {
                $('.selected-sub-project').addClass('bg-bold-blue');
            }else{
                $('.selected-sub-project').removeClass('bg-bold-blue')
            }


            if(current_action == 2) {
                $('.selected-equipment').addClass('bg-bold-blue');
            }else{
                $('.selected-equipment').removeClass('bg-bold-blue')
            }


            if(current_action == 3) {
                $('.selected-item').addClass('bg-bold-blue');
            }else{
                $('.selected-item').removeClass('bg-bold-blue')
            }

            showCurrentInformation();
            
        }

        function showCurrentInformation() {
            let arr_obj = [];
            let obj = localStorage.getItem('my_list');
            if(obj) {

                arr_obj = JSON.parse(obj);
        
                 setInformation(arr, current_index, current_equip, current_item) ;

            }
           
        }

        function setInformation(my_list_arr, prj,equip, item) {
            // let my_list_obj = localStorage.getItem('my_list');
            console.log(my_list_arr.length);

            if(my_list_arr.length == 0) {
                return null;
            }else{
                if(my_list_arr[prj]) {
                    setInfo('.selected-project', my_list_arr[prj]?.prj_type);
                    setInfo('.selected-sub-project', my_list_arr[prj]?.prj_sub_type);

                    if(my_list_arr[prj].equipment_list.length > 0  &&  my_list_arr[prj].equipment_list[equip]) {
                    
                            setInfo('.selected-equipment',  my_list_arr[prj]?.equipment_list[equip]?.name);


                            $('equip-no').text(equip + 1)
                            $('equip-no').text(item + 1)
                    }
                }

            }

           

           

        }   


        function setInfo(selector , text) {
            $(`${selector}`).prev('p').text(text);
        }

        function showList() {
            let list = localStorage.getItem('my_list');
            let arr = JSON.parse(list);
            let html = '';
            $.each(arr, function(i,v){
                html +=`<div class="item " >
                                <p>Project Type: <span class="prj_type">${v.prj_type}</span></p>
                                <p>Project Sub Type: <span class="prj_sub_type">${v?.prj_sub_type}</span></p>
                            `
                if(v.equipment_list.length > 0) {
                    $.each(v.equipment_list, function(eq_k,eq_v) {
                        html +=  `<p>equipment ${eq_k + 1}: <span class="prj_sub_type">${eq_v?.name}</span><button class="btn btn-primary ${eq_v.done_status == true ? 'btn-equip-done': '' }">${eq_v.done_status == true ? 'Set as Done': 'Done' }</button></p>`

                        if(eq_v.items.length > 0) {
                            $.each(eq_v.items, function(item_k,item_v) {
                                html +=  `<p>Item ${item_k + 1}: <span class="prj_sub_type">Item_Category:${item_v?.item_category}</span></p>`
                            })
                        }else{
                            html+="no item at all"
                        }
                    })
                }else{
                    html+="no equipment at all"
                }
                
                html+=`</div>
                            <hr/>`
            })

            $('.menu').html(html);
        }

        function toggleDisable(selector, status) {

            if(status == 1) {
                $(`${selector}`).prop('disabled',true);
                $(`${selector}`).addClass('btn-secondary');
            }else{
                $(`${selector}`).prop('disabled',false);
                $(`${selector}`).removeClass('btn-secondary');
            }
           
        }

        function toggleShow(selector, status) {
            console.log(selector);
            
            if(status == 1) {
                
                $(`${selector}`).addClass('d-none');
            }else{
                
                $(`${selector}`).removeClass('d-none');
            }
        }


        


    })
 </script>
 @endsection


 PaKhaNa-000013

