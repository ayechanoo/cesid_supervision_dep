
@extends('layouts.frontEnd')
@section('content')
        <!-- Navigation-->
        <!-- Masthead-->
        <section class="masthead bg-white text-white text-center">
            <div class="container d-flex align-items-center flex-column">
                <!-- Masthead Avatar Image-->
                
                <!-- Masthead Heading-->
                <h5 class="text-uppercase mb-0 text-dark mb-0 fw-bold">Creating New Budget Estimate</h5>
                <span class="text-secondary mb-0 text-dark mb-5">Please Fill all informations to demand for a budget.</span>
                
                
                <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 ">
                    <div class="text-end">
                        <span class="text-dark d-block">Sa-1001</span>
                        <span class="text-dark d-block">Kyats in Thousand (2 Decimal)</span>
                    </div>
                    <div class="card mx-auto">
                       
                            <div class="card-body" style="background-color:#EEEEEE;color:#000000;">
                                <form action="{{route('excel.import')}}" method="post" enctype="multipart/form-data">
                                    @csrf

                                    <div class="mb-3 row">
                                        <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                                        <div class="col-sm-8">
                                        <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                                        @foreach($departments as $key => $mini)
                                            <optgroup label="{{$key}}">
                                                @foreach($mini as $k => $min)
                                                <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach

                                        </select>
                                        </div>
                                    </div>
                                    
                                    <div class="mb-3 row" id="exist-budget">
                                        <label for="budget_year" class="col-sm-4 col-form-label">Budget Year</label>
                                        <div class="col-sm-8 d-flex gap-1" >
                                        <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="budget_year">
                                        @foreach($budget_years as $year)
                                            
                                                <option value="{{$year->id}}">{{Carbon\Carbon::parse($year->from)->format('F Y')}}-{{Carbon\Carbon::parse($year->to)->format('F Y')}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" data-type ='budget' onClick="javascrip:void(0)">New</button>
                                        </div>
                                        
                                    </div>
                                    <div id ="new-budget" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="budget_from" class="col-sm-4 col-form-label">Budget From</label>
                                            <div class="col-sm-8">
                                            <input type="date" placeholder="....."  name="budget_from" class="form-control add-border-blue" id="budget_from" value="">
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <label for="budget_to" class="col-sm-4 col-form-label">Buget To</label>
                                            <div class="col-sm-8">
                                            <input type="date" name="budget_to" class="form-control add-border-blue" id="budget_to">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" id="exist-proposal-type">
                                        <label for="proposal_type" class="col-sm-4 col-form-label">Proposal Types</label>
                                        <div class="col-sm-8 d-flex gap-1">
                                        <select class="form-select add-border-blue"  name="proposal_type" aria-label="Default select example" id="proposal_type">
                                        @foreach($proposal_types as $type)
                                            
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" onClick="javascrip:void(0)" data-type="proposal-type">New</button>
                                        </div>
                                    </div>
                                    <div id ="new-proposal-type" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="new_proposal_form" class="col-sm-4 col-form-label">New Proposal Type</label>
                                            <div class="col-sm-8">
                                            <input type="text" placeholder="....."  name="new_proposal_type_form" class="form-control add-border-blue" id="new_proposal_form" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" id="exist-proposal">
                                        <label for="proposal" class="col-sm-4 col-form-label">Sub Proposal</label>
                                        <div class="col-sm-8 d-flex gap-1">
                                        <select class="form-select add-border-blue" name="proposal" aria-label="Default select example" id="proposal">
                                        @foreach($proposals as $item)
                                            
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" onClick="javascrip:void(0)" data-type="proposal">New</button>
                                        </div>
                                    </div>
                                    <div id ="new-proposal" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="new_proposal_form" class="col-sm-4 col-form-label">New Proposal </label>
                                            <div class="col-sm-8">
                                            <input type="text" placeholder="....." name="new_proposal_form" class="form-control add-border-blue" id="new_proposal_form" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" id="exist-item-categ">
                                        <label for="item_categ" class="col-sm-4 col-form-label">Item Category</label>
                                        <div class="col-sm-8 d-flex gap-1 ">
                                        <select class="form-select add-border-blue" name="item_categ" aria-label="Default select example" id="item_categ">
                                        @foreach($item_categs as $cat)
                                            
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" onClick="javascrip:void(0)" data-type="item-categ">New</button>
                                        
                                        </div>
                                    </div>
                                    <div id ="new-item-categ" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="new_proposal_form" class="col-sm-4 col-form-label">New Proposal </label>
                                            <div class="col-sm-8">
                                            <input type="text" placeholder="....."  name="new_proposal_form" class="form-control add-border-blue" id="new_proposal_form" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" id="exist-item-sub-categ">
                                        <label for="item_sub_categ" class="col-sm-4 col-form-label">Item Sub Category(optional)</label>
                                        <div class="col-sm-8 d-flex gap-1">
                                        <select class="form-select add-border-blue" name="item_sub_categ" aria-label="Default select example" id="item_sub_categ">
                                        @foreach($item_sub_categs as $cat)
                                            
                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" onClick="javascrip:void(0)" data-type="item-sub-categ">New</button>
                                        </div>
                                    </div>
                                    <div id ="new-item-sub-categ" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="item-sub-categ" class="col-sm-4 col-form-label">New Item Sub Category </label>
                                            <div class="col-sm-8">
                                            <input type="text" placeholder="....." name="item-sub-categ" class="form-control add-border-blue" id="item-sub-categ" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" id="exist-item-info">
                                        <label for="proposal" class="col-sm-4 col-form-label">Item Name</label>
                                        <div class="col-sm-8 d-flex gap-1">
                                        <select class="form-select add-border-blue" name="proposal" aria-label="Default select example" id="proposal">
                                        @foreach($item_infos as $row)
                                            
                                            <option value="{{$row->id}}">{{$row->name}}</option>
                                                
                                        @endforeach

                                        </select>
                                        <button class="btn btn-dark-blue new-btn" onClick="javascrip:void(0)" data-type="item-info">New</button>
                                        </div>
                                    </div>
                                    <div id ="new-item-info" class="d-none">
                                        <div class="mb-3 row">
                                            <label for="item-info" class="col-sm-4 col-form-label">New Item Name</label>
                                            <div class="col-sm-8">
                                            <input type="text" placeholder="....." name="item-info" class="form-control add-border-blue" id="item-info" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div class="mb-3 row">
                                            <label for="item-info" class="col-sm-4 col-form-label">Specification</label>
                                            <div class="col-sm-8">
                                            <textarea name="specification"  rows="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div id ="new-quantity" class="">
                                        <div class="mb-3 row">
                                            <label for="quantity" class="col-sm-4 col-form-label">Quantity</label>
                                            <div class="col-sm-8">
                                            <input type="number" min=1 placeholder="23"  name="quantity" class="form-control add-border-blue" id="quantity" value="">
                                            </div>
                                        </div>
                                    </div>
                                

                                    <div id ="new-unit" class="">
                                        <div class="mb-3 row">
                                            <label for="unit" class="col-sm-4 col-form-label">unit</label>
                                            <div class="col-sm-8">
                                            <select class="form-select add-border-blue" name="proposal" aria-label="Default select example" id="proposal">
                                                
                                            @foreach(config('helper.units') as $k=>$r)
                                                
                                                <option value="{{$r}}">{{$k}}</option>
                                                    
                                            @endforeach

                                            </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div id ="new-unit-price" class="">
                                        <div class="mb-3 row">
                                            <label for="unit-price" class="col-sm-4 col-form-label">Unit Price</label>
                                            <div class="col-sm-8">
                                            <input type="text" min=1 placeholder="10000" name="unit-price" class="form-control add-border-blue" id="unit-price" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3 row d-none">
                                        <label for="export_file" class="col-sm-4 col-form-label">Import Excel</label>
                                        <div class="col-sm-8">
                                        <input type="file" name="excel_file" class="form-control add-border-blue" id="export_file">
                                        </div>
                                    </div>
                                    <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                                    <input type="submit" value="Submit" class="btn btn-dark-blue"  />

                                </from>
                            </div>
                    </div>
                </div>
                
            </div>
        </section>
        
 @endsection
