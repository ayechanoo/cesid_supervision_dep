

@extends('layouts.frontEnd')
@section('content')

<div class=" mt-5 bg-white text-white ">

    <div class=" px-4 d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image-->
        
        <!-- Masthead Heading-->
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 " >
             <a class="btn btn-primary float-end mb-2   " href="{{route('download.excel')}} ">Excel Export</a>
            <div class="table-responsive " style="width:1500px;">
                <table class="table table-bordered" >
                    <thead>
                        <tr>
                            <td rowspan="3">Department</td>
                            @foreach($sub_categories as $label => $cat)
                            @php $count = count($cat) * 2; @endphp

                                
                            
                            <td colspan="{{$count}}">{{$label}}</td>
                            
                            

                            @endforeach
                            
                        </tr>

                        <tr>
                            
                            @foreach($sub_categories as $label => $cat)
                            @foreach($cat as $v)

                                @if($label == $v->Item_Category)
                                <td colspan="{{2}}"></td>
                                @else
                                <td colspan="{{2}}">{{$v->Item_Category}}</td>
                                @endif
                            
                            
                            @endforeach

                            @endforeach
                            



                        </tr>

                        <tr>
                            
                            @foreach($sub_categories as $cat)
                            @foreach($cat as $v)
                            <td>qty</td>
                            <td>amount</td>
                            @endforeach

                            @endforeach
                            



                        </tr>  
                    </thead>
                    <tbody>
                        
                    @foreach($collection as $key => $value)
                        <tr>
                            <td>{{$value->name_mm}}</td>
                            @foreach($sub_categories as $k=>$cat)
                                @foreach($cat as $de => $v)
                                
                                    @php 
                                    $qty_field_name = $v->Item_Category."_qty";
                                    $total_field_name = $v->Item_Category."_amount";
                                    @endphp
                                    <td>@php echo $value->{$qty_field_name} @endphp</td>
                                    <td>@php echo $value->{$total_field_name} @endphp</td>
                                    

                                @endforeach
                            @endforeach

                            
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
    
</script>

@endsection