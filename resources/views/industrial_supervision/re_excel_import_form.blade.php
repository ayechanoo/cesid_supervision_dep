@extends('layouts.frontEnd')
@section('content')
    <div class="mt-5">
        @include('layouts.msg_layouts')
        <h4>Please Chose for Sa-1001 format</h4>
        <div class="card">
            <div class="card-body" style="background-color:#EEEEEE;color:#000000;">
                <form action="{{route('manual.re.excel-create-store')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="mb-3 row">
                        <label for="department_id" class="col-sm-4 col-form-label">Budget Year</label>
                        <div class="col-sm-8">
                        <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="department_id">
                        
                                @foreach($budget_years as $k => $year)
                                <option value="{{$year->uuid}}">{{$year->from}} - {{$year->to}}</option>
                                @endforeach
                            

                        </select>
                        </div>
                    </div>

                    <div class="mb-3 row">
                        <label for="minstry_id" class="col-sm-4 col-form-label">Ministry</label>
                        <div class="col-sm-8">
                        <select class="form-select add-border-blue" name="ministry_id" aria-label="Default select example" id="minstry_id">
                        @foreach($ministries as $key => $min)
                            
                                <option value="{{$min->uuid}}">{{$min->name_mm}}</option>
                               
                        @endforeach

                        </select>
                        </div>
                        
                    </div>

                

                    <div class="mb-3 row">
                        <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                        <div class="col-sm-6">
                        <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                        <option>Please Select One</option>

                        </select>
                        </div>

                        <div class="col-sm-2">
                            <button onclick="return false;" class="btn btn-primary btn_be_find">Find History</button>
                        </div> 
                    </div>

                    <div class="mb-3 row">
                        <label for="in_transaction_id" class="col-sm-4 col-form-label">Result Record:</label>
                        <div class="col-sm-8">
                        <span class='be_record'></span>
                        </div>

                    
                    </div>
                    <div id="re_excel_import_div" class="d-none">
                        <div class="mb-5 row " id="re_excel_import_div">
                            <label for="export_file" class="col-sm-4 col-form-label">Recommend Reviewed By</label>
                            <div class="col-sm-8 ">
                                <div class="col-6 row">
                                    <div class="form-check col">
                                      <input class="form-check-input" value="1" type="radio" name="reviewed_by" id="flexRadioDefault2" checked>
                                      <label class="form-check-label" for="flexRadioDefault2">
                                        TC ဌာန
                                      </label>
                                    </div>

                                    <div class="form-check col">
                                      <input class="form-check-input" value="2" type="radio" name="reviewed_by" id="flexRadioDefault3" checked>
                                      <label class="form-check-label" for="flexRadioDefault3">
                                            ဒုဝန်ကြီးဌာန
                                      </label>
                                    </div>

                                    <div class="form-check col">
                                      <input class="form-check-input" value="3" type="radio" name="reviewed_by" id="flexRadioDefault4" checked>
                                      <label class="form-check-label" for="flexRadioDefault4">
                                        ဝန်ကြီးဌာန
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mb-5 row" >
                            <label for="export_file" class="col-sm-4 col-form-label">Recommend Excel Import</label>
                            <div class="col-sm-8">
                            <input type="file" name="excel_file" class="form-control add-border-blue" id="export_file">
                            @error('upload_file')
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                            </div>
                        </div>
                    
                    </div>

                    <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                    <input type="submit" value="Export" class="btn btn-dark-blue"  />

                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('.btn_be_find').click(function() {
            let budget_id = $('select[name="budget_year"] option:selected').val();
            let dep_id = $('select[name="department_id"] option:selected').val();

            let formData = {
                budget_id:budget_id,
                dep_id:dep_id,
                _token:"<?php echo csrf_token() ?>"
            }
            
             $.ajax({
               type:'POST',
               url:"{{route('ajax.intrans_by_budget_dep')}}",
               data:formData,
               success:function(data) {
                  if(data.data) {
                    $('.be_record').html('Record is founded!')
                    if($('.be_record').hasClass('text-danger')) {
                        $('.be_record').removeClass('text-danger');
                    }
                    $('#re_excel_import_div').removeClass('d-none');
                    $('.be_record').addClass('text-success');
                  }
               },
               error:function(xhr,status,errors) {
                
                    let err = JSON.parse(xhr.responseText);
                    console.log(typeof err);
                    $('.be_record').html(err.error_msg);
                    if($('.be_record').hasClass('text-success')) {
                        $('.be_record').removeClass('text-success');
                    }
                    $('.be_record').addClass('text-danger');
               }
            });
            
        })
        $('select[name="ministry_id"]').change(function() {
            let value = $(this).val();

            $('select[name="department_id"]').empty();
            let formData = {
                mini_id:value,
                _token:"<?php echo csrf_token() ?>"
            }
            
             $.ajax({
               type:'POST',
               url:"{{route('ajax.deps_by_minid')}}",
               data:formData,
               success:function(data) {
                  if(data) {
                    $.each(data, function(i,v) {
                        $('select[name="department_id"]').append(`<option value="${v.uuid}">
                                       ${v.name_mm}
                                  </option>`);
                    })
                  }
               },
               error:function(xhr,status,err) {
                console.log(xhr);
               }
            });
        })


    })
</script>
@endsection

