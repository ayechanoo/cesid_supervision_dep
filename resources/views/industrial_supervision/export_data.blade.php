

@extends('layouts.frontEnd')
@section('content')
<div class="masthead bg-white text-white text-center">
    <div class=" px-4 d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image-->
        
        <!-- Masthead Heading-->
        <div class="col-sm-12">
            <h5 class="text-uppercase mb-0 text-dark mb-5">Preview Data</h5>

            <a href="{{route('download.sa-one.excel',['budgetYear_id'=>$budget_year_id,'department_id' => $department_id])}}" class="btn btn-outline-dark-blue float-end">Download as Excel</a>
        </div>
       
        
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="">
            
                
            </div>
            <div class="">
                    <div class="card-body" style="background-color:#EEEEEE;color:#000000;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="9" align="center" style="font-size:15px; height: 50px" class="text-uppercase" >Proposed machinerues abd eqyuonebt for estimate in fiscal year({{$budget->from}} - {{$budget->to}})</th>
                                    <th>Sa-2001</th>
                                </tr>

                                <tr>
                                    <!-- <th colspan="9" rowspan="2" align="right" style="font-size:15px; height: 50px" class="text-uppercase" >MINISTRY OF AGRICULTURE ၊LIVESTOCK &  IRRIGATION                                                                                                                    </th>
                                    <th colspan="2" rolspan="2" class="text-right">  DEPARTMENT: MINISTER'S  OFFICE</th> -->
                                    
                                        <th colspan="2" rowspan="2" align="right" style="font-size:15px; height: 50px" class="text-uppercase" >Ministry:{{htmlspecialchars_decode($ministry_name)}}  </th>
                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th colspan="2" rolspan="2" class="text-right">  DEPARTMENT: {{$dep_name}}</th>
                                    
                                    
                                </tr>

                                <tr>
                                    
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    
                                    <th colspan="2" class="text-right">Kyats in thousand(2 Decimal)</th>
                                    
                                </tr>
                                
                                <tr>
                                    <th rowspan="2">Sr No</th>
                                    <th rowspan="2">Description of Equipment (TECHNICAL DETAILS)</th>
                                    <th colspan="2">Demand</th>
                                    <th colspan="2">Recommand</th>
                                    <th colspan="3">ReMarks</th>
                                    <th colspan="1" rowspan=2 >Justification</th>
                                    
                                </tr>
                                <tr>
                                    <th>Qty</th>
                                    <th>Total Amount</th>
                                    <th>Qty</th>
                                    <th>Total Amount</th>
                                    <th>Free</th>
                                    <th>Loan</th>
                                    <th>Grant</th>
                                </tr>
                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                </tr>
                            </thead>
                            <tbody>
        
                                @php $proj_index = 1; @endphp
                                @foreach($collection as $proposal_type => $rows)
                                
                                <tr>
                                    <td> {{ $proj_index++ }} </td>
                                    <td class="text-start">{{ucwords($proposal_type)}}</td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                </tr>
                                
                                    @foreach($rows as $proposal => $items)
                                    
                                    <tr>
                                        <td></td>
                                        <td class="text-start">{{ ucwords($proposal) }}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                        @php $equpi_index = 1; @endphp
                                        

                                        @foreach($items as $equpi => $demands)

                                            <tr>
                                                <td></td>
                                                <td class="text-start">{{ $equpi_index++ }}.  {{ ucwords($equpi) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                                @php $total_amount = 0; @endphp
                                                    
                                                    @foreach($demands as $key => $value)
                                                        
                                                        @php 
                                                        $unit_price = (int)$value['unit_price'];
                                                        $qty = (int)$value['qty'];
                                                        $qty_total_amount = ($qty * $unit_price);
                                                        $total_amount += $qty_total_amount;
                                                        $show_qty_total = number_format(($qty_total_amount / 1000), 2);
                                                        
                                                        @endphp
                                                        
                                                        <tr>
                                                            <td></td>
                                                            <td class="text-start">{{ $key+1 }}. {{ ucwords($value['name']) ?? '' }}({{ucwords($value['item_sub_name']) ?? ''}})
                                                            </td>
                                                            <td>{{ $value['qty'] ?? '' }} {{ ucwords($value['unit']) }}</td>
                                                            <td>{{ $show_qty_total }}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>{{$value['justification'] ??  '-'}}</td>
                                                            
                                                        </tr>
                                                        @if($value['specification'] != $value['item_sub_name'])
                                                        <tr>
                                                            <td></td>
                                                            <td class="text-start">
                                                                Specification:{{$value['specification']}}
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                        </tr>
                                                        @endif
                                                    @endforeach
                                                    @php 
                                                    $show_total_amount = number_format((($total_amount/1000)  ), 2);
                                                    @endphp

                                                        <tr>
                                                            <td></td>
                                                            <td class="text-end">Total Amount</td>
                                                            <td></td>
                                                            <td>{{ $show_total_amount }}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            
                                                        </tr>
                                        @endforeach
                                    
                                   


                                        
                                    @endforeach

                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
        
    </div>
</div>
@endsection