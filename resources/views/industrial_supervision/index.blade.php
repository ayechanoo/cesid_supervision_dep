@extends('layouts.frontEnd')
@section('content')
<header class="masthead  text-white text-start">

    <div class="col-8 mx-auto ">
        <div class="p-3">
            <h3 class="text-dark">Excel Upload</h3>
        </div>
        @include('layouts.msg_layouts')
        <div class="card" style="color:#303030">
            
            <div class="card-body">
                <form action="{{route('sv.upload')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-5     row">
                        <label for="budget_from" class="col-sm-4 col-form-label">Budget From</label>
                        <div class="col-sm-8">
                        <input type="date"  name="budget_from" class="form-control add-border-blue" id="budget_from" value="">
                        @error('budget_from')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="mb-5 row">
                        <label for="budget_to" class="col-sm-4 col-form-label">Buget To</label>
                        <div class="col-sm-8">
                        <input type="date" name="budget_to" class="form-control add-border-blue" id="budget_to">
                        @error('budget_to')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="mb-5     row">
                        <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                        <div class="col-sm-8">
                        @error('department_id')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                        @foreach($departments as $key => $mini)
                            <optgroup label="{{$key}}">
                                @foreach($mini as $k => $min)
                                <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach

                        </select>
                        </div>
                    </div>

                    <div class="mb-5     row">
                        <label for="export_file" class="col-sm-4 col-form-label">Import Excel</label>
                        <div class="col-sm-8">
                        <input type="file" name="excel_file" class="form-control add-border-blue" id="export_file">
                        @error('upload_file')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                        </div>
                    </div>
                    <div class="d-flex gap-4">
                        <input type="reset" value="Cancel" class="btn btn-danger form-control "  />
                        <input type="submit" value="Submit" class="btn btn-primary   form-control"  />
                    </div>

                </form>
            </div>
    </div>
        <!-- Masthead Avatar Image-->
        
        <!-- Masthead Heading-->
        

        <!-- <a href="{{route('demand-excel.export')}}" class="btn btn-outline-dark-blue">Export Data</a> -->
        <!-- <div class="d-lg-flex gap-3">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="">
                <span class="text-dark text-uppercase font-bold d-block">Uploading Sa-1001 Excel Form </span>
                    <span class="text-dark d-block">Please fill all informations and  use Excel Format (Sa-1001)</span>
                    
                </div>
                
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 d-none    ">
                <div class="text-center">
                <span class="text-dark text-uppercase font-bold d-block">Exporting Data </span>
                    <span class="text-dark d-block">Please fill all informations </span>
                    
                </div>
                <div class="card">
                        <div class="card-body" style="background-color:#EEEEEE;color:#000000;">
                            <form action="{{route('demand-excel.export')}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="mb-3 row">
                                    <label for="department_id" class="col-sm-4 col-form-label">Budget Year</label>
                                    <div class="col-sm-8">
                                    <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="department_id">
                                    
                                            @foreach($budget_years as $k => $year)
                                            <option value="{{$year->id}}">{{$year->from}} - {{$year->to}}</option>
                                            @endforeach
                                        

                                    </select>
                                    </div>
                                </div>
                                
                                <div class="mb-3 row d-none">
                                    <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                                    <div class="col-sm-8">
                                    <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                                    @foreach($departments as $key => $mini)
                                        <optgroup label="{{$key}}">
                                            @foreach($mini as $k => $min)
                                            <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach

                                    </select>
                                    </div>
                                </div>

                                <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                                <input type="submit" value="View" class="btn btn-dark-blue"  />

                            </form>
                        </div>
                </div>
            </div>
        </div> -->
    </div>
</header>
@endsection