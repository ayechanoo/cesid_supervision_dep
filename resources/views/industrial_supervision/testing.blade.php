<table>
    <thead>
        <tr>
            <td colspan={{ $vehicleNameCount + 2 }} align="center"
                style="font-size:15px;height: 100px;vertical-align: center;font-weight:bold;background-color:#DCDCDC;">
                စီမံကိန်း၊ ဘဏ္ဍာရေးနှင့် စက်မှုဝန်ကြီးဌာန <br>ဗဟိုစက်ပစ္စည်းစာရင်းအင်းနှင့် စစ်ဆေးရေးဦးစီးဌာန၊
                ပစ္စည်းထိန်းဌာနခွဲ

            <td>
        </tr>

        <tr>
            <td colspan={{ $vehicleNameCount + 2 }} align="center"
                style="font-size:15px;height: 100px;vertical-align: center;font-weight:bold;">
                {{ $start_date}} မှ
                {{ $end_date}}ထိ<br>ထုတ်ပေးခဲ့သည့် မော်တော်ယာဉ်စာရင်း(ထုတ်ပေးမှုအခြေအနေအလိုက်
                စာရင်းချုပ်)
            <td>
        </tr>

        <tr>
            <td align="center" style="width: 300px;height:80px;vertical-align: center;font-weight:bold;">အကြောင်းအရာ</td>
            @foreach ($vehicleNames as $vehicleName)
                <td align="center" style="width: 300px;height:80px;vertical-align: center;font-weight:bold; rotate:-90;">
                    {{ $vehicleName->name }}</td>
            @endforeach

            <td align="center" style="width: 100px;height:80px;vertical-align: center;font-weight:bold;">Grand Total</td>
        </tr>
        @php

            $colTotal = 0;

        @endphp
        @foreach ($issueTypes as $issueType)
            @php
                $totalCnt = 0;
            @endphp
            <tr>
                <td align="center" style="width: 200px;height:50px;vertical-align: center;font-weight:bold;">
                    {{ $issueType->name }}
                </td>
                @foreach ($vehicleNames as $vehicleName)
                    @php
                        $vehicleCount = \Illuminate\Support\Facades\DB::select(
                            \Illuminate\Support\Facades\DB::raw("select count(*) as count from vehicles join vehicle_issue_infos,issue_types,vehicle_names where vehicle_issue_infos.vehicle_id = vehicles.id and issue_types.id = vehicle_issue_infos.issue_type_id
and  vehicles.vehicle_name_id = vehicle_names.id and vehicle_names.id = {$vehicleName->id} and issue_types.id = {$issueType->id}  and vehicle_issue_infos.date between '{$start_date}' and '{$end_date}' and vehicle_issue_infos.deleted_at is null"),
                        );
                        if (!empty($vehicleCount)) {
                            $totalCnt += $vehicleCount[0]->count;
                        }
                    @endphp
                    <td align="center" style="width: 200px;height:50px;vertical-align: center;font-weight:bold;">
                        @if (!empty($vehicleCount))
                            {{ $vehicleCount[0]->count == 0 ? '' : $vehicleCount[0]->count }}
                        @endif
                    </td>
                @endforeach
                <td align="center" style="width: 200px;height:50px;vertical-align: center;font-weight:bold;">
                    {{ $totalCnt }}
                </td>
            </tr>
        @endforeach
        <tr>
            @php
                $allTotal = 0;
            @endphp
            <td align="center" style="width: 300px;height:80px;vertical-align: center;font-weight:bold;">Grand Total
            </td>
            @foreach ($vehicleNames as $vehicleName)
                <td align="center" style="width: 300px;height:80px;vertical-align: center;font-weight:bold;">
                    @php
                        $vehicleCountByName = \Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw("select count(*) as count from vehicles join vehicle_issue_infos,vehicle_names where vehicle_issue_infos.vehicle_id = vehicles.id and  vehicles.vehicle_name_id = vehicle_names.id and vehicle_names.id = {$vehicleName->id}  and vehicle_issue_infos.date between '{$start_date}' and '{$end_date}' and vehicle_issue_infos.deleted_at is null"));

                        $allTotal += $vehicleCountByName[0]->count;
                    @endphp
                    @if (!empty($vehicleCountByName))
                        {{ $vehicleCountByName[0]->count == 0 ? '' : $vehicleCountByName[0]->count }}
                    @endif
                </td>
            @endforeach
            <td align="center" style="width: 300px;height:80px;vertical-align: center;font-weight:bold;">
                {{ $allTotal }}
            </td>
        </tr>



    </thead>
    <tbody>


    </tbody>
</table>