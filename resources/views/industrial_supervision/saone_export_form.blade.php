@extends('layouts.frontEnd')
@section('content')
    <div class="mt-5">
        <h4>Please Chose for Sa-1001 format</h4>
        <div class="card">
            <div class="card-body" style="background-color:#EEEEEE;color:#000000;">
                <form action="{{route('demand-excel.export')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="mb-3 row">
                        <label for="department_id" class="col-sm-4 col-form-label">Budget Year</label>
                        <div class="col-sm-8">
                        <select class="form-select add-border-blue" name="budget_year" aria-label="Default select example" id="department_id">
                        
                                @foreach($budget_years as $k => $year)
                                <option value="{{$year->id}}">{{$year->from}} - {{$year->to}}</option>
                                @endforeach
                            

                        </select>
                        </div>
                    </div>
                    
                    <div class="mb-3 row">
                        <label for="department_id" class="col-sm-4 col-form-label">Department</label>
                        <div class="col-sm-8">
                        <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                        @foreach($departments as $key => $mini)
                            <optgroup label="{{$key}}">
                                @foreach($mini as $k => $min)
                                <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                @endforeach
                            </optgroup>
                        @endforeach

                        </select>
                        </div>
                    </div>

                    <input type="reset" value="Cancel" class="btn btn-outline-dark-blue"  />
                    <input type="submit" value="Export" class="btn btn-dark-blue"  />

                </form>
            </div>
        </div>
    </div>
@endsection