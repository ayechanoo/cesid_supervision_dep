@extends('layouts.frontEnd')
@section('content')

        <div class="row mt-5" style="padding-top:10vh;">
            <div class="col-lg-8 mx-auto" >
                <div class="card rounded border-0 my-5">
                    
                    
                    
                    <div class="card-body">
                        <div class="row dt-table-header">
                            <div class="px-4 d-flex align-items-center justify-content-between">
                                <h4 class="">{{ __('Industrial Supervision Department') }}</h4>
                                <a href="{{route('supervision.index')}}" class="btn btn-outline-dark-blue">Back to Home</a>
                                
                                <form action="{{route('demand-excel.store')}}" method="post" >
                                    @csrf
                                    <input type="hidden" name="data" value="{{json_encode($result)}}"/>
                                    <input type="hidden" name="budget_from" value="{{ $budget_year_from }}"/>
                                    <input type="hidden" name="budget_to" value="{{ $budget_year_to }}"/>
                                    <input type="hidden" name="department_id" value="{{ $department->id }}"/>
                                    
                                    <button type="submit" class="btn btn-outline-dark-blue">Import and Ceate</button>
                                </form>
                            </div>
                        
                            <div class="col-12 d-flex justify-content-center my-5">
                                <h5>{{$dep_name}} for Budget Estimate in ({{$budget_from}} - {{$budget_to}})  </h5>
                            </div>
                        </div>
                        <div>
                            Please Fix these information
                            @foreach($result['errors'] as $key => $item)
                            
                            <ul>
                                <li>Row No:<span class="text-danger">{{$key}}</span></li>
                                @foreach($item as $col=>$error)
                                    <ul>
                                        <li>[col-{{$col}}] : <span class="text-{{$error['status']== 403 ? 'danger' : 'secondary'}}">{{$error['message']}}</span></li>
                                       
                                    </ul>
                                @endforeach
                            </ul>
                            @endforeach
                        </div>
                        <div>
                            <h5>Avaliable Result are :</h5>
                            <div class="table-responsive">
                                
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Proposal</th>
                                            <th scope="col">Sub Proposal</th>
                                            <th scope="col">Equipment Type</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">SubCategory</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Specification</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Unit</th>
                                            <th scope="col">Unit Price</th>
                                            <th scope="col">Total Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        @foreach($result['valid_data'] as $key => $value)
                                        <tr>
                                            <td>{{$value[2]['type'] == 'true' ? ($value[2]['name']) : '-'}}</td>
                                            <td>{{($value[3]['type'] == 'true') ? $value[3]['name'] : '-'}}</td>
                                            <td>{{$value[4]['type'] == 'true' ? $value[4]['name'] : '-'}}</td>
                                            <td>{{$value[5]['type'] == 'true' ? $value[5]['name'] : '-'}}</td>
                                            <td>{{$value[6]['type'] == 'true' ? $value[6]['name'] : '-'}}</td>
                                            <td>{{$value[7]['type'] == 'true' ? $value[7]['name'] : '-'}}</td>
                                            <td>{{$value[8]['type'] == 'true' ? $value[8]['value'] : '-'}}</td>
                                            <td>{{$value[9]['type'] == 'true' ? $value[9]['value'] : '-'}}</td>
                                            <td>{{$value[10]['type'] == 'true' ? $value[10]['value'] : '-'}}</td>
                                            <td>{{$value[11]['type'] == 'true' ? $value[11]['value'] : '-'}}</td>
                                            <td>{{$value[12]['type'] == 'true' ? $value[12]['value'] : '-'}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
