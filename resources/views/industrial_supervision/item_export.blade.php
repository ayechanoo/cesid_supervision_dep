

@extends('layouts.frontEnd')
@section('content')

<div class="masthead bg-white text-white text-center">
    <div class=" px-4 d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image-->
        
        <!-- Masthead Heading-->
        <div class="col-sm-12">
            <h5 class="text-uppercase mb-0 text-dark mb-5">Demand Report for {{$budget_year->from}} - {{$budget_year->to}} </h5>

            <a href="{{route('download.excel',['budgetYear_id'=>$budget_year->id,'department_id' => $department_id])}}" class="btn btn-outline-dark-blue float-end d-none">Download as Excel</a>
        </div>
       
        
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="text-dark">
                <table >
                    <tr>
                        <td rowspan="2">Department</td>
                        @foreach($sub_categories as $de => $v)
                            
                        <td colspan="2">{{$v->name}}</td>

                        @endforeach
                    </tr>
                    
                    <tr>
                        
                         @foreach($sub_categories as $de => $v)
                            
                            <td>qty</td>
                            <td>amount</td>
    
                         @endforeach
                            
                        
                        
                        
                    </tr>  
                    
                    @foreach($collection as $key => $value)
                        <tr>
                            <td>{{$value->name_mm}}</td>
                            @foreach($sub_categories as $de => $v)
                                @php 
                                $qty_field_name = $v->name."_qty";
                                $total_field_name = $v->name."_amount";
                                @endphp
                                <td>@php echo $value->{$qty_field_name} @endphp</td>
                                <td>@php echo $value->{$total_field_name} @endphp</td>
                                
        
                            @endforeach

                            
                            
                        </tr>
                    @endforeach
                        
                   
                    
                   
                </table>
                
            </div>
            
        </div>
        
    </div>
</div>
@endsection