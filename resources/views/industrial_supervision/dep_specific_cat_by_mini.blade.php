

@extends('layouts.frontEnd')
@section('content')

<div class=" mt-5 bg-white text-white ">

    <div class=" px-4 d-flex align-items-center flex-column">
        <!-- Masthead Avatar Image-->
        
        <!-- Masthead Heading-->
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 " >
             <a class="btn btn-primary float-end mb-2   " href="{{route('download.excel')}} ">Excel Export</a>
            <div class="table-responsive " style="width:1500px;">
                <table class="table table-bordered" >
                    <thead>

                        <tr>
                            <td rowspan="3">Department</td>
                            @foreach($cat_list as $key => $cat)

                                @php $count = (count($cat->subcategories) == 0) ? 1 * 4 : count($cat->subcategories)*4; @endphp
                            
                                

                                    
                                        <td colspan="{{$count}}">{{$cat->name}}</td>
                                        


                                



                            @endforeach
                            



                        </tr> 

                        

                        <tr>
                            
                            @foreach($cat_list as $key => $cat)


                            
                                @if(count($cat->subcategories) > 0)

                                    @foreach($cat->subcategories as $k => $sub)

                                        
                                        <td colspan="{{4}}">{{$sub->name}}</td>
                                        

                                    @endforeach

                                @else

                                    
                                        <td colspan="{{4}}">{{$cat->name}}</td>
                                        


                                @endif



                            @endforeach
                            



                        </tr> 



                        <tr>
                            
                            @foreach($cat_list as $key => $cat)


                            
                                @if(count($cat->subcategories) > 0)

                                    @foreach($cat->subcategories as $k => $sub)

                                        
                                        <td>BE</td>
                                        <td>တန်ဘိုး</td>
                                        <td>RE</td>
                                        <td>တန်ဘိုး</td>

                                    @endforeach

                                @else

                                    
                                        <td>BE</td>
                                        <td>တန်ဘိုး</td>
                                        <td>RE</td>
                                        <td>တန်ဘိုး</td>


                                @endif



                            @endforeach
                            



                        </tr>  

                        
                    </thead>
                    <tbody>

                        @foreach($collection as $col => $final_val)
                        <tr>
                            <td>{{ $final_val->department_name }}</td>

                            @foreach($cat_list as $key => $cat)


                                    @if(count($cat->subcategories) > 0)

                                        @foreach($cat->subcategories as $k => $sub)

                                            @php 
                                            $reqty = "re_isc_".$sub->id."_qty";
                                            $reamt = "re_isc_".$sub->id."_total";
                                            $beqty = "be_isc_".$sub->id."_qty";
                                            $beamt = "be_isc_".$sub->id."_total";


                                            
                                            @endphp
                                            <td><?php echo $final_val->{$beqty} == 0 ? '' : $final_val->{$beqty}; ?></td>
                                            <td><?php echo $final_val->{$beamt} == 0 ? '' : number_format(((int)$final_val->{$beamt})/1000, 2,'.'); ?></td>
                                            <td><?php echo $final_val->{$reqty} == 0 ? '': $final_val->{$reqty}; ?></td>
                                            <td><?php echo $final_val->{$reamt} == 0 ? '' : number_format(((int)$final_val->{$reamt})/1000, 2, '.'); ?></td>
                                            

                                        @endforeach

                                    @else

                                        @php 
                                            $icreqty = "re_ic_".$cat->id."_qty";
                                            $icreamt = "re_ic_".$cat->id."_total";
                                            $icbeqty = "be_ic_".$cat->id."_qty";
                                            $icbeamt = "be_ic_".$cat->id."_total";
                                            
                                        @endphp
                                            <td><?php echo $final_val->{$icbeqty} == 0 ? '' : $final_val->{$icbeqty}; ?></td>
                                            <td><?php echo $final_val->{$icbeamt} == 0 ? '' : number_format(((int)$final_val->{$icbeamt})/1000, 2,'.'); ?></td>
                                            <td><?php echo $final_val->{$icreqty} == 0 ? '': $final_val->{$icreqty}; ?></td>
                                            <td><?php echo $final_val->{$icreamt} == 0 ? '' : number_format(((int)$final_val->{$icreamt})/1000, 2, '.'); ?></td>


                                    @endif



                            @endforeach

                        </tr>

                        @endforeach

                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>

$(document).ready( function () {
    $('#myTable').DataTable();
} );
    
</script>

@endsection