

@extends('layouts.frontEnd')
@section('content')

<div class=" bg-white text-white text-center" style="margin-top:10vh; margin-bottom:20px;">
    <div class=" px-4 d-flex align-items-center flex-column">
        
        <main class="mx-5" style="">
            
            <div class="row g-5"  style="color:#303030">
                <div class="col-md-5 col-lg-4 order-md-start">
                    <div class="card" style="text-align:left;">
                        <div class="card-body">
                            <h5 class="card-title mb-4">General Information</h5>
                            <div class="col-12" id="budget_div">
                                
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>  
                                    
                                
                            </div>
                            
                            <div class="col-12" id="budget_from_div">
                                <label for="qty" class="form-label">Budget Start <span class="text-danger">*</span></label>
                                <input type="date"  name="budget[from]" class="form-control add-border-blue" id="budget_from" value="">
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>  
                                    
                                
                            </div>

                            <div class="col-12 mt-2" id="budget_to_div">
                                <label for="qty" class="form-label">Budget End <span class="text-danger">*</span></label>
                                <input type="date"  name="budget[to]" class="form-control add-border-blue" id="budget_from" value="">
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>
                            </div>
                            <div class="col-12 mt-2" id="department_div">
                                <label for="qty" class="form-label">Department<span class="text-danger">*</span></label>
                                <select class="form-select add-border-blue" name="department_id" aria-label="Default select example" id="department_id">
                                    @foreach($departments as $key => $mini)
                                        <optgroup label="{{$key}}">
                                            @foreach($mini as $k => $min)
                                            <option value="{{$min->department_id}}">{{$min->department_name}}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach

                                </select>
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="text-align:left; margin-top :5vh;">
                        <div class="card-body">
                            <h5 class="card-title mb-4">Project Information </h5>
                            <div class="col-12" id="prj_div">
                                <div class="d-flex">
                                    <label for="qty" class="form-label  align-self-center">Main Project Title </label>
                                    <span class="text-danger float-end"><button class="btn btn-primary rounded-0 btn-new btn-sm m-2" data-selector="prj[new]">+</button></span>
                                </div>
                                <select class="form-select" id="country" name="prj[old]" required>
                                    <option value="">Choose...</option>
                                    @foreach($projects as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach                                    
                                </select>
                                <input type="text" class="form-control" name="prj[new]"  id="qty" placeholder="eg: Works">
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>
                            </div>
                            <div class="col-12 mt-2" id="sub_prj_div">
                                <div class="d-flex">
                                    <label for="qty" class="form-label  align-self-center">Sub Project Title </label>
                                    <span class="text-danger float-end"><button class="btn btn-primary rounded-0 btn-new btn-sm m-2" data-selector="sub_prj[new]">+</button></span>
                                </div>
                                <select class="form-select" id="country" name="sub_prj[old]" required>
                                    <option value="">Choose...</option>
                                    
                                </select>
                                <input type="text" class="form-control" id="qty" name="sub_prj[new]" placeholder="eg: Work Extensions">
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-8" id="demand-info-div" style="color:#303030;text-align:left;">
                    <h4 class="mb-3">Demand Information</h4>
                    <form class="needs-validation" onclick="return false;" novalidate>
                        <div class="row g-3">
                            <div class="col-8" id="item_name_div">
                                <label for="item_name" class="form-label">Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="item[name]" id="item_name" placeholder="example:nego printer">
                                <div class="invalid-feedback">
                                    Please enter a valid item_name address for shipping updates.
                                </div>
                            </div>
                            <div class="col-4" id="item_unit_price_div">
                                <label for="unit_price" class="form-label">Unit Per Price <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="item[unit_price]" id="unit_price" placeholder="2">
                                <div class="invalid-feedback">
                                    Please enter a valid unit_price address for shipping updates.
                                </div>
                            </div>
                            <div class="col-4" id="item_qty_div">
                                <label for="qty" class="form-label">Qty <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="item[qty]" id="qty" placeholder="2">
                                <div class="invalid-feedback">
                                    Please enter a valid qty address for shipping updates.
                                </div>
                            </div>
                            <div class="col-4" id="item_unit_string_div">
                                <label for="country" class="form-label">In Unit</label>
                                <select class="form-select" id="country" name="item[unit_string]" required>
                                    <option value="">Choose...</option>
                                    @foreach($units as $key => $value)
                                    <option value="{{ $key }}"> {{ $key }} </option>
                                    @endforeach
                                    
                                </select>
                                <div class="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            <div class="col-4" id="item_total_div">
                                <label for="total_amount" class="form-label">Total Amount <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="item[total]" id="total_amount" placeholder="2">
                                <div class="invalid-feedback">
                                    Please enter a valid total_amount address for shipping updates.
                                </div>
                            </div>
                            <div class="col-md-4" id="equip_div">
                                <div class="d-flex">
                                    <label for="equip" class="form-label  align-self-center">Equipment Type</label>
                                    <span class="text-danger float-end"><button class="btn btn-primary rounded-0 btn-new btn-sm m-2" data-selector="equip[new]">+</button></span>
                                </div>
                                <select class="form-select" id="country" name="equip[old]" required>
                                    <option value="">Choose...</option>
                                    @foreach($equipments as $key => $value)
                                        <option value="{{$value->id}}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                                <input type="text" class="form-control" name="equip[new]" id="total_amount" placeholder="2">
                                <div class="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            <div class="col-md-4" id="cate_div">
                                <div class="d-flex">
                                    <label for="equip" class="form-label  align-self-center">Main Category</label>
                                    <span class="text-danger float-end"><button class="btn btn-primary rounded-0 btn-new btn-sm m-2" data-selector="cate[new]">+</button></span>
                                </div>
                                <select class="form-select" id="country" name="cate[old]" required>
                                    <option value="">Choose...</option>
                                    @foreach($item_categories as $key => $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                    
                                </select>
                                <input type="text" class="form-control" name="cate[new]" id="total_amount" placeholder="2">
                                <div class="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            <div class="col-md-4" id="sub_cat_div">
                                <div class="d-flex">
                                    <label for="equip" class="form-label  align-self-center">Sub Category</label>
                                    <span class="text-danger float-end"><button class="btn btn-primary rounded-0 btn-new btn-sm m-2" data-selector="sub_cate[new]">+</button></span>
                                </div>  
                                <select class="form-select" id="country" name="sub_cate[old]" required>
                                    <option value="">Choose...</option>
                                    <option>sub category</option>
                                </select>
                                <input type="text" class="form-control" name="sub_cate[new]" id="total_amount" placeholder="2">
                                <div class="invalid-feedback">
                                    Please select a valid country.
                                </div>
                            </div>
                            
                            <div class="col-12" id="specification_div">
                                <label for="floatingTextarea2">Specification</label>
                                <textarea class="form-control" name="item[specification]" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
  
                                <div class="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>

                            <div class="col-12" id="justification_div">
                                <label for="floatingTextarea2">Justification</label>
                                <textarea class="form-control" name="item[justification]" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
  
                                <div class="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>

                        </div>
                        <hr class="my-4">
                        
                        <button class="w-100 btn btn-primary btn-lg btn-submit" type="submit">Continue to checkout</button>
                    </form>
                </div>
            </div>
        </main>

        
    </div>
</div>
@endsection

@section('script')
<script>
    var dic_prjs = @json($projects);
    var dic_sub_prjs = @json($sub_projects);
    var categories = @json($item_categories);
    var sub_categories = @json($item_sub_categories);


    var prjs = $.map(dic_prjs, function (obj) {
        obj.text =  obj.name; // replace pk with your identifier

        return obj;
    });

    

    var sub_prjs = $.map(dic_sub_prjs, function (obj) {
        obj.text =  obj.name; // replace pk with your identifier

        return obj;
    });

    console.log(sub_prjs);

    
    
    $(document).ready(function() {
        basicSetup();

        $('select[name="prj[old]"]').change(function() {
            let prj_id = $(this).val();
            let sub_prjs = dic_sub_prjs.filter(item => item.proposal_type_id == prj_id);
            let options = [];
            

            $('select[name="sub_prj[old]"]').empty();
            $('select[name="sub_prj[old]"]').append(new Option('Skip Step', '', true, true));
            $.each(sub_prjs, function(key, obj) {
                let newOption = new Option(obj.name, obj.id, true, true);
                $('select[name="sub_prj[old]"]').append(newOption);
            });

           
        })

        $('select[name="cate[old]"]').change(function() {
            let cate_id = $(this).val();
            console.log(sub_categories);
            let sub_cates = sub_categories.filter(item => item.item_category_id == cate_id);
            let options = [];
            

            $('select[name="sub_cate[old]"]').empty();
            $('select[name="sub_cate[old]"]').append(new Option('Skip Step', '', true, true));
            $.each(sub_cates, function(key, obj) {
                let newOption = new Option(obj.name, obj.id, true, true);
                $('select[name="sub_cate[old]"]').append(newOption);
            });

           
        })


        $('.btn-submit').click(function() {
            console.log('now you clicked!');
            checkRequired();
        })

        
       

        $('.btn-new').click(function() {
            
            let selector = $(this).data('selector');
            if($(`input[name="${selector}"]`).hasClass('d-none')) {

                $(`input[name="${selector}"]`).removeClass('d-none');
                 $(`input[name="${selector}"]`).prev().addClass('d-none') ;
                 
                 $(this).text('-');
                 $(this).addClass('btn-danger');
                 $(this).removeClass('btn-primary');

            }else{

                $(`input[name="${selector}"]`).addClass('d-none');
                $(`input[name="${selector}"]`).val('');
                 $(`input[name="${selector}"]`).prev().removeClass('d-none')    ;
                 $(this).text('+');
                 

                 $(this).removeClass('btn-danger');
                 $(this).addClass('btn-primary');
            }
            
        })
    
    })  

    function basicSetup(){
        muteNewInputBox();
        refillGeneralInfo();
    }
    function checkRequired() {
        //let selected = $('select[name="sub_prj[old]"] option:selected').val();

        var formData = new FormData();

        $('input[type="text"],input[type="number"], input[type="date"]').not('.d-none').each(function(i,v) {
            let {name, value}  = v;
            console.log(value);
            if(name != '' &&  value != '') {
                formData.append(name,value);
                let info = {};

                if(name=="budget[from]" || name =="budget[to]"  ) {
                    let gen = localStorage.getItem('my_gen_info');
                    if(!gen){
                        
                        info[name] = value;
                    }else{
                        info = JSON.parse(gen);
                        info[name] = value;

                    }

                    localStorage.setItem('my_gen_info', JSON.stringify(info));

                    
                }
            }
        })

        $('select').not('.d-none').each(function(i,v) {
            let {name}  = v;
            if(name != '') {
                let selected = $(`select[name="${name}"] option:selected`).val();
                if(selected != '') {
                    formData.append(name,selected);

                    if(name=="department_id") {

                    let gen = localStorage.getItem('my_gen_info');
                    if(!gen){
                        info[name] = selected;
                    }else{
                        info = JSON.parse(gen);
                        info[name] = selected;

                    }

                    localStorage.setItem('my_gen_info', JSON.stringify(info));

                    
                }
                }
                
            }
        })
        

        

        $('textarea').not('.d-none').each(function(i,v) {
            let {name,value}  = v;
            if(name != '') {
                
                formData.append(name,value);
            }
        })

       

        $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

        $.ajax({
                type: "POST",
                url: "{{route('ajax-demand-store')}}",
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if(res.status == true) {
                        swal(res.msg)
                        .then((value) => {
                            
                                $('#demand-info-div input[type="text"],#demand-info-div input[type="date"], #demand-info-div input[type="number"]').val('');
                                 $('#demand-info-div select').not('.d-none').each(function(i,v) {
                                    let {name}  = v;
                                    if(name != '') {
                                     $(`#demand-info-div select[name="${name}"]`).val('');
                                       
                                    }
                                })

                                 $('textarea').val('');
                                 location.reload();
                            
                        });
                    }
                },
                error: function(xhr, status, error) {
                    let err_state = xhr.status;
                    if(err_status = 422) {
                        let err_arr = JSON.parse(xhr.responseText);
                        let err = {};
                        $.each(err_arr.errors, function(i,v) {
                            
                            Object.keys(v[0])
                            .forEach(function eachKey(key) { 
                                console.log(key);
                                $(`#${key}`).children('.invalid-feedback').css('display','block');
                                $(`#${key}`).children('.invalid-feedback').text(v[0][key]);
                                
                            });

                        })
                        
                        
                       
                        
                    }else{
                        swal('something went wrong! please contact us!')
                    }
                    // console.log(xhr.status);
                    // console.log(xhr.responseText);
                }
            });

        
    }
    function muteNewInputBox() {
        $('input').each(function(i,v) {
            let {name,value} = v;
            if(name.includes("[new]")){
                $(`input[name="${name}"]`).addClass('d-none');
            }
        })
    }

    function refillGeneralInfo() {
           let gen = localStorage.getItem('my_gen_info');
           if(gen) {
            let arr = JSON.parse(gen);
            Object.keys(arr)
                            .forEach(function eachKey(key) { 
                               
                               if(key == 'department_id') {
                                $(`select[name="${key}"]`).val(arr[key]);
                               }else{
                                $(`input[name="${key}"]`).val(arr[key]);
                               }
                            });
           }
        }
</script>
@endsection