@extends('layouts.frontEnd')
@section('content')

    <!-- START PAGE CONTENT-->
    
    <div class="page-heading mt-5">
        <h1 class="page-title">{{ __('key.createProposalType') }}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html"><i class="la la-home font-20"></i></a>
            </li>
        </ol>
    </div>
    <div class="page-content fade-in-up">
        <div class="ibox ">
            <div class="ibox-head">
                <div class="ibox-title">{{ __('key.newProposalType') }}</div>
                <div class="ibox-tools">
                    <a class="ibox-collapse"><i class="ti-angle-down"></i></a>
                </div>
            </div>
            <div class="ibox-body card">
                <form class="form-horizontal" action="{{route('proposal-type.store')}}" method="POST">
                    @csrf
                    <div class="p-4">
                        <div class="form-group mb-4 row">
                            <label for="name" class="col-2">{{__('key.name')}}</label>
                            <input class="col-8 form-control" type="text" name="name" placeholder="Name" id="name">
                            @error('name')
                            <div class="col-4"> <span class="text-danger ">{{ $message }}</span></div>
                            @enderror
                        </div>

                        <div class="form-group mb-4 float-right">
                            <button class="btn btn-success" type="submit" >{{ __('key.save') }}</button>
                            <a class="btn btn-danger"  href="{{ route('proposal-type.index') }}">{{ __('key.cancel') }}</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div>
@endsection

