<table style="border-collapse:collapse;table-layout:fixed;">
    <thead>
        <tr>
            <th colspan="9" align="center">{{strtoupper('Proposed machinerues abd eqyuonebt for estimate in fiscal year')}} ({{$budget_from}}-{{$budget_to}})</th>
            <th>Sa-1001</th>
        </tr>

        <tr>
            <!-- <th colspan="9" rowspan="2" align="right" style="font-size:15px; height: 50px" class="text-uppercase" >MINISTRY OF AGRICULTURE ၊LIVESTOCK &  IRRIGATION                                                                                                                    </th>
            <th colspan="2" rolspan="2" class="text-right">  DEPARTMENT: MINISTER'S  OFFICE</th> -->
            
                <th colspan="2" rowspan="2" align="left" style="word-wrap: break-word; text-align:left;" >MINISTRY: {{htmlspecialchars_decode($ministry_name)}} </th>
                
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="1" rolspan="2" class="text-right">  DEPARTMENT: {{$dep_name}}</th>
            
            
        </tr>

        <tr>
            
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            
            
            <th colspan="1" class="text-right">Kyats in thousand(2 Decimal)</th>
            
        </tr>

        <tr>
            <th rowspan="2">Sr No</th>
            <th rowspan="2" style="word-wrap: break-word; text-align:left;">Description of Equipment (TECHNICAL DETAILS)</th>
            <th colspan="2">Demand</th>
            <th colspan="2">Recommand</th>
            <th colspan="3">ReMarks</th>
            <th  colspan="1" rowspan=2 >Justification</th>
            
        </tr>
        <tr>
            <th>Qty</th>
            <th>Total Amount</th>
            <th>Qty</th>
            <th>Total Amount</th>
            <th>Free</th>
            <th>Loan</th>
            <th>Grant</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
        </tr>
    </thead>
    <tbody>
        
        @php $proj_index = 1; @endphp
        @foreach($collection as $proposal_type => $rows)
        
        <tr>
            <td> {{ $proj_index++ }} </td>
            <td class="text-start">{{ucwords($proposal_type)}}</td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
            <td ></td>
        </tr>
        
            @foreach($rows as $proposal => $items)
            
            <tr>
                <td></td>
                <td class="text-start">{{ ucwords($proposal) }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
                @php $equpi_index = 1; @endphp

                @foreach($items as $equpi => $demands)

                    <tr>
                        <td></td>
                        <td class="text-start">{{ $equpi_index++ }}.  {{ ucwords($equpi) }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                        @php $total_amount = 0; @endphp
                            
                            @foreach($demands as $key => $value)

                                @php 
                                $unit_price = ($value['unit_price']*1000);
                                $qty = $value['qty'];
                                $qty_total_amount = ($qty * $unit_price);
                                $total_amount += $qty_total_amount;
                                @endphp
                                
                                <tr>
                                    <td></td>
                                    <td class="text-start">{{ $key+1 }}. {{ ucwords($value['name']) ?? '' }}({{ucwords($value['item_sub_name']) ?? ''}})</td>
                                    <td>{{ $value['qty'] ?? '' }} {{ ucwords($value['unit']) }}</td>
                                    <td>{{ $qty_total_amount }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="word-wrap: break-word; text-align:left;">{{ $value['justification'] ?? '-'}}</td>
                                    
                                </tr>
                                @if($value['specification'] != $value['item_sub_name'])
                                <tr>
                                    <td></td>
                                    <td class="text-start">
                                        Specification:{{$value['specification']}}
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                </tr>
                                @endif
                            @endforeach

                                <tr>
                                    <td></td>
                                    <td class="text-end">Total Amount</td>
                                    <td></td>
                                    <td>{{ $total_amount }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    
                                </tr>
                @endforeach
            
            <tr>
                <td colspan="10"> </td>
                
            </tr>

            @endforeach

            
        @endforeach
    </tbody>
</table>