<table class="table">

    <tr>
        <td rowspan="3">Department</td>
        @foreach($sub_categories as $label => $cat)
        @php $count = count($cat) * 2; @endphp

            
        
            <td colspan="{{$count}}">{{$label}}</td>
            
        

        @endforeach
            
        
        
        
    </tr>
    
    <tr>
        
        @foreach($sub_categories as $label => $cat)
        @foreach($cat as $v)

            @if($label == $v->Item_Category)
            <td colspan="{{2}}"></td>
            @else
            <td colspan="{{2}}">{{$v->Item_Category}}</td>
            @endif
            
            
        @endforeach

        @endforeach
            
        
        
        
    </tr>

    <tr>
        
        @foreach($sub_categories as $cat)
        @foreach($cat as $v)
            <td>qty</td>
            <td>amount</td>
        @endforeach

        @endforeach
        
    </tr>  

         
        @foreach($collection as $key => $value)
            <tr>
                <td>{{$value->name_mm}}</td>
                
                @foreach($sub_categories as $k=>$cat)
                    @foreach($cat as $de => $v)
                    
                        @php 
                        $qty_field_name = $v->Item_Category."_qty";
                        $total_field_name = $v->Item_Category."_amount";
                        @endphp
                        
                        <td>@php echo $value->{$qty_field_name} @endphp</td>
                        <td>@php echo $value->{$total_field_name} @endphp</td>
                        

                    @endforeach
                @endforeach
            </tr>
        @endforeach
    
</table>