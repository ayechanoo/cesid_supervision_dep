<div class="col-2 px-sm-2 px-0 bg-primary" >
    <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100" style="margin-top:10vh;" >
        <a href="/" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <span class="fs-5 d-none d-sm-inline">DashBoard</span>
        </a>

        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Set Up
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item {{ request()->routeIs('equipment-type.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('equipment-type.index')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Equipment Type</span>
                </a>
                
            </li>

            <li class="nav-item {{ request()->routeIs('proposal-type.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('proposal-type.index')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Proposal Type</span>
                </a>
                
            </li>

            <li class="nav-item {{ request()->routeIs('main-proposal.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('main-proposal.index')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Proposal</span>
                </a>
                
            </li>

            <li class="nav-item {{ request()->routeIs('main-category.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('main-category.index')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Main Category</span>
                </a>
                
            </li>
           

            <li class="nav-item  {{ request()->routeIs('sub-cateogry.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('sub-category.index') }}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Sub Category</span>
                </a>
                
            </li>
            

            <li class="nav-item  {{ request()->routeIs('item_info.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('item_info.index') }}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Item List</span>
                </a>
                
            </li>
            

            
            

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
               Demand Management
            </div>

            
            <li class="nav-item {{ request()->routeIs('sv.*') ? 'active' : '' }} ">
                <a class="nav-link" href="{{route('sv.index')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Excel Import</span>
                </a>
                
            </li>

            <li class="nav-item  {{ request()->routeIs('manual.*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('manual.create')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Manual Import</span>
                </a>
                
            </li>


            <div class="sidebar-heading">
                Recommand Management
            </div>

            <li class="nav-item  {{ request()->Is('manual.re/*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('manual.re.excel-import-form')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Excel Import</span>
                </a>
                
            </li>
          
            

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <div class="sidebar-heading">
                Reporting
            </div>

            <!-- Nav Item - Pages Collapse Menu -->

            <li class="nav-item {{ request()->Is('report/filter/*') ? 'active' : '' }} ">
                <a class="nav-link" href="{{route('report.filter.budget_dep')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Dep Reports</span>
                </a>
                
            </li>

            <li class="nav-item {{ request()->Is('/report/deps/by_minid/*') ? 'active' : '' }} ">
                <a class="nav-link" href="{{route('report.deps_by_minid.create')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>ဦးစီးဌာနအလိုက် စာရင်း</span>
                </a>
                
            </li>

            

            
            <li class="nav-item {{ request()->routeIs('report.qties_with_all_deps') ? 'active' : '' }} ">
                <a class="nav-link" href="{{route('report.qties_with_all_deps')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Total Qty with Departments</span>
                </a>
                
            </li>

            <li class="nav-item ">
                <a class="nav-link" href="{{route('supervision.saone')}}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>SA-1001</span>
                </a>
                
            </li>

         
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

            
            <div class="sidebar-card d-none d-lg-flex">
                
                <p class="text-center mb-2"><strong>CESID</strong> Partnered with Myanmar Software Integrated Solutions(MSIS)</p>
                
            </div>

        </ul>
        
    </div>
</div>