
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Freelancer - Start Bootstrap Theme</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{asset('supervision_user/assets/favicon.ico')}}" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.2.0/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css"/>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
     


        
        
        <style>

           
            
        </style>
    </head>
    <body id="">
        <!-- Navigation-->
        @include('layouts.navBar')
        <!-- Masthead-->
        
        <div class="continer-fluid">
            <div class="row">
                <!-- sidebar -->
                @include('layouts.sidebar')
                <!-- sidebar -->
                <div class="col-10 py-3">
                @yield('content')
                </div>
            </div>
        </div>
       
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; Your Website 2022</small></div>
        </div>

        <!-- modal -->

        <div class="modal fade border-none" id="confirm-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="thankYouModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content" style="height: 50vh;">
                    <div class="d-flex flex-column align-items-center my-auto">
                    <h4 class="modal-title mb-4 text-center fw-bold" id="alert_msg_title"  >
                    
                            Are you sure ?
                    </h4>
                    
                    <div>
                        <button type="button" class="btn btn-lg float-right btn-secondary btn-confirm-click rounded-4 mt-2" data-value="false" data-bs-dismiss="modal">{{ 'Cancel' }}</button>
                        <button type="button" class="btn btn-lg  btn-warning rounded-4 btn-confirm-click mt-2" data-value="true" data-bs-dismiss="modal"> {{ 'Continue' }}</button>

                    <div>
                    </div>
                <button type="button" class="btn-close rounded-circle shadow-sm p-3 bg-white" data-bs-dismiss="modal" aria-label="Close"></button>

                </div>
            </div>
        </div>
        <!-- modal  end -->
        
        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.js">
            </script>
        <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script> -->
        <!-- Core theme JS-->
        <!-- <script src="{{asset('supervision_user/js/scripts.js')}}"></script> -->
        
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>
        <script src="{{asset('supervision_user/js/budget.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="{{asset('js/app.js')}}"></script>

        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        </script>
        @yield('script')
        
                                        
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- <script src="{{asset('supervision_user/js/sb-forms-0.4.1.js')}}"></script> -->

        <script>
            var modalConfirm = function (callback) {
                $("#confirm-modal").modal('show');


                $("#confirm-modal .btn-secondary").on("click", function () {
                    callback(false);
                    $("#exampleModal").modal('hide');
                });

                $("#confirm-modal .btn-warning").on("click", function () {
                    callback(true);
                    $("#exampleModal").modal('hide');
                });
            };
        </script>
    </body>
</html>
