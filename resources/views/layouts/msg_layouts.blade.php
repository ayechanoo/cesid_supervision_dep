@if(Session::has('message'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>{{ Session::get('message') }}</strong> 
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif


@if(Session::has('error_msg'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>{{ Session::get('error_msg') }}</strong> 
  
</div>
@endif