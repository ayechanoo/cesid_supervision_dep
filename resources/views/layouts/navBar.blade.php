<nav class="navbar navbar-expand-lg bg-primary text-uppercase fixed-top py-2 " style="background-color:#19b3bc !important;" id="mainNav">
    <div class="container-fluid">
        <div class="navbar-brand w-100 d-flex justify-content-between" href="#page-top" style="white-space:initial;font-size:1rem;">
            <div class="d-flex gap-1" style="">
                
                <a href="{{route('supervision.index')}}" class="text-decoration-none text-light">SuperVision Department(CESID)</a>

                
            </div>

            
        </div>
        <button class="navbar-toggler text-uppercase font-weight-bold bg-bold-blue text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto">

            

           
            
            </ul>
        </div>
    </div>
</nav>

