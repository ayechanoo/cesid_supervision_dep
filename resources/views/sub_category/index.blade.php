@extends('layouts.frontEnd')
@section('content')
    <!-- END SIDEBAR-->


    <div class="row">
        <div class="col-lg-12">
            <div class="card rounded border-0 my-5">
                <div class="card-body">
                    <div class="row dt-table-header">
                        <div class="px-4 d-flex align-items-center">
                            <h4 class="">{{ __('key.subcategoryList') }}</h4>
                        </div>
                        @include('layouts.msg_layouts')
                        <div class="col-12 d-flex justify-content-end">
                            <a href="{{ route('sub-category.create') }}"
                               class="btn btn-sm btn-primary mr-3 rounded ">
                                <i class="bi bi-plus-square"></i>&nbsp;<span>{{__('key.addNew')}}</span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        
                        <table
                            class="table table-bordered table table-head-primary mb-4"
                            id="subCategoryDataTable" aria-describedby="heading">
                            <thead>

                            <tr>
                                <th>{{ __('key.name') }}</th>
                                <th>{{ __('key.categoryType') }}</th>
                                <th>{{ __('key.action') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- END QUICK SIDEBAR-->
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/dashboard/sub_category.js') }}">
    </script>
@endsection
