@extends('layouts.frontEnd')
@section('content')

    <!-- START PAGE CONTENT-->
    <div class="page-heading mt-5">
        <h1 class="page-title">{{ __('key.editSubCategory') }}</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html"><i class="la la-home font-20"></i></a>
            </li>
        </ol>
    </div>
    <div class="page-content fade-in-up">

        <div class="ibox ">
            <div class="ibox-head">
                <div class="ibox-title">{{ __('key.updateSubCategory') }}</div>
                <div class="ibox-tools">
                    <a class="ibox-collapse"><i class="ti-angle-down"></i></a>
                </div>
            </div>
            <div class="ibox-body card">
                <form class="form-horizontal" action="{{route('sub-category.update',$row->uuid)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="p-4">

                    
                        <div class="form-group mb-4 row">
                            <label class="col-2">{{__('key.name')}}</label>
                            <input class="col-8 form-control" type="text" name="name" value="{{$row->name ?? old('name')}}" placeholder="Name">
                            @error('name')
                            <div class="col-4"> <span class="text-danger ">{{ $message }}</span></div>
                           @enderror
                        </div>

                        <input type="hidden" name="item_category_id" id="item_category_id"  >
                            <div class="form-group mb-4 row">

                                <label  class="col-2">{{ __('key.proposalType') }}</label>
                                <select name="item_category_id" id="proposalType_selection" class="col-8 select2-show-search select2 form-control">
                                    <option selected disabled> Please choose ob_end_clean</option>
                                    @foreach ($categories as $category)
                                   
                                   <option value="{{ $category->id }}"
                                   {{ $category->id === $row->category->id ? "selected" : '' }}>{{$category->name}}</option>
                                   @endforeach
                                </select>
                                @error('item_category_id')
                                <div class="col-4"> <span class="text-danger ">{{ $message }}</span></div>
                               @enderror

                            </div>
                        <div class="form-group mb-4 float-right  ">

                            <button class="btn btn-success" type="submit" >{{ __('key.save') }}</button>
                            <a href="{{ route('sub-category.index') }}" class="btn btn-danger" type="submit" >{{ __('key.cancel') }}</a>
                        </div>

                    </div>

                </form>

            </div>

    </div>


    </div>


   @endsection
   @section('script')
   <script>
    $(document).ready(function(){
   
        // $('.select2-show-search').select2({
        //     minimumResultsForSearch: '',
        //     width: '66.7%'
        // });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#proposalType_selection').val(selectedVehicleType).trigger("change");
        $("#proposalType_selection").on('select2:select', function (e) {

            let {value,name} = e.target;

            console.log(value);
            $('input[name="proposal_type_id"]').val(value);

        });

    })
</script>
   @endsection
